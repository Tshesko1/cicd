# Resources
# https://www.w3schools.com/python/python_file_write.asp
# https://www.timetemperature.com/africa/congo_democratic_republic_time.shtml
##################### INITIAL SCRIPT #####################################
# Date : 
# SRC : 
##########################################################################
import tkinter as tk
import tkinter.ttk as ttk
#import tkinter.messagebox as tkMessagebox
from tkinter.messagebox import showinfo, showwarning #, showerror
#from tkinter.scrolledtext import ScrolledText

# importing the required modules
from datetime import datetime # TODO, timezone
import os, platform

#### 3rd Party Modules
import psutil
#### Cloud modules
from firebase_admin import credentials, initialize_app, storage

# Native Scan
#1s python2.5 +
from uuid import getnode as get_mac
deviceMacAddress = get_mac()
#2

#3
#import fcntl
#import socket
#import struct

##########################################################################
# def initialScript():
#     infoSegment = ""
#     completeInfo = ""
#     reportFileName = "SysConfigReport.txt"

#     # First We will print the basic system information
#     # using the platform module

#     print("\n\t\t\t Basic System Information\n")
#     print("[+] Architecture :", platform.architecture()[0])
#     #completeInfo += f"\n{infoSegment}"
#     #input(f"How is it so far ? {completeInfo}")
#     print("[+] Machine :", platform.machine())
#     print("[+] Operating System Release :", platform.release())
#     print("[+] System Name :",platform.system())
#     print("[+] Operating System Version :", platform.version())
#     print("[+] Node: " + platform.node())
#     print("[+] Platform :", platform.platform())
#     print("[+] Processor :",platform.processor())

#     # Using the psutil library to get the boot time of the system
#     boot_time = datetime.fromtimestamp(psutil.boot_time())
#     print("[+] System Boot Time :", boot_time)
#     # getting the system up time from the uptime file at proc directory

#     with open("/proc/uptime", "r") as f:
#         uptime = f.read().split(" ")[0].strip()

#     uptime = int(float(uptime))
#     uptime_hours = uptime // 3600
#     uptime_minutes = (uptime % 3600) // 60
#     print("[+] System Uptime : " + str(uptime_hours) + ":" + str(uptime_minutes) + " hours")

#     # getting the total number of processes currently running
#     pids = []
#     for subdir in os.listdir('/proc'):
#         if subdir.isdigit():
#             pids.append(subdir)
#     print('Total number of processes : {0}'.format(len(pids)))

#     # Displaying The CPU information
#     print("\n\t\t\t CPU Information\n")

#     # This code will print the number of CPU cores present
#     print("[+] Number of Physical cores :", psutil.cpu_count(logical=False))
#     print("[+] Number of Total cores :", psutil.cpu_count(logical=True))
#     print("\n")

#     # This will print the maximum, minimum and current CPU frequency
#     cpu_frequency = psutil.cpu_freq()
#     print(f"[+] Max Frequency : {cpu_frequency.max:.2f}Mhz")
#     print(f"[+] Min Frequency : {cpu_frequency.min:.2f}Mhz")
#     print(f"[+] Current Frequency : {cpu_frequency.current:.2f}Mhz")
#     print("\n")

#     # This will print the usage of CPU per core
#     for i, percentage in enumerate(psutil.cpu_percent(percpu=True, interval=1)):
#         print(f"[+] CPU Usage of Core {i} : {percentage}%")
#         print(f"[+] Total CPU Usage : {psutil.cpu_percent()}%")

#     # reading the cpuinfo file to print the name of
#     # the CPU present

#     with open("/proc/cpuinfo", "r") as f:
#         file_info = f.readlines()

#     cpuinfo = [x.strip().split(":")[1] for x in file_info if "model name" in x]

#     for index, item in enumerate(cpuinfo):
#         print("[+] Processor " + str(index) + " : " + item)

#     # writing a function to convert bytes to GigaByte
#     def bytes_to_GB(bytes):
#         gb = bytes/(1024*1024*1024)
#         gb = round(gb, 2)
#         return gb

#     # Using the virtual_memory() function it will return a tuple
#     virtual_memory = psutil.virtual_memory()
#     print("\n\t\t\t Memory Information\n")
#     #This will print the primary memory details
#     print("[+] Total Memory present :", bytes_to_GB(virtual_memory.total), "Gb")
#     print("[+] Total Memory Available :", bytes_to_GB(virtual_memory.available), "Gb")
#     print("[+] Total Memory Used :", bytes_to_GB(virtual_memory.used), "Gb")
#     print("[+] Percentage Used :", virtual_memory.percent, "%")
#     print("\n")

#     # This will print the swap memory details if available
#     swap = psutil.swap_memory()
#     print(f"[+] Total swap memory :{bytes_to_GB(swap.total)}")
#     print(f"[+] Free swap memory : {bytes_to_GB(swap.free)}")
#     print(f"[+] Used swap memory : {bytes_to_GB(swap.used)}")
#     print(f"[+] Percentage Used: {swap.percent}%")

#     # Gathering memory information from meminfo file

#     print("\nReading the /proc/meminfo file: \n")
#     with open("/proc/meminfo", "r") as f:
#         lines = f.readlines()
#     print("[+] " + lines[0].strip())
#     print("[+] " + lines[1].strip())

#     # accessing all the disk partitions
#     disk_partitions = psutil.disk_partitions()
#     print("\n\t\t\t Disk Information\n")

#     # displaying the partition and usage information
#     for partition in disk_partitions:
#         print("[+] Partition Device : ", partition.device)
#         print("[+] File System : ", partition.fstype)
#         print("[+] Mountpoint : ", partition.mountpoint)
				
#         disk_usage = psutil.disk_usage(partition.mountpoint)
#         print("[+] Total Disk Space :", bytes_to_GB(disk_usage.total), "GB")
#         print("[+] Free Disk Space :", bytes_to_GB(disk_usage.free), "GB")
#         print("[+] Used Disk Space :", bytes_to_GB(disk_usage.used), "GB")
#         print("[+] Percentage Used :", disk_usage.percent, "%")

#     # get read/write statistics since boot
#     disk_rw = psutil.disk_io_counters()
#     print(f"[+] Total Read since boot : {bytes_to_GB(disk_rw.read_bytes)} GB")
#     print(f"[+] Total Write since boot : {bytes_to_GB(disk_rw.write_bytes)} GB")

#     # gathering all network interfaces (virtual and physical) from the system
#     if_addrs = psutil.net_if_addrs()
#     print("\n\t\t\t Network Information\n")
#     # printing the information of eah network interfaces
#     for interface_name, interface_addresses in if_addrs.items():
#         for address in interface_addresses:
#             print(f"Interface :", interface_name)
#             if str(address.family) == 'AddressFamily.AF_INET':
#                 print("[+] IP Address :", address.address)
#                 print("[+] Netmask :", address.netmask)
#                 print("[+] Broadcast IP :", address.broadcast)
#             elif str(address.family) == 'AddressFamily.AF_PACKET':
#                 print("[+] MAC Address :", address.address)
#                 print("[+] Netmask :", address.netmask)
#                 print("[+] Broadcast MAC :",address.broadcast)

#     # getting the read/write statistics of network since boot
#     net_io = psutil.net_io_counters()
#     print("[+] Total Bytes Sent :", bytes_to_GB(net_io.bytes_sent))
#     print("[+] Total Bytes Received :", bytes_to_GB(net_io.bytes_recv))

#     # Getting The battery Information
#     battery = psutil.sensors_battery()
#     print("\n\t\t\t Battery Information\n")
#     print("[+] Battery Percentage :", round(battery.percent, 1), "%")
#     print("[+] Battery time left :", round(battery.secsleft/3600, 2), "hr")
#     print("[+] Power Plugged :", battery.power_plugged)

#     # os.system(f'echo "{completeInfo}" >> {reportFileName}')
#     # os.system(f"subl {reportFileName}")


# def selection():
#     selectedOpt = int(input("1. Initial\n2. Production\nChoose one option: "))

#     if selectedOpt == 1:
#         initialScript()
#     elif selectedOpt == 2:
#         productionScript()
#     else:
#         print("Wrong Choice")
#         exit()

#     saveReport(completeInfo)

		
		# #ttk.Label(self.master, text=strText).grid(row=rowCounter, column=0, columnspan=2, pady=yPad)
		
		# # userAgreementText = ttk.Entry(master)
		# # userAgreementText.grid(row=rowCounter, column=0, rowspan=rowSpan, columnspan=2, pady=yPad)
		# # userAgreementText.insert(tk.END, strText)

		# rowCounter += rowSpan
		# #self.textfield = ScrolledText.ScrolledText(self, wrap=tkinter.WORD)
		# # textfield = ScrolledText(master, wrap=tk.WORD, state=tk.DISABLED, width= 40, height = 10)
		# # textfield.grid(row=rowCounter, column=0, rowspan=rowSpan, columnspan=2, padx=xPad, pady=yPad)
		# # strText = """\
		# # Integer posuere erat a ante venenatis dapibus.
		# # Posuere velit aliquet.
		# # Aenean eu leo quam. Pellentesque ornare sem.
		# # Lacinia quam venenatis vestibulum.
		# # Nulla vitae elit libero, a pharetra augue.
		# # Cum sociis natoque penatibus et magnis dis.
		# # Parturient montes, nascetur ridiculus mus.
		# # """
		# # textfield.insert(tk.END, strText)


		# #userConsent
		# rowCounter += 10
		# # ttk.Label(master, text="I agree to it").grid(row=rowCounter, column=0, pady=yPad)

		# rowCounter +=1
		# finalStr = tk.StringVar()
		# finalStr.set(f"{firstName.get()} | {lastName.get()} | {mobile.get()} | {email.get()}")
		# status = ttk.Label(master, textvariable=finalStr)
		# status.grid(row=rowCounter, column=0)

##################### END OF INITIAL SCRIPT #####################################

### TODOs
# 1. Center the app on start up
# 2. Show steps to be completed with progress: Grey and Green
# Sunday 19 March 2023 01:45:01 PM IST

class GUI():
	def __init__(self):#, master):
		super().__init__()
		self.setAppDetails()
		self.setAppDimensions()
		self.setAppMakeup()


	def setAppDetails(self):
		self.adminMode : bool = True
		self.adminMode = False
		if self.adminMode:
			self.WIP("__inti()__")

		self.targetAppName = '\"SVGD COMBINE\"'

		self.xPad = 5
		self.yPad = 10
		self.rowCounter = 0 #-1

		self.appVersion  = f"1.0.0"
		self.appName     = f"Client System Configuration"
		self.appTitle    = f"{self.appName} - version {self.appVersion}"


	def setAppDimensions(self):
		self.appWindowMinWidth  = 500
		self.appWindowMinHeight = 430
		self.appWindowXPadding  = 300
		self.appWindowYPadding  = 100


	def setAppMakeup(self):
		self.colourDark      = "#000"
		self.colourDarkBlue  = "#1F2732"
		self.colourSkyBlue   = "sky blue"
		self.colourGreen     = "Green"
		self.colourSystemGrey= "#d9d9d9"
		self.colourWhite     = "#FFF"
		self.colourYellow    = "Yellow"

		#self.bgColour        = self.colourSkyBlue
		self.bgColour        = self.colourDarkBlue
		self.textColour      = self.colourWhite


	# TODO : Separate this part in its own method
		self.master = tk.Tk()
		self.master.title(self.appTitle)
		self.master.configure(background=self.colourSkyBlue)
		self.master.configure(background=self.colourDarkBlue)

		appWindowDimensionConfig = f"{self.appWindowMinWidth}x{self.appWindowMinHeight}"
		appWindowDimensionConfig+= f"+{self.appWindowXPadding}+{self.appWindowYPadding}" 
		self.master.geometry(appWindowDimensionConfig)
		self.master.resizable(False, False)

		# #agreement
		strText = f"Ce logiciel a pour but de collecter les informations système"
		strText+= f" sur les caractéristiques physiques de votre ordinateur"
		strText+= f" pour des fins d'analyses et possibles:"
		strText+= f"\n   1. Optimisation de l'application {self.targetAppName}"
		strText+= f"\n   2. Mise à niveau des composantes de votre ordinateur\n       [Micro-Processeur, RAM, ...]"
		strText+= f"\n\nNota Bene: "
		strText+= f"\n1. Les informations collectées demeureront confidentielles."
		strText+= f"\n2. Ce logiciel n'effectue aucune copie, suppression ou altération de vos fichiers personnels déja présents dans l'ordinateur."
		strText+= f"\n3. Votre nom et vos contacts sont demandés afin de vous recontacter sur base de votre demande d'analyse/conception du projet {self.targetAppName}."
		self.userAgreementText = strText
		self.userAgreementConsent = tk.StringVar()

		self.completeInfo = ""
		self.utcTimestamp = "NotYetSet"
		self.localTimestamp = self.utcTimestamp
		#self.reportFileName = f"ClientSystConfigReport-{utcTimestamp}.txt" # +
		self.reportFileName = f"ClientSystConfigReport.txt" 
		
		self.isAppInitialized = False

		self.strVarFirstName = tk.StringVar()
		self.strVarLastName = tk.StringVar()
		self.strVarMobile = tk.StringVar()
		self.strVarEmail = tk.StringVar()

		self.clientContactDetails = False
		self.btnScan = None

		self.frUserAgreement = tk.Frame(self.master, bg=self.bgColour) #, fg=self.colourWhite, font=("Helvetica", 12, "bold") )
		self.frUserDetails = tk.LabelFrame(self.master, text="Votre nom et vos contacts officiels") # pady=f"{2*self.yPad}") #bg=self.bgColour)

		self.createStatusBar(self.master)

		self.presentUserAgreement()#self.frUserAgreement)#self.master)
		# TEST : self.scanClientProdScript()

		# Launch the app
		self.master.mainloop()


	def updateRowCounter(self, value=1):
		self.rowCounter+= value


	def WIP(self, featureName=None, popUp=False, futEnhancement=False):
		if featureName == None:
			showinfo("Feature Not Set", "Enter the name of the feature in WIP")
			return
		strText = f"[W.I.P.] Work In Progress for the feature:\n'{featureName}'"
		if futEnhancement:
			strText = "\nCette fonctionnalité peut être ajoutée sur base de votre demande\n"
		print(strText)
		if popUp:
			showinfo("Work In Progress", strText)


	def createStatusBar(self, container=None):
		if container == None:
			container = self.master
		# TODO
		#builtBy = f"developed by '{self.devTeam['name']}'"
		#builtBy = f"built by '{self.devTeam.name}'" # Not Valid Call
		self.strVarStatusBar = tk.StringVar() #(self.get_name_of_the_app())
		self.strVarStatusBar.set(f"Developed by CSMART LAB +919606848849 | csmartlab@gmail.com")
		# TODO
		#self.lblStatusBar = tk.Label(container, textvariable=self.strVarStatusBar, bg=self.colourSkyBlue, fg=self.colourWhite, font=("Helvetica", 10, "bold"), bd=1, relief=tk.SUNKEN, anchor=tk.W)
		self.lblStatusBar = tk.Label(container, textvariable=self.strVarStatusBar, font=("Helvetica", 10, "bold"), bd=1, relief=tk.SUNKEN, anchor=tk.W)
		self.lblStatusBar.pack(side=tk.BOTTOM, fill=tk.X)


	def saveReport(self, reportText=None):
		if reportText == None:
			showinfo("Invalid Report Provided", "No Valid Report has been provided !")
			return
		else:
			self.reportFileName = self.renameTheReportFileWithTimeStamp()
			#reportText = self.addUserDetailsInTheReportHeading(reportText)
			with open(self.reportFileName, "wt") as report:
				report.write(reportText)
			self.uploadToCloud(self.reportFileName)


	def renameTheReportFileWithTimeStamp(self):#, initialNameOfTheFile):
		#utcTimestamp = 
		#localTimeStamp = datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
		localTimeStamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
		utcTimestamp = localTimeStamp

		fileName  = self.reportFileName.split(".")[0]
		extension = self.reportFileName.split(".")[1]
		
		fileName = f"{platform.node()}-{fileName}-{utcTimestamp}.{extension}"
		self.appTemporarilyGeneratedFiles.append(fileName)
		if self.adminMode:
			print(f"Temp files: {self.appTemporarilyGeneratedFiles}")

		return fileName


	def addUserDetailsInTheReportHeading(self): #, scanReportText):
		nDots = 5*"*"
		heading = f"\n{nDots}Client's Details{nDots}"
		heading += f"\nName : {self.strVarFirstName.get()} | {self.strVarLastName.get()}"
		heading += f"\nContacts : {self.strVarMobile.get()} | {self.strVarEmail.get()}"
		heading += f"\nUser Consent: '{self.userAgreementConsent.get()}'"
		heading += f"\nUser Consent Date: '{datetime.now()}'"
		heading += f"\nAgreement Text:\n{self.userAgreementText}"
		heading += f"\n{3*nDots}\n\n{nDots}Client's Device Scan Report{nDots}"

		# scanReportText = heading + scanReportText
		# return scanReportText
		self.completeInfo = f"{heading}\n{self.completeInfo}"


	def uploadToCloud0(self, nameOfTheFileToBeUploaded):
		print("\n\nW.I.P. Upload to Cloud")
		self.firebaseUpload(nameOfTheFileToBeUploaded)
		
		# if self.adminMode:
		#     input(f"Check the report file: '{reportFileName}'")
		#if self.deleteReportFile(nameOfTheFileToBeUploaded):
		if self.deleteAppTemporarilyGeneratedFiles():
			#exit()
			#self.status.after(3000, self.master.destroy)
			self.master.after(3000, self.master.destroy)
		else:
			#self.deleteReportFile(nameOfTheFileToBeUploaded)
			self.deleteAppTemporarilyGeneratedFiles()
			print("Error while uploading | del")
		#return


	def generateTheAdminKeyFile(self): #, fileToUpload):
		# TODO:
		import json # https://www.youtube.com/watch?v=-51jxlQaxyA
		# TODO: json.load()   -> file
		# TODO: json.loads()  -> string
		# boolean: lower case: true / false --- NOT upper case

		# TODO : try : except Exception:

		# TODO: self.fileAdminKeyContent
		#self.fileAdminKeyContent = "svgdcombineAdminServiceAccount.json"

		self.adminKeyContent = {
			"type": "service_account",
			"project_id": "svgdcombine",
			"private_key_id": "1f80bf24553bb86e2e7879fdb50806c14538743f",
			"private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDMWDEs8r3IF2/r\nN1GDLwB+WXaG2ByijdxncP6RBHNu+krqm6SqRmMPdVikHLc6Xwqp6o+1mimzPdpt\nbxjreVvsI8ilC7HC6188M2pWpLXu/Lyy+AUTID8KwRsJAwMd/ZMkAe4e4te/9EvM\nYbzj4P0j11q9REDex8AUyGEoOj6X32AB9psUO6OReUBVcgs8NM6OWa19AVSrlDCr\nVFJYayNzE2l3Giu5RQ7gqYQTa+MjMvf8FnJnT5K00C/6r2n9pcO6Tz8AV69zOxZj\nG20zCfsfFMJpOthDi40OBZ6+IoW/c3zVbkFMmZ4qWySjI/28d7wmcps+3y536JkA\nleYErkZPAgMBAAECggEAKnmYey3XyfwpLNzaSijecuT4sK3CQf231x6TI6Sns+U9\n5kdE8onf9Gohazd21KDF0CzsA0UQruvH9+BUbznSJCMKBViJcilHqfVMAMeK86rA\nNHk+YMvenscl7fkc0EKRr7Xg/7WKQCGxEcItJ7bwF1+fwmCqFUSzBf7ixyBYO2v5\nbMJnld20MqR5OqlhuUi/QztQwVUgPeSrEmO3GfjniCZIOmkIK8bqBoUeEJnnV9A4\nYXkJ/ivWWvGpCDXyHpYZyQpL1IIK3u/Hn1+r6PMQkYEB6zS9rn4sP6euxLrknPYh\nlK3ZEcqpvP3Zw3Quj+cV8Jf6z5b9MKP/kFrPEa16YQKBgQDlN3Cwdu5DuzpIoS3j\nRQVgETzOzw4ZfgxgaNnJpShryPGWHvd3czhyEECWe0n9JDle8sPtKL+nOY+iFmjY\nusdLRSSxcAyVpfpWedO0E4Sx8CTkSdBppWd8JyhqSQN8ucOa9ffSy3LnoFQdMZ0q\nN8RbKGigF/gwCBBVoXOaZvTfbwKBgQDkOMDcox9ZuQx3H66GAluhaqg5tRC3EQ/J\nb9tj8E7IOD56GGo3oIFMa9lOJ7UNvOizIq8q+JBEWuHEAce7JiGZe5USKrHGitGe\nlHlIaceJzMOMEieZUbSTSXgj/zlM9alx8t39+23ji029f7dKqSyONB2hXqnKgs2E\n+pGRHF2XIQKBgFG5EH56CL0xVbhrbvbmtuHng2ygugAwxqzg9TVFo0G6pRfG9qgp\n9mgEj2KXlmugrKvDlkNKEfrHNQSvRLZw8XlOr6RrtLDuOnD2gppjkx315tFV1+6y\nwFCtjWVHnuyrNqhbJf6L9u3KZfL6q3RP6bwujg72PNCzL0+4foUATDIVAoGAOiM3\nBj3Lw7h/M21vsRWyr/kSp7i3bFs9UUxccCYMe7bQO5wymr8ZkRU8Q0ucNMa15MRm\nKSUu4yrfrmXhsowxLP8tZ3ZW2UQNyD78cnT37uOpwOnzCnyk6oxhvZHL0A4Yvz1m\nTcgaI0geYrLBR5S3E/a0waHy0PLnVgPax0OZl+ECgYALJBKDqMKlNcri3J4olDnv\nf5r/OlMA4l8QxQNSuu+GcraWgcgx4p4nBX+PhqSnq1NlcYC6h/r0T2IYroPBON9X\npsPe1lsEpF02e6nHMpgqGaN7S5Jjh2qgLSKA7sZHrI+4uaHLj9PZG/lU/0mnMBvQ\ntRL+IRFwkKPCJicUxmeiPA==\n-----END PRIVATE KEY-----\n",
			"client_email": "firebase-adminsdk-a1z1c@svgdcombine.iam.gserviceaccount.com",
			"client_id": "101355361869845114176",
			"auth_uri": "https://accounts.google.com/o/oauth2/auth",
			"token_uri": "https://oauth2.googleapis.com/token",
			"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
			"client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-a1z1c%40svgdcombine.iam.gserviceaccount.com"
		}
		
		#with open(fileAdminKeyContent, "wt") as fakc:
		with open(self.fileAdminKeyContent, 'w', encoding='utf-8') as fakc:
			#fakc.write( str(self.adminKeyContent) )
			json.dump(self.adminKeyContent, fakc, ensure_ascii=False, indent=4)# , sort_keys=True) 
		#self.uploadToCloud(fileToUpload)


	def uploadToCloud(self, nameOfTheFileToBeUploaded):
		print("W.I.P. Upload to Cloud")
		try:
			self.generateTheAdminKeyFile()
			self.firebaseUpload(nameOfTheFileToBeUploaded)
		except Exception as e:
			heading = "Network issues while uploading"
			strText = "Il y a un souci avec votre connexion internet\nVeuillez réessayer ..."
			strText += "\n\n[Si ça persiste, veuillez prendre une photo de cette page et nous l'envoyer par email/WhatsApp.]"
			#strText += "\n\n[👉🏽 Si ça persiste, veuillez prendre une photo de cette page et nous l'envoyer par email/WhatsApp.]"
			#if self.adminMode:
			if True:
				strText += f"\n\n{e}"
			#showinfo(heading, strText, icon="warning")
			showwarning(heading, strText)
			
			self.master.after(6000, self.master.destroy)
			#self.tryToUploadAgain(nameOfTheFileToBeUploaded)
			return
		else: # All Good
			if self.adminMode:
				#input(f"Check the report file: '{reportFileName}'")
				input(f"Check the report file: '{nameOfTheFileToBeUploaded}'")
			self.frUserDetails.pack_forget()
			strText = "Scan Terminé avec Succès\n\n\nVeuillez nous recontacter\npour confirmer ce scan"
			ttk.Label(self.master, text=strText, background=self.bgColour, foreground=self.colourYellow, font=("Helvetica", 22, "bold")).pack()
			self.master.after(5000, self.master.destroy)
		finally: # 
			self.deleteAppTemporarilyGeneratedFiles()
			#exit()
			#return
		
	#def deleteReportFile(self, fileName): # TODO array of files to delete
		# if platform.system() == "Linux" or platform.system() == "Darwin":
		#   deleteCommand = f"rm -vr {fileName}"
		# elif platform.system() == "Windows":
		#   deleteCommand = f"del {fileName} -v"
		# else:
		#   pltName = input("Which platform is this ?")
		#   print(f"'{pltName}' is not yet currently supported")
		#   return False
		# os.system(deleteCommand)
		# strText = "Scan Completed Successfully !"
		# print(strText)
		# #self.status['text'] = strText
		# return True
	def deleteAppTemporarilyGeneratedFiles(self):#, listOfFilesToBeDeleted=self.appTemporarilyGeneratedFiles): # TODO array of files to delete
		
		# TODO
		# Undefined - return
		# if win
		# elif lnx
		# for loop to delete

		# if platform.system() == "Linux" or platform.system() == "Darwin":
		#   for index, fileName in enumerate(self.appTemporarilyGeneratedFiles):
		#     strText = f"[{index}]> '{fileName}'"
		#     deleteCommand = f"rm -vr {fileName}"
		#     os.system(deleteCommand)
		#     if self.adminMode:  
		#       print(strText)
		#     #showinfo("Delete loop", strText, icon="info")

		# elif platform.system() == "Windows":
		#   for fileName in self.appTemporarilyGeneratedFiles:
		#     if self.adminMode:
		#       print(f"[{fileName}]")
		#     deleteCommand = f"del {fileName} -v"
		#     os.system(deleteCommand)
		# else:
		#   pltName = input("Which platform is this ?")
		#   print(f"'{pltName}' is not yet currently supported")
		#   return False
		# #os.system(deleteCommand)
		# strText = "Temporarily Files Removed Successfully !"
		# print(strText)
		# #self.status['text'] = strText
		# return True

		currentDevicePlatform = platform.system()
		supportedPlatforms = [ "Windows", "Linux",  "Darwin" ]
		deleteCommands     = [ "del -v",  "rm -vr", "rm -vr" ]
		delCmd = None

		for zipPlatformAndCmd in zip(supportedPlatforms, deleteCommands):
			if currentDevicePlatform == zipPlatformAndCmd[0]:
				delCmd = zipPlatformAndCmd[1]
		#else: #
		if delCmd == None:
			#showinfo(f"Unsupported Platform", f"The platform: '{currentDevicePlatform}' is currently not yet supported", icon="warning")
			showwarning(f"Unsupported Platform", f"The platform: '{currentDevicePlatform}' is currently not yet supported")
			return False

		print("\nQuick Clean up of temp files")
		for index, fileName in enumerate(self.appTemporarilyGeneratedFiles):
			deleteCommandFullString = f"{delCmd} {fileName}"
			strText = f"[{index}]> '{deleteCommandFullString}'"      
			os.system(deleteCommandFullString)
			if self.adminMode:
				print(strText)
			#showinfo("Delete loop", strText, icon="info")
		else:
			strText = "Temporarily Files Removed Successfully !"
			if True: #self.adminMode:
				print(strText)
			#self.status['text'] = strText
			return True


	def firebaseUpload(self, testFileToBeUploaded=None):
		if testFileToBeUploaded == None:
			print("File name not provided")
			return
		# https://storage.googleapis.com/svgdcombine.appspot.com/ClientSystConfigReport.txt

		# global serviceAccountCredentials
		# global gsBucketURL
		#self.isAppInitialized
		
		gsBucketURL = "gs://svgdcombine.appspot.com"
		serviceAccountCredentials = self.fileAdminKeyContent  # "svgdcombineAdminServiceAccount.json"

		# from firebase_admin import credentials, initialize_app, storage
		# Init firebase with your credentials
		#cred = credentials.Certificate("YOUR DOWNLOADED CREDENTIALS FILE (JSON)")
		if not self.isAppInitialized:
			cred = credentials.Certificate(serviceAccountCredentials)
			#initialize_app(cred, {'storageBucket': 'YOUR FIREBASE STORAGE PATH (without gs://)'})
			initialize_app(cred, {'storageBucket': f'{gsBucketURL.split("gs://")[1]}'})
			self.isAppInitialized = True

		# Put your local file path
		if testFileToBeUploaded != None:
			fileName = testFileToBeUploaded #"myImage.jpg"

		bucket = storage.bucket()
		blob = bucket.blob(fileName)
		blob.upload_from_filename(fileName)

		# Opt : if you want to make public access from the URL
		blob.make_public()
		
		if self.adminMode:
			print(f"The URL of the file {fileName} uploaded : {blob.public_url}")
		print("\n\nSUCCESS : Report file uploaded to Server\n")


	def presentUserAgreement(self, container=None):
		print(f"{self.appName} running on '{platform.system()}'")
		
		container = self.frUserAgreement
		self.WIP(f"presentUserAgreement(): {container}")
		#if container != None:  
		container.pack()
		ttk.Label(container, text="NOTE IMPORTANTE", font=("Helvetica", 12, "bold"), background=self.bgColour, foreground="red").grid(row=self.rowCounter, column=0, columnspan=2, pady=self.yPad)
		self.updateRowCounter()

		self.userAgreementTextWidget = tk.Text(container, width=50, height=15, bd=5, relief="groove", wrap=tk.WORD, font=("Helvetica", 12))
		#self.userAgreementTextWidget = tk.Text(master, state=tk.DISABLED, width=20, height=10, bd=5, relief="groove", wrap=tk.WORD)
		self.userAgreementTextWidget.insert(tk.END, self.userAgreementText)
		#self.userAgreementTextWidget.grid(fill="x", expand="yes")
		self.userAgreementTextWidget.grid(row=self.rowCounter, column=0, columnspan=2, padx=10)#self.xPad)
		self.userAgreementTextWidget.configure(state= tk.DISABLED)
		self.rowCounter += 1

		# Friday 14 January 2022 11:28:42 AM IST
		# Checkbox
		# https://www.pythontutorial.net/tkinter/tkinter-checkbox/
		#agreement = tk.StringVar()
		chckbxUserAgreementConsent = ttk.Checkbutton(container, text="J'ai lu, j'ai compris et je suis d'accord",
			command=self.userAgreementConsent_changed, variable=self.userAgreementConsent, # agreement,
			#bg=self.bgColour,
			#fg=self.textColour,
			#font=("Helvetica", 12, "bold"),
			onvalue='agree', offvalue='disagree')
		chckbxUserAgreementConsent.grid(row=self.rowCounter, column=0, padx=f"{self.xPad}", pady=self.yPad)
		#userConsent.grid(row=rowCounter, column=0, padx=20, pady=yPad)
		self.btnNext = tk.Button(container, state=tk.DISABLED, text="Continuer", command=lambda: self.displayUserContactInfos(container))
		self.btnNext.grid(row=self.rowCounter, column=1, padx=f"{self.xPad}", pady=self.yPad)
		self.updateRowCounter()


	def userAgreementConsent_changed(self):
		#tk.messagebox.showinfo(title='Result', message=agreement.get())
		#global btnScan, userConsent, adminMode
		
		#print(userConsent["variable"])
		if self.adminMode:
			print(f"Current User Consent Choice : {self.userAgreementConsent.get()}'")

		if self.userAgreementConsent.get() == "agree":
			# self.btnScan['state'] = tk.NORMAL
			self.btnNext.configure(state=tk.NORMAL)
			self.btnNext.configure(background=self.colourGreen)
			self.btnNext.configure(foreground=self.colourWhite)
			#self.userAgreementTextWidget.grid_forget()
		elif self.userAgreementConsent.get() == "disagree":
			# self.btnScan['state'] = tk.DISABLED
			self.btnNext.configure(state=tk.DISABLED)
			self.btnNext.configure(background=self.colourSystemGrey)
			self.btnNext.configure(foreground=self.colourDark)
			# textAgreement.grid()


	def displayUserContactInfos(self, previousContainerToRemove=None):
		if self.adminMode:
			print(f"Agreement: '{self.userAgreementConsent.get()}' | r : {self.rowCounter} | container : {previousContainerToRemove}")
		if self.userAgreementConsent.get() == "agree" and previousContainerToRemove != None:
			if self.adminMode:
				print("New Frame !")
			previousContainerToRemove.pack_forget()
			#self.frUserAgreement.pack_forget()

			self.rowCounter = 0
			self.frUserDetails.pack(pady=f"{2*self.yPad}")
			self.listOfWidgetsToDisableEditAccess = []

			self.addFirstName(self.frUserDetails)
			self.addLastName(self.frUserDetails)
			self.addMobileNumber(self.frUserDetails)
			self.addEmailID(self.frUserDetails)
			self.addContactMeButton(self.frUserDetails)

			self.btnScan = tk.Button(self.master, state=tk.DISABLED, text="Scanner mon ordinateur", comman=self.scanClientDevice)
			self.btnScan.pack(pady=f"{4*self.yPad}") #grid(row=self.rowCounter, column=0, pady=20)


	def addFirstName(self, container=None):
		if container != None:
			tk.Label(container, text="Votre Prénom" #, background=self.bgColour
			).grid(row=self.rowCounter, column=0, pady=self.yPad)
			fName = ttk.Entry(container, textvariable=self.strVarFirstName)
			fName.grid(row=self.rowCounter, column=1, padx=self.xPad)
			self.listOfWidgetsToDisableEditAccess.append(fName)
			self.updateRowCounter()

			
	def addLastName(self, container=None):
		if container != None:
			ttk.Label(container, text="Nom de famille").grid(row=self.rowCounter, column=0, pady=self.yPad)
			#self.strVarLastName.set("")
			lName = ttk.Entry(container, textvariable=self.strVarLastName)
			lName.grid(row=self.rowCounter, column=1, padx=self.xPad)
			self.listOfWidgetsToDisableEditAccess.append(lName)
			self.updateRowCounter()


	#def removeEntryPlaceholder(self, event, widget):
		#print(f"Event: {widget}")
	def removeEntryPlaceholder(self, event, widget, strVar):
		print(f"Current Widget State Normal : { widget['state'] != tk.NORMAL }") # == :
		# if widget['state'] == tk.NORMAL:
		#   print("Entry already clicked !")
		#   return
		# else:
		#   print(f"ELSE Current Widget State : {widget['state']} ----- {tk.NORMAL}") # == tk.NORMAL:
		
		#if self.adminMode:
		if True:
			print(f"Event: {event}")
			#print(f"Widget: {widget} : {widget['text'].get()}")
			print(f"Widget: {widget}")
			print(f"StringVar: {strVar.get()}")
			strVar.set("")
			print(f"StringVar: {strVar.get()}")
			#widget['text']=""
			widget.insert(0, "")
			widget.configure(state=tk.NORMAL)
			print(f"[{widget['state']}]Last Check Current Widget State Normal : { widget['state'] == tk.NORMAL }") 


	def addMobileNumber0(self, container=None):
		if container != None:
			ttk.Label(container, text="Numéro de Téléphone").grid(row=self.rowCounter, column=0, pady=self.yPad)
			mobileNum = ttk.Entry(container, textvariable=self.strVarMobile)
			mobileNum.grid(row=self.rowCounter, column=1, padx=self.xPad)
			self.listOfWidgetsToDisableEditAccess.append(mobileNum)
			self.updateRowCounter()

	#TODO
	def addMobileNumber(self, container=None):
		if container == None:
			self.WIP(f"Container for '{ 'mobile number' }' has not been set properly")
			return
	#if container != None:
		ttk.Label(container, text="Numéro de Téléphone").grid(row=self.rowCounter, column=0, pady=self.yPad)
		self.strVarMobile.set("Ex: +243xxxxxxxxxx")
		mobileNum = ttk.Entry(container, textvariable=self.strVarMobile, state=tk.DISABLED)
		mobileNum.grid(row=self.rowCounter, column=1, padx=self.xPad)
		# https://stackoverflow.com/questions/7299955/tkinter-binding-a-function-with-arguments-to-a-widget
		mobileNum.bind("<Button-1>", lambda event, widget=mobileNum, strVar=self.strVarMobile : self.removeEntryPlaceholder(event, mobileNum, strVar) )
		self.listOfWidgetsToDisableEditAccess.append(mobileNum)
		self.updateRowCounter()


	def addEmailID(self, container=None):
		if container != None:
			ttk.Label(container, text="Adresse Email").grid(row=self.rowCounter, column=0, pady=self.yPad)
			#self.strVarEmail.set("")
			emailEntry = ttk.Entry(container, textvariable=self.strVarEmail)
			emailEntry.grid(row=self.rowCounter, column=1, padx=self.xPad)
			self.listOfWidgetsToDisableEditAccess.append(emailEntry)
			self.updateRowCounter()


	def addContactMeButton(self, container=None):
		if container != None:
			self.btnContactMe = tk.Button(container, text="Vous pouvez me conctacter.", command=self.contactMe, foreground=self.colourWhite, background=self.bgColour)
			self.btnContactMe.grid(row=self.rowCounter, column=0, columnspan=2, padx=self.xPad, pady=self.yPad)
			self.updateRowCounter()


	def contactMe(self, minimumLength=2):
		for index, widget in enumerate(self.listOfWidgetsToDisableEditAccess):
			print(f"Widget {index} : '{widget.get()}'")
			#if len(widget["textvariable"].get()) > 2:
			# TODO Proper Validation of each field
			if len(widget.get()) > minimumLength:        
				widget.configure(state=tk.DISABLED)
			else:
				#showinfo("Valeur(s) Incorrecte(s)", "Veuillez entrer des Valeurs Correctes.", icon="warning")
				showwarning("Valeur(s) Incorrecte(s)", "Veuillez entrer des Valeurs Correctes.")
				return
		else: # All went well, now client can Scan
			self.addUserDetailsInTheReportHeading()
			self.btnContactMe.configure(state=tk.DISABLED)
			self.btnScan.configure(state=tk.NORMAL)
			self.btnScan.configure(background=self.colourGreen)
			self.btnScan.configure(foreground=self.colourWhite)


	def updateCompleteSysInfo(self, infoSegment): # The Segment MUST be a complete string
		self.completeInfo += f"\n{infoSegment}"
		print(infoSegment)


	def scanClientDevice(self):
		self.fileAdminKeyContent = "svgdcombineAdminServiceAccount.json"
		self.appTemporarilyGeneratedFiles = []
		self.appTemporarilyGeneratedFiles.append(self.fileAdminKeyContent)
		if self.adminMode:
			print(f"Temp files: {self.appTemporarilyGeneratedFiles}")

		#self.nativeScan()
		self.scanClientProdScript()


################################ Native Scan ################################
################ 1
	# https://stackoverflow.com/questions/159137/getting-mac-address

	# # python2.5 +
	# from uuid import getnode as get_mac
	# deviceMacAddress = get_mac()

	def getMacWithUUID(self):
	#def getMacWithUUID():
		global deviceMacAddress
		value = hex(deviceMacAddress)
		self.updateCompleteSysInfo(f"{value}") #0xdf8aa5c603d2

		value = ':'.join(("%012X" % deviceMacAddress)[i:i+2] for i in range(0, 12, 2))
		self.updateCompleteSysInfo(f"From UUID : {value}") # DF:8A:A5:C6:03:D2

	################ 2
	# import platform
	# if platform.system() == 'Linux':
	#   import LinuxMac
	#   import linuxmac
	#   mac_address = LinuxMac.get_mac_address()
	# elif platform.system() == 'Windows':
	#   # etc
	#   mac_address = WindowsMac.get_mac_address()
	# print("\n\n\n")

	################ 3
	# import fcntl
	# import socket
	# import struct

	#def getHwAddr(self, ifname):
	def getHwAddr(self, ifname):
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', bytes(ifname, 'utf-8')[:15]))
		self.updateCompleteSysInfo(f"{ifname} : {':'.join('%02x' % b for b in info[18:24])}")


	################ 4
	#>>> for i in zip(['system','node','release','version','machine','processor'],platform.uname()): print(i[0],':',i[1])

	################ 5


	################ Their executions
	def nativeScan(self):
		self.updateCompleteSysInfo("\n\nNative Scan\n\n\n")
		self.getMacWithUUID()
		#self.getHwAddr('wlp9s0b1')
		#print(getHwAddr('enp0s8'))
		#self.getHwAddr('enp5s0')

		self.updateCompleteSysInfo(f"\n\n\n")

	# if __name__ == "__main__":
	#     main()
################################ End of Native Scan ################################


	def scanClientProdScript(self):
		# First We will print the basic system information
		# using the platform module
		self.updateCompleteSysInfo("\n\t\t\t Basic System Information\n")
		self.updateCompleteSysInfo(f"[+] Architecture : {platform.architecture()[0]}")
		self.updateCompleteSysInfo(f"[+] Machine : {platform.machine()}")
		self.updateCompleteSysInfo(f"[+] Operating System Release : {platform.release()}")
		self.updateCompleteSysInfo(f"[+] System Name : {platform.system()}")
		self.updateCompleteSysInfo(f"[+] Operating System Version : {platform.version()}")
		self.updateCompleteSysInfo(f"[+] Node: {platform.node()}")
		self.updateCompleteSysInfo(f"[+] Platform : {platform.platform()}")
		self.updateCompleteSysInfo(f"[+] Processor : {platform.processor()}")

		# Using the psutil library to get the boot time of the system
		boot_time = datetime.fromtimestamp(psutil.boot_time())
		self.updateCompleteSysInfo(f"[+] System Boot Time : {boot_time}")

		# TODO
		if platform.system() == "Linux" or platform.system() == "Darwin":
		#if True: # Linux-Specific paths
			# getting the system up time from the uptime file at proc directory
			with open("/proc/uptime", "r") as f:
				uptime = f.read().split(" ")[0].strip()
			uptime = int(float(uptime))
			uptime_hours = uptime // 3600
			uptime_minutes = (uptime % 3600) // 60
			self.updateCompleteSysInfo(f"[+] System Uptime : {str(uptime_hours)} : {str(uptime_minutes)} hours")

			# getting the total number of processes currently running
			pids = []
			for subdir in os.listdir('/proc'):
				if subdir.isdigit():
					pids.append(subdir)
			self.updateCompleteSysInfo('Total number of processes : {0}'.format(len(pids)))


		# Displaying The CPU information
		self.updateCompleteSysInfo("\n\t\t\t CPU Information\n")
		# This code will print the number of CPU cores present
		self.updateCompleteSysInfo(f"[+] Number of Physical cores : {psutil.cpu_count(logical=False)}")
		self.updateCompleteSysInfo(f"[+] Number of Total cores : {psutil.cpu_count(logical=True)}")
		self.updateCompleteSysInfo("\n")

		# This will print the maximum, minimum and current CPU frequency
		cpu_frequency = psutil.cpu_freq()
		self.updateCompleteSysInfo(f"[+] Max Frequency : {cpu_frequency.max:.2f}Mhz")
		self.updateCompleteSysInfo(f"[+] Min Frequency : {cpu_frequency.min:.2f}Mhz")
		self.updateCompleteSysInfo(f"[+] Current Frequency : {cpu_frequency.current:.2f}Mhz")
		self.updateCompleteSysInfo("\n")

		# This will print the usage of CPU per core
		for i, percentage in enumerate(psutil.cpu_percent(percpu=True, interval=1)):
			self.updateCompleteSysInfo(f"[+] CPU Usage of Core {i} : {percentage}%")
			self.updateCompleteSysInfo(f"[+] Total CPU Usage : {psutil.cpu_percent()}%")

		# reading the cpuinfo file to print the name of
		# the CPU present


		# print(f"Demo Auto Build running on '{platform.system()}'")
		if platform.system() == "Linux" or platform.system() == "Darwin":
		# if True: # Linux-Specific paths
			with open("/proc/cpuinfo", "r") as f:
				file_info = f.readlines()

			cpuinfo = [x.strip().split(":")[1] for x in file_info if "model name" in x]

			for index, item in enumerate(cpuinfo):
					self.updateCompleteSysInfo(f"[+] Processor {str(index)} : {item}")


		# writing a function to convert bytes to GigaByte
		def bytes_to_GB(bytes):
				gb = bytes/(1024*1024*1024)
				gb = round(gb, 2)
				return gb

		# Using the virtual_memory() function it will return a tuple
		virtual_memory = psutil.virtual_memory()
		self.updateCompleteSysInfo("\n\t\t\t Memory Information\n")
		#This will print the primary memory details
		self.updateCompleteSysInfo(f"[+] Total Memory present : {bytes_to_GB(virtual_memory.total)} Gb")
		self.updateCompleteSysInfo(f"[+] Total Memory Available : {bytes_to_GB(virtual_memory.available)} Gb")
		self.updateCompleteSysInfo(f"[+] Total Memory Used : {bytes_to_GB(virtual_memory.used)} Gb")
		self.updateCompleteSysInfo(f"[+] Percentage Used : {virtual_memory.percent} %")
		self.updateCompleteSysInfo("\n")

		# This will print the swap memory details if available
		swap = psutil.swap_memory()
		self.updateCompleteSysInfo(f"[+] Total swap memory :{bytes_to_GB(swap.total)}")
		self.updateCompleteSysInfo(f"[+] Free swap memory : {bytes_to_GB(swap.free)}")
		self.updateCompleteSysInfo(f"[+] Used swap memory : {bytes_to_GB(swap.used)}")
		self.updateCompleteSysInfo(f"[+] Percentage Used: {swap.percent}%")

	
		
		# print(f"Demo Auto Build running on '{platform.system()}'")
		if platform.system() == "Linux" or platform.system() == "Darwin":
		# if True: # Linux-Specific paths
			# Gathering memory information from meminfo file
			self.updateCompleteSysInfo("\nReading the /proc/meminfo file: \n")
			with open("/proc/meminfo", "r") as f:
				lines = f.readlines()
			self.updateCompleteSysInfo(f"[+] {lines[0].strip()}")
			self.updateCompleteSysInfo(f"[+] {lines[1].strip()}")



		# get read/write statistics since boot
		disk_rw = psutil.disk_io_counters()
		self.updateCompleteSysInfo(f"[+] Total Read since boot : {bytes_to_GB(disk_rw.read_bytes)} GB")
		self.updateCompleteSysInfo(f"[+] Total Write since boot : {bytes_to_GB(disk_rw.write_bytes)} GB")

		# gathering all network interfaces (virtual and physical) from the system
		if_addrs = psutil.net_if_addrs()
		self.updateCompleteSysInfo(f"\n\t\t\t Network Information\n")
		# printing the information of eah network interfaces
		for interface_name, interface_addresses in if_addrs.items():
			for address in interface_addresses:
				self.updateCompleteSysInfo(f"Interface : {interface_name}")
				if str(address.family) == 'AddressFamily.AF_INET':
					self.updateCompleteSysInfo(f"[+] IP Address : {address.address}")
					self.updateCompleteSysInfo(f"[+] Netmask : {address.netmask}")
					self.updateCompleteSysInfo(f"[+] Broadcast IP : {address.broadcast}")
				elif str(address.family) == 'AddressFamily.AF_PACKET':
					self.updateCompleteSysInfo(f"[+] MAC Address : {address.address}")
					self.updateCompleteSysInfo(f"[+] Netmask : {address.netmask}")
					self.updateCompleteSysInfo(f"[+] Broadcast MAC : {address.broadcast}")

		# getting the read/write statistics of network since boot
		net_io = psutil.net_io_counters()
		self.updateCompleteSysInfo(f"[+] Total Bytes Sent : {bytes_to_GB(net_io.bytes_sent)}")
		self.updateCompleteSysInfo(f"[+] Total Bytes Received : {bytes_to_GB(net_io.bytes_recv)}")

		# Getting The battery Information
		battery = psutil.sensors_battery()
		self.updateCompleteSysInfo(f"\n\t\t\t Battery Information\n")
		self.updateCompleteSysInfo(f"[+] Battery Percentage : {round(battery.percent, 1)} %")
		self.updateCompleteSysInfo(f"[+] Battery time left : {round(battery.secsleft/3600, 2)} hr")
		self.updateCompleteSysInfo(f"[+] Power Plugged : {battery.power_plugged}")

		self.addCompletionTimeStamp()
		self.saveReport(self.completeInfo)

		# os.system(f'echo {completeInfo} >> {reportFileName}')
		# os.system(f"subl {reportFileName}")

		# DELIMITER = f"\n{20*'-'}\n" 
		# print(DELIMITER, completeInfo, DELIMITER, "Status ???")

	def addCompletionTimeStamp(self):
		#self.completeInfo += f"\n\nScan Completion Date : {datetime.now().strftime('%Y-%m-%d_%H:%M:%S')}"
		self.updateCompleteSysInfo(f"\n\nScan Completion Date : {datetime.now().strftime('%Y-%m-%d_%H:%M:%S')}")


def main():
		GUI()
		

if __name__ == "__main__":
	main()