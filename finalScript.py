import time, os, platform
#python3 -m pip install pyinstaller
#pip3 install pyinstaller

def tic():
  global startTime
  startTime = time.time()


def toc(testName=None):
  global startTime 
  strText = f"{time.time() - startTime } second(s)"
  if testName:
    print(f"{testName}: {strText}")
  return strText


def runUnitTest():
  print("\nPYTHON 3 UNIT TESTs\n")
  os.system("python3 -m unittest test_SVGD_COMBINE.py")

def visualOfTheNode():
  print("WIP :  To be properly implemented later with the support of the 'Platform Specific Commands' Class")
  return

  os.system('clear && lsb_release -a && uname -a && echo "HOME: " $HOME && ls -lart $HOME && echo "USER : " $USER && pwd && echo "Content of this current folder: " && ls -lart . && echo "SYSTEM FOLDERS" && ls -lart /')

def createTheScreen():
  return
  #apt-get install xvfb -y
  import os
  print("-------> Creating the Screen")
  os.system("apt-get install xvfb -y")
  os.system("Xvfb :1 -screen 0 720x720x16 &")
  #os.environ("DISPLAY") = ":1.0"

tic()

if platform.system() == "Darwin":
  print(f"Be Aware {platform.system()} only supports 'icns' format for Icons extension")

appName = "SVGD_COMBINE"
pathToIcon = "images/Cnk.jpg"
#pathToIcon = None

appsToBePackage = [
  {
    "name": "ClientSystemConfiguration",
    "icon": None
  },
  { "name": "SVGD_COMBINE",
    #"icon": "images/Cnk.jpg"
    "icon": "images/cnk_W6o_2.ico"
    #"icon": None
  },

]

#Server
# if input("===> '\n* self.adminMode\n* self.demoPassword\n* self.isTrialPeriod '\nDid you configure them properly ? ").lower() != 'y':
#   exit()



print(f"---> Demo Auto Build running on '{platform.system()}'")

createTheScreen()

visualOfTheNode()

if platform.system() == "Linux" or platform.system() == "Darwin":
  print(os.environ['HOME'])
  #Server
  #input(" ... Is this correct ? ")

# platformSpecificBuildCommand = "time " if L or M else "" 
# if input("===> '\n* self.adminMode\n* self.demoPassword\n* self.isTrialPeriod '\nDid you configure them properly ? ").lower() != 'y':
#  exit()

print(f"WIP ---> {platform.system()} : Delete Stage : On Hold For now")

# if platform.system() == "Linux" or platform.system() == "Darwin":
#   print(f"---> {platform.system()} : Delete Stage : On Hold For now")
#   #return
#   pass
#   #exit()
#   #continue
#   clear = lambda: os.system('clear')
#   clear()

#   #rm -rv build/ dist/ __pycache__/ *.spec || time pyinstaller --onefile SVGD.py COMBINE.py

#   print("Deleting the old build resources ...")
#   platformSpecificBuildCommand = "rm -rv ./build/ ./dist/ __pycache__/ *.spec"
#   os.system(platformSpecificBuildCommand)


#   #Server
#   # if input("===> Send to Windows for Build ? ").lower() == 'y':
#   #   os.system("bash ../sendToWindows.sh")
#   os.system("bash ../sendToWindows.sh")

# elif platform.system() == "Windows":
#   print(f"---> {platform.system()} : Delete Stage")
#   clear = lambda: os.system('cls')
#   clear()

#   if input("===> Do you want to delete the old build resources ? ").lower() == 'y':
#     items = [".\\dist\\*.exe", ".\\build/", ".\\__pycache__\\", ".\\*.spec" ]
#     for item in items:
#       print(f"Now at item : '{item}'")
#       platformSpecificBuildCommand = f"del {item}" # -v" # -r"
#       os.system(platformSpecificBuildCommand)

print("---> Out of the Delete Stage")

runUnitTest()

for app in appsToBePackage:
  platformSpecificBuildCommand = " " if platform.system() == "Windows" else "time "

  platformSpecificBuildCommand += f"pyinstaller"    # PyInstaller
  platformSpecificBuildCommand += " --onefile"      # Package into single
  platformSpecificBuildCommand += " -w"             # Windows only - No Console
  #platformSpecificBuildCommand += f" -icon={app['icon']}" if app['icon'] != None else " "
  platformSpecificBuildCommand += f" --icon={app['icon']}" if app['icon'] != None else " "
  #platformSpecificBuildCommand += " SVGD.py COMBINE.py"

  print("---> Warning: --noconfirm")
  platformSpecificBuildCommand += " --noconfirm"

  platformSpecificBuildCommand += f" {app['name']}.py"

  os.system(platformSpecificBuildCommand)
  toc(f"{app['name']} Build")

for app in appsToBePackage:
  #os.system(f".\\dist\\{app['name']}") if platform.system() == "Windows" else os.system(f"./dist/{app['name']}")
  os.system(f".\\dist\\{app['name']}") if platform.system() == "Windows" else os.system(f"cp -v ./dist/{app['name']} /tmp/")
#os.system(f"echo 'For Testing, Open another Tab in the  \'/tmp\' folder'")


if platform.system() == "Linux": #or platform.system() == "Darwin":
  # if input("Reboot ? y/n : ").strip().lower() == "y":
  #    os.system("sudo shutdown -r now")
  os.system(f"echo 'For Testing, Open another Tab in the  \'/tmp\' folder'")
  os.system(f"/tmp/ClientSystemConfiguration && /tmp/SVGD_COMBINE")

visualOfTheNode()