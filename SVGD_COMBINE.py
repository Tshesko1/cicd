#from tkinter import *
#main = Tk()
#main.mainloop()

# Useful Resources ---
# pip3 install -U pyinstaller

# Tuesday 01 February 2022 04:10:46 PM IST
# https://python-course.eu/advanced-python/
# https://python-course.eu/machine-learning/

#import COMBINE as combine

print("# BackEnd & FrontEnd : Being Loaded ...")
#https://python-textbok.readthedocs.io/en/1.0/Introduction_to_GUI_Programming.html
# from tkinter import Tk, Label, Button

# TEST : https://www.code4example.com/python/python-program-to-count-vowels-or-consonants-of-the-given-string/

# try:
#     import Tkinter as tk
# except ImportError:
#     import tkinter as tk

# Essential FrontEnd modules
from sys import version_info
if version_info.major == 3:
	import tkinter as tk
	from tkinter import ttk
	from tkinter.font import Font
	#from tkinter import PhotoImage
	from tkinter.messagebox import showinfo, showwarning, showerror, askyesno

elif version_info.major == 2:
	import Tkinter as tk
	from Tkinter import ttk
	from tkFont import Font
	from Tkinter.messagebox import showinfo # TODO

# Essential BackEnd modules
from itertools import combinations #, permutations, combinations_with_replacement
from datetime import datetime # TODO, timezone
import time, re, sqlite3, os
###############################
# 3rd party dependencies
###############################
# import psutil                       #   1. https://pypi.org/project/psutil/
### PWRD
# from stdiomask import getpass       #   2. https://pypi.org/project/stdiomask/
# TODO
#import numba, numpy as np
import warnings
warnings.filterwarnings('ignore')
# ###########################################################
import platform, hashlib

print("--------> ", platform.machine())
#TODO : from uuid import getnode as get_mac
# import charInStr #as chInStr

# import PIL
# from PIL import Image , ImageTk
#from PIL import Image
#import PIL.ImageTk
# ###########################################################
# # To be moved back to Backend once Refactored
# from itertools import combinations
# import time # To be moved back to Backend once Refactored

###################################################################################
# ADMIN Settings
# TODO: Maximus Utilities module: tic-toc, centerWindow, 

adminMode : bool = True
adminMode = False

testerMode : bool = True
testerMode = False

isTrialPeriod : bool = True
#isTrialPeriod = False
#deadlineTuple = (2022, 2, 24)
#deadlineTuple = (2022, 2, 27)
#deadlineTuple : tuple = (2022, 3, 7)
#deadlineTuple : tuple = (2023, 3, 5)
#deadlineTuple : tuple = (2023, 4, 30)
#deadlineTuple : tuple = (2023, 5, 10)
#deadlineTuple : tuple = (2023, 5, 25) # Delais Client
#deadlineTuple : tuple = (2023, 6, 25) # For Internal Tests
deadlineTuple : tuple = (2123, 6, 25) # 100 Years ;-)
# Friday 06 October 2023 09:10:43 PM IST : Removing the print / debug


# devConfKey = "linux" if adminMode else "clientFrSY"
# devConfKey = "windows" if adminMode else "clientIrDvd"
# devConfKey = "windows" if adminMode else "clientFrSY"

devConfKey : str = "linux"
#devConfKey = "tshanoza"
# devConfKey = "clientFrSY"
# devConfKey = "clientIrDvd"
#devConfKey = "NgoCap001"
devConfKey = None

#if True: #adminMode:
if adminMode:
	print(f"Selected device : {devConfKey}")

# TODO: self.master.update_idletasks()
###################################################################################


class DB_SVGD:
	def __init__(self):
		self.cnx = self.cursor = self.currentUser = self.todayDateSnapShot = None
		print("DATABASE !")
		
		self.setDatabaseCredentials()
		if devConfKey == "linux":
			self.deleteDatabase()
		
		self.databaseTables = [ 
			{ "name": "users", "sqlCreationCmd": "" },
			{ "name": "sessions", "sqlCreationCmd": "" }
		]
		# TODO++ def tableCreation(name: str, sqlCreationCmd: str):
		
		self.databaseTables = ["users", "sessions"]		
		self.userPrivileges = ["User", "Admin"]		
		self.userStatus = ["INACTIVE", "ACTIVE"]		
		self.credentialsList = [
			("David", "SvgD2021"),
			("Maximus", "MasterCard256")
		]
		
		self.todayDateSnapShot = "2023-06-30 01:44:21.691721"
		self.todayDateSnapShot = str(datetime.now())
		self.todayDateSnapShot = datetime.now().strftime("%Y-%m-%d %H:%M:%S.---")
				
		self.defaultUsersList = [
			# Date "2023-06-07 15:46:53.347300"
			(1, 'Maximus', 'MasterCard256', 'csmartlab@gmail.com', '2023-06-07 15:46:53.347300', 'ACTIVE'),
			(10, 'FakeUser', 'FakePass', 'fake@email.com', '2023-06-07 15:50:53.347300', 'INACTIVE'),
			(15, 'Tshanoza', 'TsumbuReRe', 'tshanoza@gmail.com', self.todayDateSnapShot, 'ACTIVE'),
			(20, "David", "SvgD2021", "nsanguadavid@gmail.com", '2022-02-13 17:01:10.843212', "ACTIVE"),
			# Name : DAVID | NSANGUA
			# Contacts : +240222016300 | nsanguadavid@gmail.com
			# User Consent: 'agree'
			# User Consent Date: '2022-02-13 17:01:10.843212'
		]		
		# if input(f"DELETE Database : {self.databaseName} ? ").lower()[0] == "y": 
		#   os.system(f"rm -v {self.databaseName}")
		
		# os.system(f"rm -v {self.databaseName}")
		
		# else:
		self.createTableUsers()
		self.insertUsers()
		self.createTableSessions()
		# return self


	def getTimestamp(self):
		self.todayDateSnapShot = datetime.now()
		return self.todayDateSnapShot
		

	def setDatabaseCredentials(self):
		self.dbName : str = ":memory:" # Volatile Memory in RAM
		self.dbName = "confidential.db"
		self.dbName = "_svgd.db"
		global dbName   # Lazy Solution
		dbName = self.dbName
		
		self.dbUsername : str = "dbTrueAdmin"
		self.dbPassword : str = "db_F0ng0l@#2120"
		self.dbHostname : str = "dbTrueAdmin"
		self.dbPortNumb : str = ""


	def deleteDatabase(self): # TODO
		if input(f"DELETE Database : {self.dbName} ? ").lower()[0] == "y":
			os.system(f"rm -v {self.dbName}")
	
	
	def dbConnexion(self):
		self.cnx = sqlite3.connect(f"{self.dbName}")
		self.cursor = self.cnx.cursor()
		db = { "cnx": self.cnx, "cursor": self.cursor}
		# print("'db' = ", db)
		return db
		
		
	def rollbackChanges(self):
		self.cnx.rollback()
	
	
	def commitChanges(self, strMessage: str = ""):
		self.cnx.commit()
		self.cnx.close()
		strMessage = strMessage.strip()
		if not adminMode:
			return
		if strMessage != None and strMessage != "":
			print(strMessage)


	def quickBackup(self):
		print("About to do a QuickBackup")
		timestamp: str = f"{datetime.now().strftime('%Y-%m-%d_%H:%M:%S')}"
		dbBackupCnx = sqlite3.connect(f"dbBackup_{timestamp}.db")
		self.dbConnexion()
		self.cnx.backup(dbBackupCnx)
		dbBackupCnx.close()
		print("QuickBackup Completed")
	
	
	def scheduledBackup(self):
		# https://www.youtube.com/watch?v=oreAsJTNcsA : SQL Beginner to Advanced in One Hour | CareerFoundry Webinar
		strSchedBackup : str = """
		CREATE EVENT schedBackupEvent
		ON SCHEDULE EVERY 1 DAY
		STARTS CURRENT_TIMESTAMP
		COMMENT "Scheduled Backup Event"
		DO
		  CREAT DATABASE my_backup_database;
		  CREATE TABLE my_backup_database.sessions_backup
		    AS SELECT * FROM my_database.sessions;
		"""
		
		self.dbConnexion()
		self.cursor.execute(strSchedBackup)		
		self.commitChanges("QuickBackup Completed")    


	# def deleteRecord(self, kwargs**):
	def deleteRecordWithID(self, recordID: int = -1, tableName: str = "users"):
		if recordID == -1:
		  recordID = int(input("Enter the Record ID to delete from the Database : "))
		cmdDeleteRecord = f"DELETE FROM {tableName} WHERE rowid = {recordID}"
		self.cursor.execute(cmdDeleteRecord)
		self.commitChanges()


	def allGoodIn(self, tableName: str = None, **kwargs):
		if tableName == None:
			print("Table Name Not Provided")
			return
		if tableName not in self.databaseTables:
			print(f"Table '{tableName}' Not Found !")
			return
		
		self.dbConnexion()
		self.cursor.execute(f"SELECT rowid, * FROM {tableName};")
		results = self.cursor.fetchall()
		#print(f"Table {tableName} : ", connexion[1].fetchall())
		if adminMode:
			print(f"Table '{tableName}' has {len(results)} record(s)")
		
		if not adminMode:
			return
		
		for record in results:
			print(record)
		if tableName == "users" and len(results) > 0:
			print("\nDid you notice any 'INACTIVE' Account ?\n")


	def createTableUsers(self):
		# cmdToExecute += "(userID INTEGER PRIMARY KEY AUTOINCREMENT, userName TEXT UNIQUE NOT NULL, userPass TEXT NOT NULL);"
		# sqlite3.IntegrityError: UNIQUE constraint failed: users.userName
		
		tableName: str = "users"
		cmdCreateTableUsers: str = """CREATE TABLE IF NOT EXISTS users(
			userName TEXT UNIQUE NOT NULL,
			userPass TEXT NOT NULL,
			userEmail TEXT UNIQUE NOT NULL,
			userTimestamp DATE NOT NULL,
			userStatus TEXT NOT NULL
		);
		"""
		# userID INTEGER PRIMARY KEY AUTOINCREMENT,
		# userStatus TEXT, ### Active/ Inactive
		
		if adminMode:
			print(f"About to Create Table 'user'")
		self.dbConnexion()
		
		if adminMode:
			self.cursor.execute("EXPLAIN " + cmdCreateTableUsers)
			print("EXPLANATION : ", self.cursor.fetchall())
		
		self.cursor.execute(cmdCreateTableUsers)
		self.commitChanges("Table Created !")
		#print("Table Created !")
		self.allGoodIn(tableName)


	def createTableSessions(self):
		tableName: str = "sessions"
		cmdCreateTableSessions: str = f"""CREATE TABLE IF NOT EXISTS
		{tableName}(
			sessionID INTEGER PRIMARY KEY,
			userID TEXT,
			sessionIN TEXT,
			sessionOUT DATE,
			sessionRemarks TEXT,
			FOREIGN KEY(userID) REFERENCES users(userID)
		);
		"""
		self.cursor.execute(cmdCreateTableSessions)
		self.commitChanges()
		self.allGoodIn(tableName)


	def WIP(self, featureName=None, popUp=False, futEnhancement=False):
		if featureName == None:
			showinfo("Feature Not Set", "Enter the name of the feature in WIP")
			return
		strText = f"[W.I.P.] Work In Progress for the feature:\n'{featureName}'"
		if futEnhancement:
			#strText += "\n\nCette fonctionnalité peut être ajoutée sur base de votre demande\n\n"
			strText = "\n\nCette fonctionnalité peut être ajoutée sur base de votre demande\n\n"
		print(strText)
		if popUp:
			showinfo("Work In Progress", strText)

	def uiResetPassword(self):
		pass


	def initResetPassword(self, userCredentials = None):
		print(f"userCredentials = {userCredentials}")
		userEmail = "nsanguadavid@gmail.com"

		if userCredentials == None:
			print("Getting the Credentials via the CLI")
			username = input("Username : ")
			currentPassword = input("Current Password : ")
			newPassword = input("New Password : ")
		#elif userCredentials.username and userCredentials.currentPassword and userCredentials.newPassword:
		elif userCredentials["username"] and userCredentials["currentPassword"] and userCredentials["newPassword"]:
			print("Credentials received as function arguments")
			username = userCredentials["username"]
			currentPassword = userCredentials["currentPassword"]
			newPassword = userCredentials["newPassword"]
		else:
			print("Complete User Credentials not received\nProper User Credentials could not be Found !")
			return

		#if self.userExists(username, currentPassword):
		if self.userExists(username, userEmail):
			self.updatePasswordOf(username, newPassword)


	def userExists(self, targetUsername, targetUserEmail):
		#def userExists(self, targetUsername, userPass):
		# cmdSearch  = f"SELECT * FROM users WHERE userName = ? AND userPass = ?"
		# self.dbConnexion()
		# self.cursor.execute(cmdSearch, (targetUsername, userPass))
		# return self.cursor.fetchall()
		
		cmdSearch  = f"SELECT * FROM users WHERE userName = ? AND userEmail = ?"
		self.dbConnexion()
		self.cursor.execute(cmdSearch, (targetUsername, targetUserEmail))
		return self.cursor.fetchall()
	
	
	def updatePasswordOf(self, username: str = "", password = None):
		if username == "":
			return
	
		connexion = self.dbConnexion()
		if password == None:
			cmdToExecute = f"SELECT * FROM users WHERE userName = ?"
			# results = connexion[1].execute(cmdToExecute, (username, ))
			results = self.cursor.execute(cmdToExecute, (username, ))
			if adminMode:
		  		print(results.fetchall())
		
		else:
			cmdToExecute = f"UPDATE users SET userPass = ? WHERE userName = ?"
			
			# results = connexion[1].execute(cmdToExecute, (password, username))
			results = self.cursor.execute(cmdToExecute, (password, username))
			#print(results)
			
			# connexion = commitChanges(connexion)
			self.commitChanges()
			self.allGoodIn("users")


	def insertUsers(self, usersList=None):
		# pass    
		# self.WIP("insertUsers")
		# return
		if usersList == None:
			print("External / Additional List of Users Not Provided !\nUsing the default list")
			# return
			usersList = self.defaultUsersList

		tableName = "users"
		for index, user in enumerate(usersList):
			if adminMode:
				print(f"About to insert : {user}")
			#cmdToExecute = f"INSERT INTO {tableName} VALUES ({index+1}, '{user[0]}','{user[1]}')"
			cmdToExecute = f"INSERT INTO {tableName} (userName, userPass) VALUES (?, ?)"
			connexion = self.dbConnexion()
			#print("connexion : ", connexion)
			# for key, value in enumerate (connexion):
			#   print(key, value)
			
			# nsanguadavid@gmail.com
			# if self.userExists(username, currentPassword):
			#   self.updatePasswordOf(username, newPassword)
			
			
			#if self.userExists(user[1], "nsanguadavid@gmail.com"): #currentPassword): # TODO
			if self.userExists(user[1], user[3]): #currentPassword): # TODO
				if adminMode:
					print("Record already present - No Further Action Required")
			else:
				cmdInsertUsers = f"INSERT INTO {tableName} (userName, userPass, userEmail, userTimestamp, userStatus) VALUES (?, ?, ?, ?, ?)"
				self.cursor.execute(cmdInsertUsers, (user[1], user[2], user[3], user[4], user[5]))
				self.commitChanges()
				print("Done inserting this record")
		
		self.allGoodIn(tableName)
############################################################


#class COMBINE(DB_SVGD):
class COMBINE:
	def __init__(self):
		super().__init__()
		#self.db = DB_SVGD() # Instance of the Database fatures
		self.wrongPass : int = 0
		#self.adminMode : bool = True if adminMode else False
		self.adminMode : bool = adminMode
		self.testerMode: bool = testerMode
		
		if self.adminMode:
			print("BE: init()")
		
		self.setDevTeam()	
		#self.isItTheRegisteredDevice()
		self.isValidTrial()
		self.isItTheRegisteredDevice()


	def showTesterMode(self):
		dummyWindow = tk.Tk()
		dummyWindow.title("Warning Window")
		strText : str = "Tester Mode"
		showinfo(strText, strText)
		dummyWindow.destroy()
		return


	def trialOver(self, text=None):
		heading = "La période d'essaie a éxpirée" # "The Trial period has expired"
		strText = f"{heading}\n{text}" if text != None else heading    
		showwarning(heading, strText, icon="warning")


	def getAppName(self): # patch
		# TODO: Get it from the SQLite3 DB
		return "SVGD COMBINE"

	# TODO: Duplicate in class SVGD 
	def setDevTeam(self):
		self.devTeam : dict = {
			"name": "CSMART LAB",
			#"mobile": "+919606848849", OK
			"mobile": "+639392604156",
			"email": "csmartlab@gmail.com",
			#"website": "https://www.csmartlab.org",
			"website": "www.csmartlab.org"
		}


	#def getDevTeam(self, key: str = ""):
	def getDevTeam(self, key: str):
		#showinfo("key value", f"Key = {key}")
		key = key.strip()
		if key == "": # Empty Key
			return None
		if key not in self.devTeam:
			return None  # Undefined
			#return ""  # Undefined
		
		return self.devTeam[key]


	def hash_password(self, plainValue):
	#def hash_password(plainValue):
		plainValue : str = plainValue.strip()
		encryptedValue = hashlib.sha256(str.encode(plainValue)).hexdigest()
		#if self.adminMode:
		if True:
			#encryptedValue = hashlib.sha256(str.encode(plainValue)).hexdigest()
			print(f"\n# ---> {plainValue} ---> {encryptedValue}")
		return encryptedValue
		#return hashlib.sha256(str.encode(plainValue)).hexdigest()


	def check_password_hash(self, currentDeviceSignature, configuredDeviceSignature):
		self.isValidDevice = currentDeviceSignature == configuredDeviceSignature
		#isValidDevice = currentDeviceSignature == configuredDeviceSignature
		#if self.adminMode:
		if True:
			print(f"Received :\n{currentDeviceSignature}\tDevice\n{configuredDeviceSignature}\tConfigured")
			print(f"Device Verdict: {self.isValidDevice}")
			#print(f"Device Verdict: {isValidDevice}")
		return self.isValidDevice
		#return isValidDevice


	def quickEncrypt(self, plainValue: str):
		plainValue = plainValue.strip()
		return hashlib.sha256(str.encode(plainValue)).hexdigest()


	def loadAuthorizedDevices(self):
	#def isItTheRegisteredDevice():
		#TODO
		#if self.adminMode:
		#self.WIP("isItTheRegisteredDevice()")
		# MAC
		# Processor
		# Node
		# OS version
		#self.isValidDevice = True
		#configuredDeviceSignature = "f551f11538ff11e160450effa0c16bb70d283cce4dbf21ffe9cce68f360f62db" # Mx Ok
		# Linux-5.11.0-46-generic-x86_64-with-glibc2.29
		#configuredDeviceSignature = "a172b1b10d1a3a96fde145abe492c0476b214a2e0622056ba1615acebd042dae7520b5a1b312efde4fd7e2793ef4bc0cf8f1c235f778d203ab7216a0e31b3880ef0fdfc7e91bafe1efb7d4b12a56e367487b3d6e1164e8181f0249298bf2418e4828e60247c1636f57b7446a314e7f599c12b53d40061cc851a1442004354fed33785a16cccdf769ab34429ba34252f27d70ef156e91bd1feb58f0ac13717752ff11e8f120400b8f892c0fa0d62537b4b10a2666e6b5aadcb7a5578064cb58991fe219f08b081e5ed5001da3c644c3848008e889e75b6a426d2ba2eb322541107520b5a1b312efde4fd7e2793ef4bc0cf8f1c235f778d203ab7216a0e31b3880"
		
		# Linux-5.11.0-46-generic-x86_64-with-glibc2.14
		configuredDeviceSignature = "a172b1b10d1a3a96fde145abe492c0476b214a2e0622056ba1615acebd042dae7520b5a1b312efde4fd7e2793ef4bc0cf8f1c235f778d203ab7216a0e31b3880ef0fdfc7e91bafe1efb7d4b12a56e367487b3d6e1164e8181f0249298bf2418e4828e60247c1636f57b7446a314e7f599c12b53d40061cc851a1442004354fed33785a16cccdf769ab34429ba34252f27d70ef156e91bd1feb58f0ac13717752ff11e8f120400b8f892c0fa0d62537b4b10a2666e6b5aadcb7a5578064cb5899a8cb0c321c66ae81e45157f5015b112f156e1be7dda18bbbe13435c74a522fe17520b5a1b312efde4fd7e2793ef4bc0cf8f1c235f778d203ab7216a0e31b3880"
		# Received :
		# 3c196486db8b4e52b9d90039db4477c89cdc74be348da2c4bcf5139468668697  Device
		# f551f11538ff11e160450effa0c16bb70d283cce4dbf21ffe9cce68f360f62db  Configured
		# f551f11538ff11e160450effa0c16bb70d283cce4dbf21ffe9cce68f360f62db
		
		self.configuredDevices : dict = {
			"linux": { # Wednesday 26 July 2023 01:51:39 PM IST
				'Architecture': 'a172b1b10d1a3a96fde145abe492c0476b214a2e0622056ba1615acebd042dae', 'Machine': '7520b5a1b312efde4fd7e2793ef4bc0cf8f1c235f778d203ab7216a0e31b3880', 'OSR': '0a352db4f53237ba022e88fd4cfdd5ca2d2e1c5e116772696547fe6c06644566', 'SystName': '4828e60247c1636f57b7446a314e7f599c12b53d40061cc851a1442004354fed', 'OSV': '513db318578617a9a0ff0c3b362dc4065a8a800c39b177f0d5d77a73539e830d', 'Node': 'ff11e8f120400b8f892c0fa0d62537b4b10a2666e6b5aadcb7a5578064cb5899', 'Platform': 'd4ad80bbf97d9791c1a50278ccb1722321ac55bf720cefecc8aa4f84b7f0e3fb', 'Processor': '7520b5a1b312efde4fd7e2793ef4bc0cf8f1c235f778d203ab7216a0e31b3880'},
				
				# {'Architecture': 'a172b1b10d1a3a96fde145abe492c0476b214a2e0622056ba1615acebd042dae', 'Machine': '7520b5a1b312efde4fd7e2793ef4bc0cf8f1c235f778d203ab7216a0e31b3880', 'OSR': '6c2f8ee492238f4c5464b1fe3bf8ec480320199445b0740a202e4f8484967dcb', 'SystName': '4828e60247c1636f57b7446a314e7f599c12b53d40061cc851a1442004354fed', 'OSV': 'fa51510207e76db845d36a8e6d312117794db9f401237b2eabeab5c3ad516323', 'Node': 'ff11e8f120400b8f892c0fa0d62537b4b10a2666e6b5aadcb7a5578064cb5899', 'Platform': '98c59970089259998f9ee033013e3860ce71adbf3006ddbcbea64875e48eacee', 'Processor': '7520b5a1b312efde4fd7e2793ef4bc0cf8f1c235f778d203ab7216a0e31b3880'},
				
				# "linux": {
				#   "Architecture": "a172b1b10d1a3a96fde145abe492c0476b214a2e0622056ba1615acebd042dae",
				#   "Machine"     : "7520b5a1b312efde4fd7e2793ef4bc0cf8f1c235f778d203ab7216a0e31b3880",
				#   "OSR"         : "3456865147cefafa6b5851f2b5cf474755485521f63a7b4a003103ee8f34929d", #"9c20999e9745be819129a41ac91afdfd7f59251c15d2bc9c2e8ba689224dba1d", # "ae5669c3cb80405895daf5dddfcd9b0cbeb85bc0fedbad6be39896107eaec9ed", #"eb23748bd406639314dc72317b564fa1b796fe7bd6ffbd8bbd4659020a990ebf", #"09f6c6a363850d90d0965f1a08291179e58ffe49fe9188f9a501dbd556954c7e", #"7aaa1d8e87eacf993d8737bbb17c5bef97236c0b1935d79a76b90ce72819d3b3", #"b194dac8b5ff9974ae633fbf9a70aee266ec8a54a3a6130f4426667c4177ba25", #"ef0fdfc7e91bafe1efb7d4b12a56e367487b3d6e1164e8181f0249298bf2418e",
				#   "SystName"    : "4828e60247c1636f57b7446a314e7f599c12b53d40061cc851a1442004354fed",
				#   "OSV"         : "cbf88a61bff35879f0a1a27032fa33f88545192e2719f35a5d64ee6ebf097e85", #"090750b92c17e2cd37204b77239c7bff4750499b8c166c95f40f12220f2805c0", #"d022389b01ce2ef3533ab46ceb988461eb813bd8325b74dd43a1ef8903e22f36", #"be56a16990b17edd3caa0ef4667e4f58896b978f5e5f38ec6287960d385fe607",#"58bc8c1a2928e2b4875b4679c5f08f449d0d1d2a40ad068a7b68e7eb29321ea0", #"e70435123c38154e8760230bb70e3ea50ff71b7dbced2a164fafdb7d833749bf", #"5cf998249f55e1e44a355f5499ab97c7d6b8c65946c3adfe7c4164055feb12c6", #"33785a16cccdf769ab34429ba34252f27d70ef156e91bd1feb58f0ac13717752",
				#   "Node"        : "ff11e8f120400b8f892c0fa0d62537b4b10a2666e6b5aadcb7a5578064cb5899",
				#   #"Platform"    : "ea52f894f800db03a4a8edde9899476048b31a298d8a275679ee95a9e551195a", # , # "1fe219f08b081e5ed5001da3c644c3848008e889e75b6a426d2ba2eb32254110",
				#   "Platform"    : "a38a5d5b13ae1ad8e85bf797b9aa89210baf707b876eea9cc664ea0e9f698d7c", #"49eb02fbecb40acb42444e1d56af8bdb0e9b946c9a1aef274e2180e7232f478e", #"cc82a58700c50b40fb0876417eae72366cf5d08bc1f3e82da1edf56d98b3cd5d", #"c3a7ce7448a3b55badacb2d4622e2e89446ab0d885feb86b05a19739ee7cc528", #"5ff3339a78bbce51455b70738ee6f0a946d3da618edf587603d61ee6d74f7fda", #"2db59d990933c85d3f28cc87cc09f1d268eec5fc00c386cb87ebb1d3a7be70df", #"12fed75aeca6063a71527a9665ff5b91e55ceebb3f7d0167a878079fbd819dc6", # After Package / Build
				#   "Processor"   : "7520b5a1b312efde4fd7e2793ef4bc0cf8f1c235f778d203ab7216a0e31b3880", #"7520b5a1b312efde4fd7e2793ef4bc0cf8f1c235f778d203ab7216a0e31b3880", #"7520b5a1b312efde4fd7e2793ef4bc0cf8f1c235f778d203ab7216a0e31b3880", #"7520b5a1b312efde4fd7e2793ef4bc0cf8f1c235f778d203ab7216a0e31b3880", #__________________",
				# },
		
			"windows": {
				"Architecture": "a172b1b10d1a3a96fde145abe492c0476b214a2e0622056ba1615acebd042dae",
			    "Machine"     : "558b624e3a60d3e4a393272854dbbd35c6fea3f18ee692235dcbe31be8370700",
			    "OSR"         : "4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5",
			    "SystName"    : "d598026a9cbc60505f138ce53ac78088d582100c196d0f70c7e2538d4a8d7e10", 
			    # OSV : NEW '10.0.19041' > '4906553c99e9225413bacd029603f2549fe8d972bf389770063f3e932b623d80' | OLD Configured for: '7a40c8835f716a44e947b12462842142146dec18cf742fe9ecd71ccba07c8883' <---------- MISMATCH
			    "OSV"         : "4906553c99e9225413bacd029603f2549fe8d972bf389770063f3e932b623d80",                        
			    "Node"        : "cb4c9f5644fc3b1a11cc19a94c70eb5642f2c6611c0547e74ace352eee0ffd51",
			    "Platform"    : "40aac1ba637ba1b447cb69dd44f1b565fa486fda0743b0cfb6ca74f7eb2b5ecf", 
			    "Processor"   : "27f54d2a946a9e108d6ae4cb6449651538e1d71dd4b56c1f509d8c1d2d6c96f6",
		  	},
		
		  	"tshanoza": {
			    "Architecture": "a172b1b10d1a3a96fde145abe492c0476b214a2e0622056ba1615acebd042dae",
			    "Machine"     : "558b624e3a60d3e4a393272854dbbd35c6fea3f18ee692235dcbe31be8370700",
			    "OSR"         : "4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5",
			    "SystName"    : "d598026a9cbc60505f138ce53ac78088d582100c196d0f70c7e2538d4a8d7e10", 
			    "OSV"         : "e372374601d0c7ba9920512ecc11c73d5f7560008c971db8008ff28756243f42", 
			    "Node"        : "f610e41e90b8b8c2ef3c493b2048df41f7d0a5248464c6e288079c8ab8d17a50",
			    "Platform"    : "d53faec16e6cbe5ca3c5750113d853845828c59d71429204e7b85a2eca3b9599", 
			    "Processor"   : "bca7398c36b1e2a365f70437da28c7e75779386d738595f89328d000afd7653b",
			},
		
		  	"clientFrSY": { # Franck Siya
			    "Architecture": "a172b1b10d1a3a96fde145abe492c0476b214a2e0622056ba1615acebd042dae",
			    "Machine"     : "558b624e3a60d3e4a393272854dbbd35c6fea3f18ee692235dcbe31be8370700",
			    "OSR"         : "4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5",
			    "SystName"    : "d598026a9cbc60505f138ce53ac78088d582100c196d0f70c7e2538d4a8d7e10", 
			    "OSV"         : "7a40c8835f716a44e947b12462842142146dec18cf742fe9ecd71ccba07c8883", 
			    "Node"        : "773571a53ef6cbff8daa68ad1c7c4015c404c720f92743850033a6d1a8a13fdd",
			    "Platform"    : "04f0854c8a1ac92fe2f9d608946a10fedfac29bdcde3223ca328aeec7f548d19", 
			    "Processor"   : "5c868b2ccafe48d310ae2602fd9e5383b4c9dba71413457daa0ffe22f99ffee2",
		  	},

			"clientIrDvd": { # Ir David
				# 2022
				# "Architecture": "a172b1b10d1a3a96fde145abe492c0476b214a2e0622056ba1615acebd042dae",
				# "Machine"     : "558b624e3a60d3e4a393272854dbbd35c6fea3f18ee692235dcbe31be8370700",
				# "OSR"         : "4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5",
				# "SystName"    : "d598026a9cbc60505f138ce53ac78088d582100c196d0f70c7e2538d4a8d7e10",
				# "OSV"         : "6ba443c2b3525afbbac2cf0c928e8466e77845aeff95b7a3656028c5d3c0a3c5",
				# "Node"        : "6053ac1fe471748f8ab9d902d674cf94e41ae39577b88a96084d27973ec6c759",
				# "Platform"    : "ccf6e7a8db95136dfa9d8c3ded0fd57b9fbf068e66d1f891acd89ce177528d09",
				# "Processor"   : "16240a781193a5ef944bc987d9d701dbc6106a0750186f7d0ae52101b5df17bc",
				
				# 2023
				"Architecture": "a172b1b10d1a3a96fde145abe492c0476b214a2e0622056ba1615acebd042dae",
				"Machine"     : "558b624e3a60d3e4a393272854dbbd35c6fea3f18ee692235dcbe31be8370700",
				"OSR"         : "4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5",
				"SystName"    : "d598026a9cbc60505f138ce53ac78088d582100c196d0f70c7e2538d4a8d7e10",
				"OSV"         : "6ba443c2b3525afbbac2cf0c928e8466e77845aeff95b7a3656028c5d3c0a3c5",
				"Node"        : "6053ac1fe471748f8ab9d902d674cf94e41ae39577b88a96084d27973ec6c759",
				"Platform"    : "ccf6e7a8db95136dfa9d8c3ded0fd57b9fbf068e66d1f891acd89ce177528d09",
				"Processor"   : "16240a781193a5ef944bc987d9d701dbc6106a0750186f7d0ae52101b5df17bc",
			},
			
			"NgoCap001": {
				"Architecture": "a172b1b10d1a3a96fde145abe492c0476b214a2e0622056ba1615acebd042dae",
				"Machine"     : "558b624e3a60d3e4a393272854dbbd35c6fea3f18ee692235dcbe31be8370700",
				"OSR"         : "4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5",
				"SystName"    : "d598026a9cbc60505f138ce53ac78088d582100c196d0f70c7e2538d4a8d7e10", 
				"OSV"         : "d57dd400b6dddc0efde818fc2314c0c281531ac80a2016ba3c02bbf8855b8d96", 
				"Node"        : "e2dbdf46b64caa78bddf1b92b89308c9d64ca4442ce0c989d1189930e5af4c7d",
				"Platform"    : "149d48170fdad2ecc203f016407f40002a7f32f869d9e83c0affc2c9d1d1910e", 
				"Processor"   : "c99f9797018fa27b086fc0c44feedde27e0930ac431fb433318496801803d450",
			},
			
			"Romain": {
				# "Architecture": "a172b1b10d1a3a96fde145abe492c0476b214a2e0622056ba1615acebd042dae",
				# "Machine"     : "558b624e3a60d3e4a393272854dbbd35c6fea3f18ee692235dcbe31be8370700",
				# "OSR"         : "4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5",
				# "SystName"    : "d598026a9cbc60505f138ce53ac78088d582100c196d0f70c7e2538d4a8d7e10", 
				# "OSV"         : "4906553c99e9225413bacd029603f2549fe8d972bf389770063f3e932b623d80", 
				# "Node"        : "8d2ba82b66768b6661310d3cd798c83c5620c539167fbb61a2f310666939f1aa",
				# "Platform"    : "40aac1ba637ba1b447cb69dd44f1b565fa486fda0743b0cfb6ca74f7eb2b5ecf", 
				# "Processor"   : "079f141f61a3613b313c7d930741ca9cdf4e074af30651e700da0774a6fe26be",
				
				# 2023.10.07
				"Architecture": "a172b1b10d1a3a96fde145abe492c0476b214a2e0622056ba1615acebd042dae",
				"Machine"     : "558b624e3a60d3e4a393272854dbbd35c6fea3f18ee692235dcbe31be8370700",
				"OSR"         : "4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5",
				"SystName"    : "d598026a9cbc60505f138ce53ac78088d582100c196d0f70c7e2538d4a8d7e10",
				"OSV"         : "d57dd400b6dddc0efde818fc2314c0c281531ac80a2016ba3c02bbf8855b8d96",
				"Node"        : "8d2ba82b66768b6661310d3cd798c83c5620c539167fbb61a2f310666939f1aa",
				"Platform"    : "149d48170fdad2ecc203f016407f40002a7f32f869d9e83c0affc2c9d1d1910e",
				"Processor"   : "079f141f61a3613b313c7d930741ca9cdf4e074af30651e700da0774a6fe26be",
			},
		}


	def unauthorizedDevice(self):
		heading = "Unauthorized Device"
		# heading += f" {devConfKey}"
		#strText = heading
		strText  = f"Il semble que cet appareil n'est pas celui qui avait été sélectionné et configuré "
		strText += f"pour le fonctionnement du logiciel '{self.getAppName()}'\n\nVeuillez nous contacter."
		strText += f"\n\nDeveloped by CSMART LAB\n{self.devTeam['mobile']} | {self.devTeam['email']}"
		#if self.adminMode:
		if True:
			#self.updateStatusBar(strText)
			print(strText)
		dummyWindow = tk.Tk()
		dummyWindow.title("Warning Window")
		showwarning(heading, strText) #, icon="warning")
		dummyWindow.destroy()
		heading = strText = None
		return


	def isItTheRegisteredDevice0(self):
		#TODO
		#if self.adminMode:
		#self.WIP("isItTheRegisteredDevice()")
		# MAC
		# Processor
		# Node
		# OS version
		#self.isValidDevice = True
		configuredDeviceSignature = "f551f11538ff11e160450effa0c16bb70d283cce4dbf21ffe9cce68f360f62db" # Mx Ok
		
		# Received :
		# 3c196486db8b4e52b9d90039db4477c89cdc74be348da2c4bcf5139468668697  Device
		# f551f11538ff11e160450effa0c16bb70d283cce4dbf21ffe9cce68f360f62db  Configured
		# f551f11538ff11e160450effa0c16bb70d283cce4dbf21ffe9cce68f360f62db

		currentDevice = {
			# "MAC": "A",
			# "MAC": getMacWithUUID(),
			"Architecture": "B",
			"Architecture": platform.architecture()[0],
			"Machine": "C",
			"Machine": platform.machine(),
			"OSR": "D",
			"OSR": platform.release(),
			"SystName": "E",
			"SystName": platform.system(),
			"OSV": "F",
			"OSV": platform.version(),
			"Node": "G",
			"Node": platform.node(),
			"Platform": "H",
			"Platform": platform.platform(),
			"Processor": "I",
			"Processor": platform.processor()
	    }
	    
		if self.adminMode:
			#if True: 
			print(f"Current Device: {currentDevice}\n\n")

		currentDeviceSignature = "" #None
		#if True:
		if self.adminMode:		
			print("About to Encrypt ...")
		for key in currentDevice:
			
			hashedSegment = self.hash_password(currentDeviceSignature + currentDevice[key])
			currentDeviceSignature = hashedSegment
			#print(f"{key}\t{currentDevice[key]}\t{hashedSegment}\t{currentDeviceSignature}")
			if self.adminMode:
				print(f"\t{hashedSegment}\t{currentDeviceSignature}\t{key}\t{currentDevice[key]}")
	
			else:
				#print(f"Is it the Configured Device ? : { 7 }")
				#if self.adminMode:
				self.isValidDevice = self.check_password_hash(currentDeviceSignature, configuredDeviceSignature)      
				#if True:
				if self.adminMode:
					print(f"Is it the Configured Device ? : { self.isValidDevice }")
	
	    #self.isValidDevice = self.check_password_hash(currentDeviceSignature, configuredDeviceSignature)

		if not self.isValidDevice:
			heading = "Incompatible Device"
			strText = f"Il semble que cet appareil n'est pas celui qui avait été sélectionné et configuré pour le fonctionnement du logiciel \'{self.getAppName()}\'"
			#strText = "The Trial period has expired"
			if True: #self.adminMode:
				print(strText)
			showinfo(heading, strText, icon="warning")
			# self.isValidDevice = False
			# return self.isValidDevice
		
		return self.isValidDevice


# def getMacWithUUID(self):
# #def getMacWithUUID():
#   deviceMacAddress = get_mac()
#   value = hex(deviceMacAddress)
#   #self.updateCompleteSysInfo(f"{value}") #0xdf8aa5c603d2
#   value = ':'.join(("%012X" % deviceMacAddress)[i:i+2] for i in range(0, 12, 2))
#   #self.updateCompleteSysInfo(f"From UUID : {value}") # DF:8A:A5:C6:03:D2
#   return value

# def hash_password(self, plainValue):
# #def hash_password(password):
#   if self.adminMode:
#     encryptedValue = hashlib.sha256(str.encode(plainValue)).hexdigest()
#     print(f"# {plainValue} ---> {encryptedValue}")
#     return encryptedValue

#   return hashlib.sha256(str.encode(plainValue)).hexdigest()


# def check_password_hash(self, currentDeviceSignature, configuredDeviceSignature):
# #def check_password_hash(password, hash):
#   if self.adminMode:
#     print(f"Received :\n{currentDeviceSignature}\tDevice\n{configuredDeviceSignature}\tConfigured")
#   #self.isValidDevice = hash_password(password) == configuredDeviceSignature
#   self.isValidDevice = currentDeviceSignature == configuredDeviceSignature
#   if self.adminMode:
#     print(f"Device Verdict: {self.isValidDevice}")
#   return self.isValidDevice


	def isItTheRegisteredDevice1(self):
		global testerMode
		if testerMode:
			self.showTesterMode()
			self.isValidDevice = True
			return True

		#devConfKey = "windows" if adminMode else "client"
		self.loadAuthorizedDevices()
		configuredDevice : dict = self.configuredDevices[devConfKey]
		
		# TODO : self.currentDevice
		currentDevice : dict = {
			# "MAC": getMacWithUUID(), # "MAC": "A",
			"Architecture": platform.architecture()[0], #"Architecture": "B",
			"Machine": platform.machine(), #"Machine": "C",
			"OSR": platform.release(), #"OSR": "D",  
			"SystName": platform.system(), #"SystName": "E",
			"OSV": platform.version(), #"OSV": "F",
			"Node": platform.node(), #"Node": "G",
			"Platform": platform.platform(), #"Platform": "H",
			"Processor": platform.processor() #"Processor": "I",
		}
		
		#if self.adminMode:
		if True:
			print(f"---> Current Device : {currentDevice}")
			print(f"---> Configured Device : {configuredDevice}\n\n")
			print("About to Encrypt ...\n")

		currentDeviceSignature = "" #None
		
		self.isValidDevice = False
		for key in currentDevice:
			# hashedSegment = self.hash_password(currentDeviceSignature + currentDevice[key])
			# currentDeviceSignature = hashedSegment
			# if self.adminMode:
			#   print(f"\t{hashedSegment}\t{currentDeviceSignature}\t{key}\t{currentDevice[key]}")
			# #
			
			# hashedSegment = hash_password(currentDeviceSignature + currentDevice[key])
			# currentDeviceSignature = hashedSegment
			
			# #hashedSegment = hash_password(currentDevice[key])
			# hashedSegment = self.hash_password(currentDevice[key])
			# currentDeviceSignature += hashedSegment
			# #if True:
			# if self.adminMode:
			#   print(f"\t{hashedSegment}\t{key}\t{currentDevice[key]}\n\t{currentDeviceSignature}\n")
			
			#Platform : 'Linux-5.13.0-27-generic-x86_64-with-glibc2.14' - 'ea52f894f800db03a4a8edde9899476048b31a298d8a275679ee95a9e551195a' <---------- 12fed75aeca6063a71527a9665ff5b91e55ceebb3f7d0167a878079fbd819dc6
			
			#### Cutting it short
			encryptedSegment = self.quickEncrypt(currentDevice[key])
			
			
			# TODO
			# > Dictionary Comparaison
			currentDevice[key] = encryptedSegment

			strText = f"{key} : '{currentDevice[key]}' > '{encryptedSegment}' | Configured for: '{configuredDevice[key]}'"
			self.isValidDevice = encryptedSegment == configuredDevice[key]
	
			if not self.isValidDevice:
				strText += f" <---------- MISMATCH"
				#if self.adminMode:
				if True:
			  		print(strText)
				#break
				if not self.adminMode:
			  		break
				#return self.isValidDevice
			strText += " OK"
			#if self.adminMode:
			if True:
				print(strText)
	    	#### End Cutting it short

		else: # No Break in the for loop
			self.isValidDevice =  True #self.check_password_hash(currentDeviceSignature, configuredDeviceSignature)
			#if self.adminMode:
			if True:
				print(f"Is it the Configured Device ? : { self.isValidDevice }")
			#
			# isValidDevice = check_password_hash(currentDeviceSignature, configuredDeviceSignature)
			# if True:
			#   print(f"Is it the Configured Device ? : { isValidDevice }")

		# > Dictionary Comparaison
		print("\n\n\nCompare Dictionaries : ", currentDevice == configuredDevice)
		x = currentDevice
		y = configuredDevice
		shared_items = {k: x[k] for k in x if k in y and x[k] == y[k]}
		print("MATCHING : ", len(shared_items))

		if not self.isValidDevice:
			self.unauthorizedDevice()
		return self.isValidDevice

		
	def getCurrentDeviceConfig(self, encrypted: bool = True):
		if encrypted: # Return as Encrypted Text
			#return currDeviceConfig : dict = {
			#return dict: {
			return {
				"Architecture": self.quickEncrypt(platform.architecture()[0]),
				"Machine": self.quickEncrypt(platform.machine()),
				"OSR": self.quickEncrypt(platform.release()),
				"SystName": self.quickEncrypt(platform.system()),
				"OSV": self.quickEncrypt(platform.version()),
				"Node": self.quickEncrypt(platform.node()),
				"Platform": self.quickEncrypt(platform.platform()),
				"Processor": self.quickEncrypt(platform.processor())
			}
		
		else: # Return as Plain Text
			#return currDeviceConfig : dict = {
			#return dict: {
			return {
				# "MAC": getMacWithUUID(), # "MAC": "A",
				"Architecture": platform.architecture()[0], #"Architecture": "B",
				"Machine": platform.machine(), #"Machine": "C",
				"OSR": platform.release(), #"OSR": "D",  
				"SystName": platform.system(), #"SystName": "E",
				"OSV": platform.version(), #"OSV": "F",
				"Node": platform.node(), #"Node": "G",
				"Platform": platform.platform(), #"Platform": "H",
				"Processor": platform.processor() #"Processor": "I",
			}
		

	def isItTheRegisteredDevice(self):
		# Yes Here
		# TODO : Figure out what was the use for this shitty "testerMode" ;-)
		global testerMode
		######################################################
		
		if testerMode:
			self.showTesterMode()
			self.isValidDevice = True
			return True
		
		self.loadAuthorizedDevices()
		self.isValidDevice = False
		currentDevice = self.getCurrentDeviceConfig()
		if self.adminMode:
			print(f"Refactor : Current Device Config Received : {currentDevice}")
		
		for deviceName in self.configuredDevices:
			#if True: #self.adminMode:
			if self.adminMode:
				print(f"Device Name : {deviceName}")  
			
			if currentDevice == self.configuredDevices[deviceName]:
				self.isValidDevice = True
				break
			else:
				pass

		# TODO : Unsafe practice to bypass the Device verification for Quick Debugging on Mac
		# if platform.system() == "Darwin":
		currentDevice = platform.system()
		if currentDevice == "Darwin":
			self.isValidDevice = True
			self.isValidDevice = True
			print(f"Running on {currentDevice}")

		print(f"VERDICT : {self.isValidDevice }\n")
				
		print("Fn Cp !") # Fin Coop
		###############################################    
		if not self.isValidDevice:
			self.unauthorizedDevice()
		return self.isValidDevice


	def isValidTrial(self):
		global testerMode
		if testerMode:
			strText = "Tester Mode"
			self.showTesterMode()
			self.validTrial = True
			return self.validTrial

		try:
			# #dateNow = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
			# dateSnapShot = datetime.now() #.strftime('%Y-%m-%d %H:%M:%S')
			# dateNow = [
			#   dateSnapShot.strftime('%Y'),
			#   dateSnapShot.strftime('%m'),
			#   dateSnapShot.strftime('%d')
			# ]
			# print("Date Now : ", dateNow)
			# deadline = ['2022', '02', '01']
			# print("Deadline : ", deadline)			
			
			#segments = [] # yy, mm, dd
			# for i in range(3):
			#   if i == 2:
			#     if ( deadline[i] < int(dateNow.split("-")[2].split(" ")[0]) ):
			#       self.validTrial = False
			#       self.trialOver()
			#       exit()
			#     else:
			#      break
			#   if (deadline[i] < int(dateNow.split("-")[i]) ):
			#     self.validTrial = False
			#     self.trialOver()
			#     exit()
			#   else:
			#     print( f"OK with {dateNow.split('-')[i]}" )


			# for i in range(3):
			#   #if self.adminMode:
			#   print(f"NOW at : {deadline[i]} --- {dateNow[i]}")
			#   if int(deadline[i]) < int(dateNow[i]):
			#     self.validTrial = False
			#     if self.adminMode:
			#       print(f"Stopping here : {deadline[i]} --- {dateNow[i]}")
			#     self.trialOver(f"\n===> [{ '-'.join(deadline) }]") #( f"STOP at : {deadline[i]} --- {dateNow[i]}" )
			#     exit()
			# else: # No Interruption :  Invalid Cases
			#   #pass
			#   self.validTrial = True
			
			#dateNow = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

			todayDateSnapShot = datetime.now() #.strftime('%Y-%m-%d %H:%M:%S')
			if True: #self.adminMode:
				print("Date Now : ", todayDateSnapShot)

			#deadlineTuple = (2022, 2, 10)
			
			#deadline = datetime(2022, 1, 26) # Tshanoza
			#deadline = datetime(2022, 2, 10) # Client
			deadline = datetime(deadlineTuple[0], deadlineTuple[1], deadlineTuple[2]) # Client
			#deadline = datetime(2022, 1, 1) # Expired
			
			#if True: #self.adminMode:
			if self.adminMode:
				print("Deadline : ", deadline)
			self.deadline = deadline
			
			self.validTrial = todayDateSnapShot <= deadline
			if True: #self.adminMode:
				print(f"Valid Trial Period ? : {self.validTrial}")

			if not self.validTrial:
				#self.trialOver(f"\n===> [{ '-'.join(deadline) }]")
				self.trialOver(f"\n=> [{ deadline }]")
				#self.trialOver(f"\n===> [{ deadline.strip(' ') }]")
				#self.trialOver(f"\n===> [{ deadline.strip()[0] }]")
				exit()

		except Exception as e: ################### AU REVOIR
			strText = f"The Trial period has expired\n{e}"
			if True: #self.adminMode:
				print(strText)
				self.validTrial = False
			# else:
			#   self.validTrial = True
		finally:
			if True: #self.adminMode:
				print(f"> TRIAL ? {self.validTrial}")

#   print("Captured: ", segments) #yy, mm, dd)
#   if segments == deadline:
#     exit()
	
	def authentification(self):
		self.getPasswordFromUser()
		return self.Auth

  
	def hash_password(self, password):
		return hashlib.sha256(str.encode(password)).hexdigest()

  
	def getAppPassword(self, username): # patch    
		self.appPass = "96430b62e77b205f4e6294bb68ee793e6c9203b193a5dad6c42eef905c977ce0"
		cnx = sqlite3.connect(dbName)
		cursor = cnx.cursor()
		# cursor.execute("SELECT userPass FROM users WHERE userName = ?", (self.getCurrentUser(),))
		# cursor.execute("SELECT userPass FROM users WHERE userName = ?", ("David",))
		sqlFindUserPass = "SELECT userPass FROM users WHERE userName = ? AND userStatus= 'ACTIVE'"
		cursor.execute(sqlFindUserPass, (username,))
		
		# result = cursor.fetchall()
		# print("DVD PSS : ", result[0][0])
		result = cursor.fetchone()
		print(f"Result is {result}")
		# if len(result) == 0:
		if result == None:
			heading = "Username Not Found !"
			text = "Nom d'Utilisateur Incorrect"
			showwarning(heading, text)
			exit()

		print("USR PSS : ", result[0])
		self.appPass = self.quickEncrypt(result[0])
		self.currentUser = username
		
		if self.adminMode:
			print(f"<{self.appPass}>")


	def getPasswordFromUser(self, uName = None, uPass=None): # patch
		self.getAppPassword(uName)
		if self.adminMode:
			print(type(uPass))

		if uPass == None:
			uPass = input("Mot de passe de l'Application: ").strip()
		
		if True: #self.adminMode:
			print(f"<{uPass}>")
    
		#print("Compare :", uPass == self.appPass)
		# if(uPass == self.appPass):
		#   return True
		# else:
		#   return False
		
		#self.Auth 
		if self.hash_password(uPass) == self.appPass:
			self.wrongPass = 0
		else:
			self.wrongPass += 1
    
		self.appPass = None
		return self.wrongPass


	def resetAppPassword(self): # TODO
		pass
######## End of class COMBINE


class SVGD():
	def __init__(self): #, master):
		super().__init__()
		
		#self.adminMode = True if adminMode else False
		self.adminMode : bool = adminMode
		self.testerMode: bool = testerMode
		
		if True: #self.adminMode:
			print("FE: init()")

		self.isTrialPeriod = isTrialPeriod
		
		#self.be = combine.COMBINE()
		self.db = DB_SVGD()
		self.combine = COMBINE()
		
		if True: #self.adminMode:
			print("### UI Trial Test")
		#if self.validTrial:
		#if self.combine.validTrial and True:
		if self.combine.validTrial and self.combine.isValidDevice:
			if True: #self.adminMode:
				print(f"### UI Trial Test:\nValid Trial Period ? : {self.combine.validTrial}\nValid Device ? : {self.combine.isValidDevice}")
			self.logInUser()
		else:
			# strText = "Votre période d'essaie a éxpirée"
			# #strText = "The Trial period has expired"
			# print(strText)
			# showinfo(strText, strText)
			return
		
		# if self.combine.authentification():
		#   self.logInPage.destroy()
		#   print("Welcome")
		# else:
		#   print("Wrong Pass")
		#   exit()

	
	def refreshScreen(self):
		self.master.update_idletasks()
	
	
	def getTimeStamp(self):
		#return time.time()
		return time.perf_counter()
	
	
	def tic(self):
		# import time
		# global startTime
		# startTime = time.time()
		self.__startTime__ = self.getTimeStamp()
		return self.__startTime__

	
	def toc(self, testName=None):
		self.__endTime__ = self.getTimeStamp()
		self.__turnAroundTime__ = self.__endTime__ - self.__startTime__ 
		
		if self.adminMode:
			print("Start Time : ", self.__startTime__)
			print("End   Time : ", self.__endTime__)

		strText = f"{self.__turnAroundTime__} second(s)"
		if testName:
			print(f"{testName} - Turn-around time : {strText}")
		return strText


	def toggleDisableEnableWidget(self, targetWidget = None, turnActive=True):
		# TODO Check for a proper widget
		if targetWidget == None:
			showwarning("No Widget Received", "Unable to operate as No Widget has been received")
			return
		if self.adminMode:
			self.updateStatusBar("Toggling Btn 'Afficher'")
		
		if turnActive:
			targetWidget['state'] = tk.NORMAL
			return
		
		targetWidget['state'] = tk.NORMAL if targetWidget['state'] == tk.DISABLED else tk.DISABLED
		return


	def PASC(self, value=None):
		strText = "Pretty Awkward Scenario:"
		if value != None:
			strText += " {value}"
		if self.adminMode:
			print(strText)

	def CTkLabel(self):
		return False


	def CTkEntry(self):
		return False


	def CTkLabel(self):
		return False


	def showTesterMode(self):
		strText = "Tester Mode"
		showinfo(strText, f"The app is running in '{strText}'")
		return


	def WIP(self, featureName=None, popUp=False, futEnhancement=False):
		if featureName == None:
			showinfo("Feature Not Set", "Enter the name of the feature in WIP")
			return
		strText = f"[W.I.P.] Work In Progress for the feature:\n'{featureName}'"
		if futEnhancement:
			#strText += "\n\nCette fonctionnalité peut être ajoutée sur base de votre demande\n\n"
			strText = "\n\nCette fonctionnalité peut être ajoutée sur base de votre demande\n\n"
		print(strText)
		if popUp:
			showinfo("Work In Progress", strText)


	def getAppPassword(self): # patch
		pass
		# TODO: Get it from the SQLite3 DB
		# return "SvgD2021"

		# cnx = sqlite3.connect()
		# self.dbConnexion()
		# input("Cnx Pass ?")


	def getAppName(self): # patch
		# TODO: Get it from the SQLite3 DB
		return "'SVGD COMBINE'"


	def getAppVersion(self): # patch
		# TODO: Get it from the SQLite3 DB
		
		# v0 : CLI
		# v1 : Tkinter
		# v2 : customTkinter
		# v3 : flet
		
		#return "0.0.1" # CLI version
		#return "1.0.1"
		#return "1.0.2"
		#return "1.0.3" # Fixes One build for all configured devices
		return "1.0.4" # Users can reset their own password - Only David for now and default pass for all


	def resetPassword(self):
		self.WIP("Reset Password") # TODO DB


	def newPassword(self):
		self.WIP("New Password") # TODO DB


	def isValidPassword(self):
		self.WIP("Valid Password") # TODO DB


	def isCorrectPassword(self, usernameEntryWidget, passwordEntryWidget): # AWAAAA
		if self.adminMode:
			print(type(usernameEntryWidget), print(passwordEntryWidget))
		auth = self.combine.getPasswordFromUser(usernameEntryWidget.get(), passwordEntryWidget.get())
		if not auth: 
			self.Auth = not bool(auth)
			self.logInPage.destroy()
			print("Welcome")
			self.createMainWindow()
		else:
			errorMsg =f"Wrong Password : [Attempt {auth}]"
			print(errorMsg)
			showinfo(errorMsg, f"Mauvais mot de passe\n", icon="warning") #[ {auth} / 3 ]")
			if auth >= 3:
				exit()


	def incrementRow(self):
		self.uiRow += 1


	def uiResetpassword(self):
		self.resetPasswordPage = tk.Tk()
		self.resetPasswordPage.title("Changer le Mot de Passe")
		self.setColours()
		self.setDevTeam()
		self.resetPasswordPage.configure(background=self.colourDarkBlue)
		self.centeredWindow(self.resetPasswordPage, 400, 80)		
		self.resetPasswordPage.resizable(width=False, height=False)

		self.uiRow = 0

		### Username Entry
		rpassLblUsername = tk.Label(self.resetPasswordPage, text="Nom de l' utilisateur", width=30)
		#rpassLblUsername.pack(pady="10", padx="10", side=tk.LEFT)
		rpassLblUsername.grid(row=self.uiRow, column="0")

		# self.rpassLblEntryUsername = tk.Entry(self.resetPasswordPage, state=tk.DISABLED, width=20)
		# self.rpassLblEntryUsername.insert(tk.END, "David") # TODO Dynamically picked up
		# #self.rpassLblEntryUsername.pack(pady="10", side=tk.RIGHT)
		# self.rpassLblEntryUsername.grid(row=self.uiRow, column="1")

		rpassLblUsernameText = tk.Label(self.resetPasswordPage, text="David", width=20)
		rpassLblUsernameText.grid(row=self.uiRow, column="1")

		# self.incrementRow()
		# ### Current Password Entry
		# rpassLblCurrPassword = tk.Label(self.resetPasswordPage, text="Actuel Mot de Passe", width=20)
		# #rpassLblCurrPassword.pack(pady="20", side=tk.LEFT)
		# rpassLblCurrPassword.grid(row=self.uiRow, column="0")

		# self.rpassEntryCurrPassword = tk.Entry(self.resetPasswordPage, show="*", width=20)
		# #self.rpassEntryCurrPassword.pack(pady="10", side=tk.RIGHT)
		# self.rpassEntryCurrPassword.grid(row=self.uiRow, column="1")
		
		# self.incrementRow()
		# ### NEW Password Entry
		# rpassLblNewPassword = tk.Label(self.resetPasswordPage, text="Saisir le Nouveau Mot de Passe", width=20)
		# #rpassLblConfPassword.pack(pady="30", side=tk.LEFT)
		# rpassLblNewPassword.grid(row=self.uiRow, column="0")

		# self.rpassEntryNewPassword = tk.Entry(self.resetPasswordPage, show="*", width=20)		
		# # TODO: https://python-course.eu/tkinter/events-and-binds-in-tkinter.php
		# # self.rpassEntryNewPassword.bind('<Return>', lambda entryWidget=self.rpassEntryNewPassword : self.collectUserCredentialsFromUI())
		# self.rpassEntryNewPassword.grid(row=self.uiRow, column="1")

		self.incrementRow()
		### Confirm New Password
		rpassLblConfPassword = tk.Label(self.resetPasswordPage, text="Confirmer le Nouveau Mot de Passe", width=30)
		#rpassLblConfPassword.pack(pady="30", side=tk.LEFT)
		rpassLblConfPassword.grid(row=self.uiRow, column="0")

		self.rpassEntryConfPassword = tk.Entry(self.resetPasswordPage, show="*", width=20)
		# TODO: https://python-course.eu/tkinter/events-and-binds-in-tkinter.php
		self.rpassEntryConfPassword.bind('<Return>', lambda entryWidget=self.rpassEntryConfPassword : self.collectUserCredentialsFromUI())
		#self.rpassEntryConfPassword.pack(pady="10", side=tk.RIGHT)
		self.rpassEntryConfPassword.grid(row=self.uiRow, column="1")
		
		self.incrementRow()
		btnYesResetPassword = tk.Button(self.resetPasswordPage, text="OK", width=40, command=lambda: self.collectUserCredentialsFromUI(), bg="#000", fg="#EEE")
		#btnYesResetPassword.pack()
		btnYesResetPassword.grid(row=self.uiRow, column="0", columnspan="2")


	def collectUserCredentialsFromUI(self):
		if not self.rpassEntryConfPassword.get(): # TODO
		#if self.rpassEntryNewPassword.get() != self.rpassEntryConfPassword.get():
			print("Password Values Not Identical")
			return

		theNewPassword = self.rpassEntryConfPassword.get()

		currentUserCredentials = {
			"username": "David", #self.rpassLblEntryUsername.get(), 
			"currentPassword": "JustSomeRubishToPassTheTest", # self.rpassEntryCurrPassword.get(), # TODO
			"newPassword": theNewPassword
		}
		print(f"Object Formatted: {currentUserCredentials}")
		self.db.initResetPassword(currentUserCredentials)
		#self.combine.db.initResetPassword(currentUserCredentials)
		self.resetPasswordPage.destroy()


	def logInUser(self):
		# TODO:
		# 1. Collect logIn Timestamp to be compared with previous ones : now > previous
		# 2. Reset password
		self.logInPage = tk.Tk()
		self.logInPage.title("Connexion à l'application")
		self.setColours()
		self.setDevTeam()
		self.logInPage.configure(background=self.colourDarkBlue)
		#self.logInPage.configure(background=self.colourAliceBlue)
		
		#self.logInPage.geometry("350x80+100+100")
		
		# self.centeredWindow(self.logInPage, 400, 150) #200)
		self.centeredWindow(self.logInPage, 320, 150) #200)
		#self.centeredWindow(self.logInPage, 350, 150) # with Image
		
		self.logInPage.resizable(width=False, height=False)

		# 2. Reset password
		# btnResetPassword = tk.Button(self.logInPage, state=tk.DISABLED, text="Reset Password", width=10, command=self.resetPassword)
		# btnResetPassword.pack(side=tk.LEFT)

		global testerMode
		if self.adminMode or testerMode:
			self.demoPassword = self.getAppPassword()
		else:
			self.demoPassword = "You seriously though that I would keep the application password here? Really ;-)"
			#self.demoPassword = "SvgD2021"
		
		# imageName = "images/Cnk.jpg"
		# imageName = "images/maths.jpg"
		# imageName = "images/giphy_maths001.gif"
		# imageName = "images/Cnk.png"

		#self.imgLogIn = tk.PhotoImage(file=imageName)

		# sudo apt-get purge python3-pil.imagetk
		# sudo apt-get install python-imaging-tk 
		# 
		# selectedImage = Image.open(imageName)
		# self.imgLogIn = ImageTk.PhotoImage(selectedImage)

		# self.lblLogIn = tk.Label(self.logInPage, image=self.imgLogIn, width=150, height=50)
		# self.lblLogIn.pack()

		### Username Entry
		# entryUsername = tk.Entry(self.logInPage, width=20)
		# entryUsername.pack(pady="10")

		defaultUser: str = "David"
		entryUsername = tk.Entry(self.logInPage, width=20)
		entryUsername.insert(tk.END, defaultUser)
		entryUsername.pack(pady="10")

		entryPassword = tk.Entry(self.logInPage, show="*", width=20) #, fg=self.colourWhite) #, bg=self.colourDarkBlue) #"blue")
		entryPassword.insert(tk.END, self.demoPassword) #real_password) # "") #"Fake Password")
		# TODO: https://python-course.eu/tkinter/events-and-binds-in-tkinter.php
		entryPassword.bind('<Return>', lambda entryWidget=entryPassword : self.isCorrectPassword(entryUsername, entryPassword))
		entryPassword.pack(pady="10")
		
		btnConfirmPassword = tk.Button(self.logInPage, text="OK", width=10, command=lambda: self.isCorrectPassword(entryUsername, entryPassword))
		btnConfirmPassword.pack()

		# btnResetPassword = tk.Button(self.logInPage, state=tk.DISABLED, text="Reset Password", width=10, command=self.resetPassword)
		# #btnResetPassword = tk.Button(self.logInPage, text="Reset Password", width=10, command=self.resetPassword)
		# btnResetPassword.pack(side=tk.LEFT, pady="10")

		self.createStatusBar(self.logInPage)
		self.logInPage.mainloop()


	def logInUser(self):
		# TODO:
		# 1. Collect logIn Timestamp to be compared with previous ones : now > previous
		# 2. Reset password
		self.logInPage = tk.Tk()
		self.logInPage.title("Connexion à l'application")
		self.setColours()
		self.setDevTeam()
		self.logInPage.configure(background=self.colourDarkBlue)
		#self.logInPage.configure(background=self.colourAliceBlue)
		
		#self.logInPage.geometry("350x80+100+100")
		
		# self.centeredWindow(self.logInPage, 400, 150) #200)
		self.centeredWindow(self.logInPage, 320, 150) #200)
		#self.centeredWindow(self.logInPage, 350, 150) # with Image
		
		self.logInPage.resizable(width=False, height=False)

		# 2. Reset password
		# btnResetPassword = tk.Button(self.logInPage, state=tk.DISABLED, text="Reset Password", width=10, command=self.resetPassword)
		# btnResetPassword.pack(side=tk.LEFT)

		global testerMode
		if self.adminMode or testerMode:
			self.demoPassword = self.getAppPassword()
		else:
			self.demoPassword = "You seriously though that I would keep the application password here? Really ;-)"
			#self.demoPassword = "SvgD2021"
		
		# imageName = "images/Cnk.jpg"
		# imageName = "images/maths.jpg"
		# imageName = "images/giphy_maths001.gif"
		# imageName = "images/Cnk.png"

		#self.imgLogIn = tk.PhotoImage(file=imageName)

		# sudo apt-get purge python3-pil.imagetk
		# sudo apt-get install python-imaging-tk 
		# 
		# selectedImage = Image.open(imageName)
		# self.imgLogIn = ImageTk.PhotoImage(selectedImage)

		# self.lblLogIn = tk.Label(self.logInPage, image=self.imgLogIn, width=150, height=50)
		# self.lblLogIn.pack()

		### Username Entry
		# entryUsername = tk.Entry(self.logInPage, width=20)
		# entryUsername.pack(pady="10")

		defaultUser: str = "David"
		entryUsername = tk.Entry(self.logInPage, width=20)
		entryUsername.insert(tk.END, defaultUser)
		entryUsername.pack(pady="10")

		entryPassword = tk.Entry(self.logInPage, show="*", width=20) #, fg=self.colourWhite) #, bg=self.colourDarkBlue) #"blue")
		entryPassword.insert(tk.END, self.demoPassword) #real_password) # "") #"Fake Password")
		# TODO: https://python-course.eu/tkinter/events-and-binds-in-tkinter.php
		entryPassword.bind('<Return>', lambda entryWidget=entryPassword : self.isCorrectPassword(entryUsername, entryPassword))
		entryPassword.pack(pady="10")
		
		btnConfirmPassword = tk.Button(self.logInPage, text="OK", width=10, command=lambda: self.isCorrectPassword(entryUsername, entryPassword))
		btnConfirmPassword.pack()

		# btnResetPassword = tk.Button(self.logInPage, state=tk.DISABLED, text="Reset Password", width=10, command=self.resetPassword)
		# #btnResetPassword = tk.Button(self.logInPage, text="Reset Password", width=10, command=self.resetPassword)
		# btnResetPassword.pack(side=tk.LEFT, pady="10")

		self.createStatusBar(self.logInPage)
		self.logInPage.mainloop()


	#def centeredWindow(self, windowWidgetToBeCentered=self.master, windowWidth=self.windowWidth, windowHeight=self.windowHeight):
	#def centeredWindow(self, windowWidgetToBeCentered, windowWidth, windowHeight):
	def centeredWindow(self, windowWidgetToBeCentered=None, windowWidth=None, windowHeight=None):
		if self.adminMode:
			print("Centering the Window ...")
		
		if windowWidgetToBeCentered == None:
			windowWidgetToBeCentered = self.master

		if windowWidth:
			self.appMinWidth = int(float(windowWidth))		# Just to make sure we are dealing with an "int" value
		else:
			windowWidth = self.appMinWidth
			
		if windowHeight:
			self.appMinHeight = int(float(windowHeight))	# Just to make sure we are dealing with an "int" value
		else:
			windowHeight = self.appMinHeight

		# monitorWidth = self.master.winfo_screenwidth()
		# monitorHeight = self.master.winfo_screenheight()
		monitorWidth = windowWidgetToBeCentered.winfo_screenwidth()
		monitorHeight = windowWidgetToBeCentered.winfo_screenheight()

		xPadding = (monitorWidth	/ 2) - (windowWidth	/ 2)
		yPadding = (monitorHeight / 2) - (windowHeight / 2)

		xPadding = int(xPadding)
		yPadding = int(yPadding)

		#windowWidgetToBeCentered.geometry(f"{windowWidth}x{windowHeight}+{xPadding}+{yPadding}")
		#self.master.geometry(f"{self.appMinWidth}x{self.appMinHeight}+{xPadding}+{yPadding}")
		windowWidgetToBeCentered.geometry(f"{windowWidth}x{windowHeight}+{xPadding}+{yPadding}")


	def createMainWindow(self):
		if self.adminMode:
			print("Creating the main UI")

		self.instanciation()		
		self.centeredWindow()
		self.createWindowMenuBar()
		self.createWindowContent()
		# Run mainloop()
		if self.adminMode:
			print("# FrontEnd : Ready for further instructions.")
		self.master.mainloop()

	# def drawMainWindow(self):
	#	 self.createMenuBar()
	#	 self.createWindowContent()
	#	 self.addCloseButton()
	#	 self.createStatusBar()

	#	 # Run mainloop()
	#	 self.master.mainloop()


	def instanciation(self):
		self.WIP("instanciation")
		self.master = tk.Tk()
		self.setAppDetails()
		self.setDevTeam()
		self.essentialInitializations()


	def setDevTeam(self):
		self.devTeam : dict = {
			"name": "CSMART LAB",
			#"mobile": "+919606848849",
			"mobile": "+639392604156",
			"email": "csmartlab@gmail.com",
			"website": "www.csmartlab.org"
		}


	def setAppDetails(self):
		# Configure the root window
		self.setColours()
		self.statusOptions = ["[P.O.C.]", "Test", "Production"]
		self.app_status = None
		self.isProductionVersion = False
		self.app_version = self.getAppVersion()
		self.app_name = self.getAppName()
		self.app_title = f"{self.app_name} - version {self.app_version}"
		
		titleStr: str = f"{self.statusOptions[1]} - {self.app_title}"
		self.master.title(titleStr)
		#pathToIcon = "./images/cnk_W6o_2.ico"
		#self.master.iconbitmap(pathToIcon)
		#self.master.geometry("500x300")
		self.master.configure(background=self.bgColour)
		#self.master.resizable(width=False, height=False)
		
		self.appMinWidth	 = 734 #654
		self.appMinHeight	= 575#s25 #600
		#self.appMinHeight	= 800 # Dummy Test
		
		self.app_rowCounter = 0
		self.row_letters = 0
		self.instructionsCounter = 0


	def setColours(self):
		self.WIP("setColours")
		### APP COLOURS
		self.colourWhite		: str = "#FFF"
		self.colourDark 		: str = "#000"
		self.colourPrimary		: str = "#4065A4"
		self.colourSecondary	: str = "#41B77F"
		self.colourTernary		: str = "#BEADFD"
		self.colourAliceBlue	: str = "AliceBlue"
		self.colourDarkBlue		: str = "#1F2732"
		self.colourGreen		: str = "Green"
		self.colourYellow		: str = "yellow"
		self.colourSystemGrey	: str = "#d9d9d9"
		self.colourSkyBlue		: str = "Sky Blue"
		self.colourRed			: str = "Red"
		self.colourPaleTurquoise: str = "Pale Turquoise"

		#self.bgColour				: str =	self.colourSystemGrey
		# self.bgColour =	self.colourDarkBlue
		# self.bgColour = self.colourWhite
		# self.bgColour: str = "#dedede"
		self.bgColour			: str =	self.colourSkyBlue
		self.fgColour			: str = self.colourDark # self.colourWhite
		self.headingTextColour	: str = self.colourPrimary
		self.colourWarning		: str = self.colourRed


	def essentialInitializations(self):
		self.WIP("essentialInitializations")
		self.counterISecondary: int = 0
		self.currentCombinations	= []
		self.iSecMatches			= ["A", "ANCFH", "AECFH"]
		self.cliOperations 			= ["Instruction Primaire", "Instruction Secondaire", "Fermer le Programme"]
		self.cliOptionsAction 		= ["Garder", "Supprimer"]
		self.cliOptionsBelong 		= ["Ayant", "N’ayant pas"]

		self.nDots: str = 10 * "- "
		# TODO : self.JoinLettersDelimiter = ", "
		s = ""
		self.theAlphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
		print (f"Complete Alphabet ({len(self.theAlphabet)}): {s.join( self.theAlphabet )}")

		self.authOK: bool = False

		self.min_Value 		= 0
		self.minimum_value 	= 1 #0
		self.max_N 			= len(self.theAlphabet) # as defined in the SRS
		self.max_K			= 15	 # as defined in the SRS

		self.m = []
		self.n = None # self.max_N # +1
		self.k = None # self.max_K # +1

		# self.a = self.n // 2
		# self.b = self.k // 2
		# self.p = []

		self.letters = None #self.theAlphabet[:self.n]

		# self.time_tracker = {
		#	 "generation": 0,
		#	 "display": 0,
		#	 "totalExec": 0
		# }


	def addTime(self): # TODO Optimise with getTime()
		self.strVarDate = tk.StringVar()
		self.strVarDate.set(f"Date & Heure {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}") # Todo
		#self.lblDateText = tk.Label(self.heading, textvariable=self.strVarDate) #, bg=self.bgColour, fg=self.fgColour)#
		#self.lblDateText = tk.Label(self.heading, textvariable=self.strVarDate, font=("Helvetica", 13, "bold"), foreground=self.colourWhite, bg=self.bgColour)
		fontTime = Font(family="Times New Roman", size=11, weight="bold")
		#fontTime = Font(family="Helvetica", size=10, weight="bold")
		#fontTime = Font(weight="bold")
		self.lblDateText = tk.Label(self.heading, textvariable=self.strVarDate, font=fontTime, foreground=self.headingTextColour, bg=self.bgColour)
		#self.lblDateText.pack(side=tk.RIGHT)
		self.lblDateText.pack(side=tk.RIGHT, padx=80) #, pady=10)
		self.lblDateText.after(1000, self.getTime)


	def getTime(self):
		self.strVarDate.set(f"Date & Heure {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}")
		self.lblDateText.after(1000, self.getTime)
		return # Debug
		if self.adminMode:
			print(f"... Tic Tac: {self.strVarDate.get()}")


	def createWindowContent(self):
		self.WIP("createWindowContent")
		self.frMain = tk.Frame(self.master).pack()
		self.drawLeftLabelFrame()
		self.drawRightLabelFrame()


	def createWindowHeading(self):
		self.heading = tk.Frame(self.master, bg=self.bgColour)#
		self.heading.pack()
		#self.addCloseButton()
		self.addWelcomeLabel()
		self.addTime()
		
		
	def drawLeftLabelFrame(self):
		self.WIP("drawLeftLabelFrame")
		self.testMode = True
		self.N = len(self.theAlphabet)
		self.K = 15
		self.totalCombos = 7_700_000
		self.instructionsCounter = 0
		self.duration = 0.0000001
		self.receivedLetters = ''.join(self.theAlphabet[:self.K])
		self.nkAlphaDurationCombos = [self.N, self.K, self.receivedLetters, self.duration, [] ]
		
		self.remainingCombos = []
		self.listOfInstructionSets = []
		self.instructionSet = self.emptyInstructionSet()

		self.createWindowHeading()
		self.createStatusBar()
		# self.dateStrVar.set("UPDATED !")
		# print("Date: UPDATED !")
		self.attachIPI()
		self.attachIPC()
		self.attachOperations()
		#self.attachISC()


	def emptyInstructionSet(self):
		 return { "n" : -1, "k" : -1, "letters" : [], "combos": [], "duration": -1, "action" : None, "isKeeping" : None, "belong" : None, "isContaining" : None }

	
	def updatelistOfInstructionSets(self, instructionSetToBeAddedInTheList, popUp=False):	
		self.listOfInstructionSets.append(instructionSetToBeAddedInTheList)
		heading = f"Current Instruction Count : {len(self.listOfInstructionSets)}"
		message = f"This instruction has been added\n{instructionSetToBeAddedInTheList}"
		if self.adminMode:
			print(f"{heading}\n{message}")
		if popUp:
		#if self.adminMode:s
			showinfo(heading, message, icon="info")


	def attachIPI(self):
		self.lblFrWidth = 300
		# LabelFrames
		#lblFrIPC = tk.LabelFrame(frLeft, text="Instruction Principale Courante", bg="blue", width=lblFrWidth)
		self.lblFrIPI = tk.LabelFrame(self.master, text="Instruction Principale Initiale", font=("Helvetica 11 bold"), width=self.lblFrWidth, height=50, bg=self.bgColour, fg=self.fgColour)# bg="blue")
		#lblFrIPC.pack(fill="both", expand="yes", padx=10)
		self.lblFrIPI.pack(fill=tk.X, padx=10, pady=5)
		#lblFrIPC.pack(padx=10)
		self.lblFrIPI.columnconfigure((0, 2), weight=1)
		self.lblFrIPI.columnconfigure(1, weight=2)

		self.lblIPI_N = tk.Label(self.lblFrIPI, text=f"Valeur de N = ?", bg=self.bgColour, fg=self.fgColour)#)#{self.N}")
		#lblIPC_N.pack(side=tk.LEFT, padx=10)
		self.lblIPI_N.grid(row=0, column=0)

		self.lblIPI_Alphabet = tk.Label(self.lblFrIPI, text= self.nDots, bg=self.bgColour, fg=self.fgColour)#) #self.receivedLetters) #, width="26")
		#lblIPC_Alphabet.pack(side=tk.LEFT, padx=10)
		self.lblIPI_Alphabet.grid(row=0, column=1)

		self.lblIPI_K = tk.Label(self.lblFrIPI, text=f"Valeur de K = ?", bg=self.bgColour, fg=self.fgColour)#) #{self.K}")
		#lblIPC_K.pack(side=tk.RIGHT, padx=10)
		self.lblIPI_K.grid(row=0, column=2)

		# Dummy Test with Fake Space
		# lblIPC_space = tk.Label(lblFrIPC) #, text=f"Combinaisons Totales = {totalCombos}")
		# #lblIPC_space.pack(fill="both", expand=True)
		# lblIPC_space.grid(row=1, column=0, rowspan=3)

		self.lblIPI_Combos = tk.Label(self.lblFrIPI, text=f"Combinaisons = ?", bg=self.bgColour, fg=self.fgColour)# #{self.totalCombos}")
		#lblIPC_K.pack(padx=10)
		#
		#lblIPC_K.grid(row=2, column=0)
		self.lblIPI_Combos.grid(row=2, column=0)

		self.lblIPIDuration = tk.Label(self.lblFrIPI, text=f"Durée d'éxécution : ?", bg=self.bgColour, fg=self.fgColour)#) #{self.totalCombos}")
		self.lblIPIDuration.grid(row=2, column=1)

		#self.btnIPI_AfficherCombos = tk.Button(self.lblFrIPI, text="Afficher", command=self.afficherCombos, width=11, state=tk.DISABLED) #, bg=self.colourPrimary)
		self.btnIPI_AfficherCombos = tk.Button(self.lblFrIPI, state=tk.DISABLED, text="Afficher", command=lambda: self.afficherCombos(0), cursor="hand1", width=11) #, bg=self.colourPrimary)
		self.btnIPI_AfficherCombos.grid(row=2, column=2, padx=5, pady=5)

		### Reference
		# len(self.listOfInstructionSets[index]['combos'])
		# len(self.listOfInstructionSets)


	def closeCombosDisplay(self, windowToBeClosed=None):
		try:
			if self.adminMode:
				print(f"Received Widget : '{windowToBeClosed}' to be destroyed.")
			# if windowToBeClosed == None:
			#	 if self.adminMode:
			#		 showwarning("Issue with Treeview deletion", "Unable to delete the Correct Treeview as No Value was passed")
			#	 return

			#tree.insert("", tk.END, text=f"L{combo+1}", values=(3*"-",	5*"-"))
			strText = f"Deleting values from the Treeview ..."
			strText = ""
			self.updateStatusBar(strText)
			
			if self.adminMode:
				strText = f"---> Value of 'self.combosDisplay' : {type(self.combosDisplay)} - {self.combosDisplay}"
				strText += f"\n---> Value of 'self.trvCombosDisplay' : {type(self.trvCombosDisplay)} - {self.trvCombosDisplay}"
				print(strText)

			if not self.combosDisplay:
				if self.adminMode:
					showinfo("Déjà fermé", "Combo Display Déjà Fermée !")
				self.combosDisplay = None
				return

			if not self.trvCombosDisplay:
				self.trvCombosDisplay = None
				return

			for record in self.trvCombosDisplay.get_children():
				self.trvCombosDisplay.delete(record)
			
			strText = f"Done deleting values from the Treeview."
			self.updateStatusBar(strText)
			
			# def trialPeriodReminder()
			if False:
				heading = "Limited Access"
				strText = "Pendant la période d'essaie, le temps d'accès aux résultats est limité."
				#, icon="warning")
				self.updateStatusBar(f"{heading} : {strText}")
				# Temporarely hidden: Tsumbu's request
				showwarning(heading, strText)

			self.combosDisplay.destroy()
			self.combosDisplay = heading = strText = None
			#windowToBeClosed.destroy()
		except Exception as error:
			# TODO : Typically when the window was closed using "x"	button at the of the app
			print(f"Error while closing the Treeview Display : {error}")


	# TODO:
	def trialPeriodReminder(self):
		pass


	def selectComboFromTreeview0(self, treeviewWidget=None):
		if treeviewWidget == None:
			treeviewWidget = self.trvCombosDisplay
		curItem=treeviewWidget.focus()
		values= treeviewWidget.item(curItem,"values")
		if self.adminMode:
			print("Combo Selected: ", values)


	def selectComboFromTreeview(self, event, treeviewWidget=None):
		if not self.adminMode:
			return

		if treeviewWidget == None:
			treeviewWidget = self.trvCombosDisplay

		allSelected = strText = record = []
		for indexOfTheSelectedItem in treeviewWidget.selection():
			item = treeviewWidget.item(indexOfTheSelectedItem)
			#record += str(item['values'])
			# record.append(item['values'])
			#print(f"### Selected item : {item} with values: {record}" )
			if self.adminMode:
				print(f"### Selected item: index = {indexOfTheSelectedItem} : {item['values']} --- All values: {item}" )
			# show a message
			# for rowItem in record:
			#	 strText.append(','.join(rowItem))
			#showinfo(title='Information', message=strText)
			item['values'][0] = str(item['values'][0])
			allSelected.append(','.join(item['values']))
		else:
			showinfo("ALL SELECTIONS", "\n".join(allSelected))


	def afficherCombos(self, index=None):
		try:
			# TODO: 
			#if self.combosDisplay != None and type(self.combosDisplay) == tkinter.Tk:
			if self.combosDisplay != None: # and type(self.combosDisplay) == tkinter.Tk:
				#print("Another result is being displayed .... Closing it and displaying the one requested now")
				heading = "Another in use"
				strText = "Veuillez patienter, car un autre résultat est déjà en cours d'affichage."
				#self.combosDisplay.after( 0, self.closeCombosDisplay )
				#showinfo(heading, strText, icon="warning")
				strText += f"\n\nDésirez-vous fermer l'autre résultat ?"
				response = askyesno(title=heading, message=strText) #, **options)
				if self.adminMode:
					print(f"User Response: {response}")
				
				if response:
					self.closeCombosDisplay()
					self.trvCombosDisplay = None
					# Simple order
				else:
					return
			# else:
			#	 pass

		except AttributeError:
			self.updateStatusBar("Display not yet created ... Creating a new one now")

		self.WIP(f"Afficher Combos with index = '{index}'")
		if index == None:
			combos = self.remainingCombos
			heading = f"Combinaisons Restantes : {len(combos)}"
			message = f"{heading} : {len(self.remainingCombos)}\n{self.remainingCombos}"
		elif index == 0:
			combos = self.iPrimary['combos']
			heading = f"Instruction Primaire Initiale : {len(combos)}"
			#message = f"Combinaisons Initiales : {len(self.IPI['combos'])}\n{self.IPI['combos']}"
			message = f"Combinaisons Initiales : {len(self.iPrimary['combos'])}\n{self.iPrimary['combos']}"
		elif index >= 2: # Total Number in "self.listOfInstructionSets"
			combos = self.listOfInstructionSets[index - 1]['combos']
			heading = f"{index - 1}è Instruction Secondaire Courante : {len(combos)}"
			#message = f"Combinaisons Formées : {len(self.listOfInstructionSets[index]['combos'])}\n{self.listOfInstructionSets[index]['combos']}"
			message = f"Combinaisons Formées : {len(self.listOfInstructionSets[index - 1]['combos'])}\n{self.listOfInstructionSets[index - 1]['combos']}"
		else:
			self.PASC()
			return
		#showinfo(heading, message, icon="info")
		# if self.adminMode: # patch
		#	 popUp = tk.Toplevel()
		#	 results = tk.Listbox(popUp, bg=self.colourPrimary)
		#	 for combo in self.listOfInstructionSets[index - 1]['combos']: 
		#		 results.insert(tk.END, combo)
		#	 results.pack()
		# TODO
		if len(combos) == 0:
			showwarning("Invalid Entries", "No Combinations / Values have been received")
			return

		self.maxDisplayTimeTrialMode: int = 90_000 #15_000
		self.combosDisplay = tk.Tk()
		#combosDisplay = self.combosDisplay
		### showinfo("Type of Window", f"CombosDisplay: {type(combosDisplay)}")

		# TODO : def createComboDisplay(self, text: str, resizable=(0,0)):
		strText = "Quick Results Display"
		self.combosDisplay.title(strText)
		self.combosDisplay.resizable(width=0, height=0)
		#self.combosDisplay.geometry("265x500")

		#self.trvCombosDisplay = None
		#combos = 100_000

		# TODO: Configure Treeview
		trvHeadingText = heading
		tk.Label(self.combosDisplay, text=trvHeadingText).pack()

		self.trvCombosDisplay = ttk.Treeview(self.combosDisplay, selectmode='browse')
		# Packing the Treeview after adding the Scrollbars
		#self.trvCombosDisplay.pack(side=tk.LEFT)

		# Attaching the Scrollbars
		## Horizontal
		hsb = ttk.Scrollbar(self.combosDisplay, orient="horizontal", command=self.trvCombosDisplay.xview)
		#self.trvCombosDisplay.configure(xscrollcommand=hsb.set)
		self.trvCombosDisplay.configure(xscrollcommand=hsb.set)
		hsb.pack(side=tk.BOTTOM, fill=tk.X)
		
		## Vertical
		vsb = ttk.Scrollbar(self.combosDisplay, orient="vertical", command=self.trvCombosDisplay.yview)
		self.trvCombosDisplay.configure(yscrollcommand=vsb.set)
		vsb.pack(side=tk.RIGHT, fill=tk.Y)
		###
		# Packing the Treeview after adding the Scrollbars
		self.trvCombosDisplay.pack(side=tk.LEFT)
		self.trvCombosDisplay.bind('<<TreeviewSelect>>', self.selectComboFromTreeview)
		
		self.trvCombosDisplay["columns"] = ("1", "2")
		self.trvCombosDisplay['show'] = 'headings'

		# TODO : Move the styling to a separate method/class
		# Styling the Treeview
		textFontRegular = ('Helvetica', 11)
		textFontHeading = ('Helvetica', 11, "bold")
		styleTrv = ttk.Style(self.master)
		styleTrv.theme_use("clam")
		styleTrv.configure(".", font=textFontRegular)
		styleTrv.configure("Treeview.Heading", foreground='red',font=textFontHeading)

		# TODO : Colours from teh config method/class
		self.trvCombosDisplay.tag_configure("evenRow", foreground="black", background="white")
		#self.trvCombosDisplay.tag_configure("oddRow",	foreground="white", background="black")
		#self.trvCombosDisplay.tag_configure("oddRow",	foreground="white", background="#DEDEDE")
		self.trvCombosDisplay.tag_configure("oddRow",	foreground="#000", background="#DEDEDE")

		self.trvCombosDisplay.column("1", width=100, anchor='c')
		self.trvCombosDisplay.column("2", width=150, anchor='c')

		self.trvCombosDisplay.heading("1", text="Numéros")
		self.trvCombosDisplay.heading("2", text="Combinaisons")

		#if self.adminMode:
		self.updateStatusBar("Loading the combinations in the TreeView ...")
		# TODO Optimasation --- Single function for both options
		for cIndex, combo in enumerate(combos):
			if cIndex % 2 == 0:
				self.trvCombosDisplay.insert("", tk.END, iid=str(cIndex), text=f"L{cIndex+1}", values=(f"{cIndex +1}",f"{combo}"), tags=("evenRow",))
			else:
				self.trvCombosDisplay.insert("", tk.END, iid=str(cIndex), text=f"L{cIndex+1}", values=(f"{cIndex +1}",f"{combo}"), tags=("oddRow",))

			self.updateStatusBar("Résultats en préparation d'affichage", cIndex+1, len(combos))
			#tree.insert(win, tk.END,text=f"L{combo+1}", values=(f"{combo +1}",f"{combo +1}"))
			# if self.adminMode:
			#	 print(f"{cIndex+1} / {len(combos)}")
		else: # All Entries loaded successfully in the TreeView 
			strDoneLoading = f"All combinations loaded in the TreeView : [{len(combos)}]"
			# if self.adminMode: # Already printing it in the updateStatusBar()
			#	 print(strDoneLoading)
			self.updateStatusBar(strDoneLoading)

			# TODO : Cool Off --- For now
			# if self.isTrialPeriod:
			#	 self.trvCombosDisplay.after( self.maxDisplayTimeTrialMode, self.closeCombosDisplay )
				#Corrections here ---------------------

			#maxDisplayTimeTrialMode = None
			
			#tree.unpack()
			self.combosDisplay.mainloop()
			#win.after(20000, closeMe)


	def attachIPC(self):
		self.lblFrWidth = 300
		# LabelFrames
		#lblFrIPC = tk.LabelFrame(frLeft, text="Instruction Principale Courante", bg="blue", width=lblFrWidth)
		self.lblFrIPC = tk.LabelFrame(self.master, text="Combinaisons Restantes", font=("Helvetica 11 bold"), width=self.lblFrWidth, height=50, bg=self.bgColour, fg=self.fgColour)#, fg=self.colourWhite, bg="blue")
		#lblFrIPC.pack(fill="both", expand="yes", padx=10)
		self.lblFrIPC.pack(fill=tk.X, padx=10, pady=5)
		#lblFrIPC.pack(padx=10)
		self.lblFrIPC.columnconfigure((0, 2), weight=1)
		self.lblFrIPC.columnconfigure(1, weight=2)

		self.lblIPC_Combos = tk.Label(self.lblFrIPC, text=f"Combinaisons = 0", bg=self.bgColour, fg=self.fgColour)#) #{self.totalCombos}")
		#lblIPC_K.pack(padx=10)
		#
		#lblIPC_K.grid(row=2, column=0)
		self.lblIPC_Combos.grid(row=2, column=0)

		#self.lblIPCDuration = tk.Label(self.lblFrIPC, text=f"Durée d'éxécution : 0") #{self.totalCombos}")
		
		#self.lblIPCDuration = tk.Label(self.lblFrIPC, text=f"{self.nDots} >") #{self.totalCombos}")
		self.lblIPCDuration = tk.Label(self.lblFrIPC, text=f"{self.nDots} >\n", bg=self.bgColour, fg=self.fgColour)#) #{self.totalCombos}")
		self.lblIPCDuration.grid(row=2, column=1)

		#self.btnIPI_AfficherCombos = tk.Button(self.lblFrIPI, text="Afficher", command=self.afficherCombos, width=11, state=tk.DISABLED) #, bg=self.colourPrimary)
		self.btnIPC_AfficherCombos = tk.Button(self.lblFrIPC, state=tk.DISABLED, text="Afficher", command=self.afficherCombos, cursor="hand1", width=11) #, bg=self.bgColour, fg=self.fgColour)#) #, bg=self.colourPrimary)
		self.btnIPC_AfficherCombos.grid(row=2, column=2, padx=5, pady=5)

		# ## Test
		# self.strVarIPC_OverviewRemainingCombos = tk.StringVar()
		# self.strVarIPC_OverviewRemainingCombos.set('[]')
		# self.lblIPC_OverviewRemainingCombos = tk.Label(self.lblFrIPC, textvariable=self.strVarIPC_OverviewRemainingCombos) #{self.totalCombos}")
		# self.lblIPC_OverviewRemainingCombos.grid(row=3, column=2, padx=5, pady=5)


	def addButtonResetAllOperations(self, parentWidget=None):
		self.WIP("addButtonResetAllOperations")
		if parentWidget == None:
			strText = "Parent Widget Not Specified for 'addButtonResetAllOperations'"
			showinfo(strText, strText)
			self.updateStatusBar(strText)
			return
		#tk.Button(self.lblFrOperations, text="Réinitialiser / Effacer toutes les opérations", command=self.resetAllOperations, bg=self.colourRed).grid(row=self.rCount, column=0, padx=5, pady=5)
		
		#tk.Button(self.lblFrOperations, text="Réinitialiser", command=self.resetAllOperations, bg=self.colourDarkBlue, fg=self.colourWhite).grid(row=self.rCount, column=0, padx=5, pady=5)
		#tk.Button(parentWidget, text="Réinitialiser", command=self.resetAllOperations, bg=self.colourAliceBlue, fg=self.colourWhite).grid(row=self.rCount, column=0, padx=5, pady=5)
		# tk.Button(parentWidget, text="Tout Réinitialiser", command=self.resetAllOperations, bg=self.colourDarkBlue, fg=self.colourWhite).grid(row=self.rCount, column=0, padx=5, pady=5)
		tk.Button(parentWidget, text="Tout Réinitialiser", command=self.resetAllOperations, bg=self.colourDarkBlue).grid(row=self.rCount, column=0, padx=5, pady=5)
		tk.Label(parentWidget, text="(Afin d'effacer toutes les opérations)", bg=self.bgColour, fg=self.colourDarkBlue,).grid(row=self.rCount, column=1, padx=5, pady=5) 
		self.updateRCount()


	def resetAllOperations(self):
		response = askyesno("Are you sure to reset all operations ?","Êtes-vous sûre de vouloir effacer toutes les opérations effectuées jusqu'ici ?\n\n[Tout sera effacé]")
		if not response: #"Pressed 'NO'"
			return
		#strText = "Pressed 'YES'"

		# self.clearInternalNandK()
		# self.instructionsCounter = 0
		# self.instructionSet = []

		self.resetIPI()
		self.resetIPC()
		self.resetOperations()
		if self.instructionsCounter > 0:
			self.resetISC()

		#self.attachOperationsSecondaryOps() # Let's See
		
		self.clearInternalNandK()
		self.instructionsCounter = 0
		self.instructionSet = []
		return


	def setLabel(self, parentWidget=None, labelWidget=None, headText=None, nullText=None):
		if self.adminMode:
			strText : str = f'\n###\nparentWidget : "{parentWidget}"'
			strText += f'\nlabelWidget : "{labelWidget}"'
			strText += f'\nheadText : "{headText}"'
			strText += f'\nnullText : "{nullText}"'
			print(strText)

		if parentWidget == None and labelWidget == None:
			strText = f"Incorrect New Label Widget configurations"
			showinfo(strText, strText)
			return

		if parentWidget != None and labelWidget == None:
			strText = headText if headText else ""
			strText += nullText
			newLabelWidget = tk.Label(parentWidget, text=strText)
			strText = f"New Label Created"
			showinfo(strText, newLabelWidget)
			return newLabelWidget
		
		# after the above tests
		#if labelWidget != None: # regardless of the value of 'parentWidget'
		strText = f"{headText} " if headText else ""
		ISCvalueNotYetSet = nullText if nullText else self.ISCvalueNotYetSet
		
		strText += f"{ISCvalueNotYetSet}"
		labelWidget.configure(text=strText)
		self.colourNormalText = self.colourGreen if self.instructionsCounter else self.colourDark

		# if self.instructionsCounter:
		#	 labelWidget.configure(foreground=self.colourNormalText if not strText.count(ISCvalueNotYetSet) else self.colourWarning)
		# else:
		#	 labelWidget.configure(foreground=self.colourNormalText)

		self.colourNormalText = self.colourDark
		labelWidget.configure(foreground=self.colourNormalText)

		if self.adminMode:
			showinfo("Instruction Counter", f"ICounter: {self.instructionsCounter}\nself.colourNormalText: {self.colourNormalText}")
		return


	def setButton(self, parentWidget=None, buttonWidget=None, btnText=None):
		if self.adminMode:
			strText = f'\n###\nparentWidget : "{parentWidget}"'
			strText += f'\nbuttonWidget : "{buttonWidget}"'
			strText += f'\nbtnText : "{btnText}"'
			print(strText)

		if parentWidget == None and buttonWidget == None:
			strText = f"Incorrect New Button Widget configurations"
			showinfo(strText, strText)
			return

		if parentWidget != None and buttonWidget == None:
			strText = btnText if btnText else "Name Undefined"
			newButtonWidget = tk.Button(parentWidget, text=strText)
			strText = f"New Button Created"
			showinfo(strText, newButtonWidget)
			return newButtonWidget
		
		# after the above tests
		#if buttonWidget != None: # regardless of the value of 'parentWidget'
		
		#strText = f"{headText} " if headText else ""
		strText = btnText if btnText else "Undefined"
		#ISCvalueNotYetSet = nullText if nullText else self.ISCvalueNotYetSet
		
		#strText += f"{ISCvalueNotYetSet}"
		buttonWidget.configure(text=strText)
		self.colourNormalText = self.colourGreen if self.instructionsCounter else self.colourDark

		if self.instructionsCounter:
			buttonWidget.configure(foreground=self.colourNormalText if not strText.count(ISCvalueNotYetSet) else self.colourWarning)
		else:
			buttonWidget.configure(foreground=self.colourNormalText)
		if self.adminMode:
			showinfo("Instruction Counter", f"ICounter: {self.instructionsCounter}\nself.colourNormalText: {self.colourNormalText}")
		return


	def resetIPI(self):
		self.ISCvalueNotYetSet = "[?]"

		#self.resetLabel(self.lblIPI_N, "Valeur de N = ")
		self.setLabel(None, self.lblIPI_N, "Valeur de N = ")
		# strText = f"Valeur de N = {self.ISCvalueNotYetSet}"
		# self.lblIPI_N.configure(text=strText)
		# self.lblIPI_N.configure(foreground=self.colourGreen if not strText.count(self.ISCvalueNotYetSet) else self.colourRed)
		
		self.setLabel(None, self.lblIPI_Alphabet, None, self.nDots)
		# strText = self.nDots
		# self.lblIPI_Alphabet.configure(text=strText)
		# self.lblIPI_Alphabet.configure(foreground=self.colourGreen if not strText.count(self.ISCvalueNotYetSet) else self.colourRed)
		
		self.setLabel(None, self.lblIPI_K, "Valeur de K = ")
		# strText = f"Valeur de K = {self.ISCvalueNotYetSet}"
		# self.lblIPI_K.configure(text=strText)
		# self.lblIPI_K.configure(foreground=self.colourGreen if not strText.count(self.ISCvalueNotYetSet) else self.colourRed)

		self.setLabel(None, self.lblIPI_Combos, "C(n, k) = ")
		self.setLabel(None, self.lblIPIDuration, "Durée d'éxécution = ")
		self.btnIPI_AfficherCombos.configure(state=tk.DISABLED)
		#, text="Afficher", command=lambda: self.afficherCombos(len(self.listOfInstructionSets)), width=11) #, bg=self.colourPrimary)
		return


	def resetIPC(self):
		self.ISCvalueNotYetSet = "[?]"

		self.setLabel(None, self.lblIPC_Combos, "C(n, k) = ")
		#self.setLabel(None, self.lblIPC_Alphabet, None, self.nDots)
		self.btnIPC_AfficherCombos.configure(state=tk.DISABLED)
		return


	def resetOperations(self):
		try:
			#if self.lblFrISC:
			self.lblFrISC.pack_forget()
			self.frInLblFrOperations.grid_forget()
			self.frInLblFrOperations.grid_remove()
		except AttributeError as attribError: #'SVGD' object has no attribute 'lblFrISC'
			print(f"an Attribute has not yet been created\n--->{attribError}")
		
		# self.frInLblFrOperations.grid_forget()
		# self.frInLblFrOperations.grid_remove()
		finally:
			print("---> Removed Secondary Operations")


	def resetISC(self):
		#self.lblFrISC.pack_forget()
		self.ISCvalueNotYetSet = "[?]"
		#self.lblFrISC.configure(text=)
		self.updateTextOfISC()

		self.setLabel(None, self.lblISC_N, "Valeur de N = ")
		self.setLabel(None, self.lblISC_Alphabet, None, self.nDots)
		self.setLabel(None, self.lblISC_K, "Valeur de K = ")
		self.setLabel(None, self.lblISC_Combos, "C(n, k) = ")
		self.setLabel(None, self.lblISCDuration, "Durée d'éxécution = ")
		self.btnISC_AfficherCombos.configure(state=tk.DISABLED)
		return


	def updateRCount(self):
		# if not self.rCount:
		#	 self.rCount = 0
		# else:
		# self.rCount += 1
		self.rCount += 1
		if self.adminMode:
			print(f"Current Row Count : {self.rCount}")
		return self.rCount


	def attachOperations(self):
		self.lblFrOperations = tk.LabelFrame(self.master, text="Les Opérations", font=("Helvetica 11 bold"), width=self.lblFrWidth, height=50, bg=self.bgColour, fg=self.fgColour)#) # fg=self.colourWhite, bg=self.colourDarkBlue) #"yellow")
		#lblFrOperations.pack(fill="both", expand="yes", padx=10)
		#lblFrOperations.pack(fill=tk.X, padx=10)
		self.lblFrOperations.pack(padx=10, pady=5)

##################
		## Operations	
		self.prevCompleted = True
		self.currCompleted = False
		self.currOpsConfirmed = False

		self.rCount = 0
		#self.updateRCount()
		
		global testerMode
		# if self.adminMode or testerMode:
		#	 self.addButtonResetAllOperations()
		#	 self.updateRCount() # executed internally

		self.addButtonResetAllOperations(self.lblFrOperations)
		# self.updateRCount() # executed internally

		self.lblOpsLetters = tk.Label(self.lblFrOperations, text="Entrez les lettres : ", bg=self.bgColour, fg=self.fgColour)#)# utiliser: ")
		self.lblOpsLetters.grid(row=self.rCount, column=0)

		self.strVarLetters = tk.StringVar()
		self.strVarLetters.set("")

		# DEMO
		#global testerMode
		if self.adminMode or testerMode:
			nDummy = 9
		else:
			nDummy = 0
		self.strVarLetters.set( ''.join( self.theAlphabet[:nDummy] ) )

		self.entryOpsLetters = tk.Entry(self.lblFrOperations, textvariable=self.strVarLetters)
		self.entryOpsLetters.grid(row=self.rCount, column=1)

		self.txtBtnLetters = ["Valider les lettres", "Modifier"]

		self.btnOpsLetters = tk.Button(self.lblFrOperations, text=self.txtBtnLetters[0], command=lambda: self.getLetters(self.btnOpsLetters), cursor="hand1", width=12)
		self.btnOpsLetters.grid(row=self.rCount, column=2, padx=5)

		self.updateRCount()

		self.lblOpsK = tk.Label(self.lblFrOperations, text="La valeur de K: ", bg=self.bgColour, fg=self.fgColour)#)
		self.lblOpsK.grid(row=self.rCount, column=0)

		self.strVarK = tk.StringVar()
		self.strVarK.set("")

		# DEMO
		#global testerMode
		if self.adminMode or testerMode:
			kDummyStr = 6 #'.join( self.theAlphabet[:9] ))
		else:
			kDummyStr = ""
		self.strVarK.set(kDummyStr)

		self.entryOpsK = tk.Entry(self.lblFrOperations, textvariable=self.strVarK)
		self.entryOpsK.grid(row=self.rCount, column=1)

		self.txtBtnK = ["Valider K", "Modifier"]

		#self.btnOpsK = tk.Button(self.lblFrOperations, text="Valider K", command=lambda: self.getK(self.btnOpsK), width=12) #, bg=self.bgColour, fg=self.fgColour)#)
		#self.btnOpsK = tk.Button(self.lblFrOperations, text="Valider K", command=self.getK, cursor="hand1", width=12) #, bg=self.bgColour, fg=self.fgColour)#)
		self.btnOpsK = tk.Button(self.lblFrOperations, text=self.txtBtnK[0], command=self.getK, cursor="hand1", width=12) #, bg=self.bgColour, fg=self.fgColour)#)
		self.btnOpsK.grid(row=self.rCount, column=2, padx=5)


		# TODO ??? Why 2 lines ?
		if self.adminMode: # patch
			nkEntryWidth = 12
			self.spbxK = tk.Spinbox(self.lblFrOperations, width=nkEntryWidth, bg=self.colourPrimary, fg="#000",
				validate=tk.ALL, from_=self.minimum_value, to=15 #self.n
			)
			self.spbxK.grid(row=self.rCount, column=3, sticky="W")

		self.updateRCount()
		
		#############################################
		# TODO : Read Ki from Excel File
		self.btnKiFromExcel = tk.Button(self.lblFrOperations, text="Collecter les valeurs K(i) à partir du fichier Excel", command=self.collectKiFromExcelFile, cursor="hand1", width=30, bg=self.colourPrimary)#self.colourDarkBlue) # )
		self.btnKiFromExcel.grid(row=self.rCount, column=0, columnspan=3, pady=10)
		
		self.updateRCount()
		###########################################

		self.btnCombos = tk.Button(self.lblFrOperations, text="Effectuer les combinaisons", command=self.calcCombos, cursor="hand1", width=30, bg=self.colourPrimary)#self.colourDarkBlue) # )
		self.btnCombos.grid(row=self.rCount, column=0, columnspan=3, pady=10)
		#self.updateRCount()

		# To be inserted when instructionsCounter == 1
		#self.attachOperationsSecondaryOps()


	def collectKiFromExcelFile(self):
		os.system("pwd && ls -l && ls -l *.xl")
		collectedKis = (["Dereck"], ["Viviane"])
		for Ki in collectedKis:
			print(Ki, "--->", set(list(Ki[0])) )
			#Ki = set(Ki)


	def clearPreviousOperations(self):
		self.strVarLetters.set("")
		self.entryOpsLetters['state'] = tk.NORMAL
		self.btnOpsLetters['state'] = tk.NORMAL
		self.btnOpsLetters['text'] = self.txtBtnLetters[0]

		self.strVarK.set("")
		self.entryOpsK['state'] = tk.DISABLED
		self.btnOpsK['state'] = tk.DISABLED
		self.btnOpsK['text'] = self.txtBtnK[0]


	def attachOperationsSecondaryOps(self):
		self.cliOptionsAction = ["Garder", "Supprimer"]
		self.cliOptionsBelong = ["Ayant", "N’ayant pas"]

		self.actionStrVar = tk.StringVar()
		self.actionStrVar.set(self.cliOptionsAction[0])

		self.belongStrVar = tk.StringVar()
		self.belongStrVar.set(self.cliOptionsBelong[0])

		self.updateRCount()

		# Tempo Frame
		#self.frInLblFrOperations = tk.Frame(self.lblFrOperations)#, bg=self.colourRed)
		self.frInLblFrOperations = tk.Frame(self.lblFrOperations, bg=self.bgColour)
		self.frInLblFrOperations.grid(row=self.rCount, column=0, columnspan=3)

		# tk.Label(self.frInLblFrOperations, text="Tempo Frame").grid(row=self.rCount, column=0)
		# self.updateRCount()

		strTextAct = f"Quelles Opérations ?"
		if self.adminMode:
			strTextAct += f" {self.iSecMatches}"
		#self.lblOpsActions = tk.Label(self.lblFrOperations, text=strTextAct, bg=self.bgColour, fg=self.fgColour)#)
		self.lblOpsActions = tk.Label(self.frInLblFrOperations, text=strTextAct, font=("Helvetica", 12, "bold"), bg=self.bgColour, fg=self.fgColour)#)
		self.lblOpsActions.grid(row=self.rCount, column=0)

		self.updateRCount()
		# tk.Button(self.lblFrOperations, text=self.cliOptionsAction[0], command=lambda: self.getAction(0), width=12).grid(row=self.rCount, column=0, padx=5, pady=5)
		# tk.Button(self.lblFrOperations, text=self.cliOptionsAction[1], command=lambda: self.getAction(1), width=12).grid(row=self.rCount, column=1, padx=5, pady=5)

		self.cbxOpsActions = ttk.Combobox(self.frInLblFrOperations, value=self.cliOptionsAction, state='readonly', width=12)#)
		#self.cbxOpsActions.bind('<<ComboboxSelected>>', self.getAction)
		self.cbxOpsActions.current(0)
		self.cbxOpsActions.grid(row=self.rCount, column=0, padx=5, pady=5)

		self.cbxOpsBelongs = ttk.Combobox(self.frInLblFrOperations, value=self.cliOptionsBelong, state='readonly', width=12)
		#self.cbxOpsBelongs.bind('<<ComboboxSelected>>', self.getBelong)
		self.cbxOpsBelongs.current(0)
		self.cbxOpsBelongs.grid(row=self.rCount, column=1, padx=5, pady=5)

		tk.Button(self.frInLblFrOperations, text="Confirmer", command=self.confirmerOperations, cursor="hand1", width=12, bg=self.colourPrimary).grid(row=self.rCount, column=2, padx=5, pady=5)
		self.updateRCount()

		# if self.adminMode:
		#	 self.addButtonResetAllOperations()

		# # Old Actions Display
		# self.rCount += 2
		# self.lblOpsActions = tk.Label(self.lblFrOperations, text=f"Quelles Opérations ? ")
		# self.lblOpsActions.grid(row=self.rCount, column=0)
		# # btnOpsAction = tk.Button(lblFrOperations, text=cliOptionsAction[0], command=lambda: getAction(0))
		# # btnOpsAction.grid(row=3, column=1)
		# tk.Button(self.lblFrOperations, text=self.cliOptionsAction[0], command=lambda: self.getAction(0), width=12).grid(row=self.rCount, column=1, padx=5)
		# tk.Button(self.lblFrOperations, text=self.cliOptionsAction[1], command=lambda: self.getAction(1), width=12).grid(row=self.rCount, column=2, padx=5)

		# # Old Belong Display
		# self.rCount += 1
		# self.lblOpsBelongs = tk.Label(self.lblFrOperations, text=f"Quelle Type ? ")
		# self.lblOpsBelongs.grid(row=self.rCount, column=0)
		# tk.Button(self.lblFrOperations, text=self.cliOptionsBelong[0], command=lambda: self.getBelong(0), width=12).grid(row=self.rCount, column=1, padx=5)
		# tk.Button(self.lblFrOperations, text=self.cliOptionsBelong[1], command=lambda: self.getBelong(1), width=12).grid(row=self.rCount, column=2, padx=5)

		self.clearPreviousOperations()
######################################


	def isValidN(self, n):
		if (n <= 26) and (n >= 1):
			return True
		else:
			strText = f"Violation de la contrainte:\n1 <= n <= 26"
			strText += "\n\nOr la valeur de 'n' fournie = {n}\n\nVeuillez entrer une valeur appropriée"
			showwarning("Invalid value of N", strText)
			return False

	# def getLetters0(self, btnWidget):
	#	 # TODO: Deal with Non-Letters

	#	 valueEntered = None

	#	 if btnWidget["text"] == self.txtBtnLetters[1]:
	#		 self.entryOpsLetters["state"]=tk.NORMAL
	#		 btnWidget["text"] = self.txtBtnLetters[0]
	#	 else:
	#		 #self.ltrs = list(self.entryOpsLetters.get())

	#		 # self.theAlphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
	#		 # print("".join( self.theAlphabet ))
	#		 #ltrs = input("Enter the letters to be used without any space and without repetition: ")
			
	#		 # Integration
	#		 valueEntered = self.entryOpsLetters.get().strip()
	#		 try:
	#			 valueEntered = int(float(valueEntered))
	#			 print(f"It is an 'int' : {valueEntered} - {type(valueEntered)}")
	#			 if self.isValidN(valueEntered) :
	#				 self.n = valueEntered
	#				 self.letters = self.theAlphabet[:self.n]
	#			 else:
	#				 self.entryOpsLetters["state"]=tk.NORMAL
	#		 except Exception as e:
	#			 print(f">>> {e}\tIt is not a potential 'int' ---> Considering it as a 'str' : ") #{e}")
				
	#			 ltrs = self.entryOpsLetters.get()
	#			 #ltrs = valueEntered # self.entryOpsLetters.get()
	#			 self.letters = list(set(ltrs.upper()))
	#			 self.letters.sort()
	#			 self.letters = "".join( self.letters )
				
	#			 # Checking of n 
	#			 valueEntered = len(self.letters)
	#			 if self.isValidN(valueEntered):
	#				 self.n = valueEntered
	#			 else:
	#				 self.entryOpsLetters["state"]=tk.NORMAL

	#		 finally:
	#			 print(f">>> The Value Entered in the EntryWidget is: {valueEntered} of type : {type(valueEntered)} ")

	#		 # ltrs = self.entryOpsLetters.get()
	#		 # self.letters = list(set(ltrs.upper()))
	#		 # self.letters.sort()
	#		 # self.letters = "".join( self.letters )
	#		 # self.n = len(self.letters)

	#		 #letters = "abcde"
	#		 #letters = "".join( self.theAlphabet[:n] )

	#		 #letters = tuple(letters.upper())
	#		 #letters = list(letters.upper())

	#		 # self.ltrs = self.entryOpsLetters.get()
	#		 print(f"Letters: ", self.letters, type(self.letters))
	#		 strN = f"N = {self.n}" #{len(self.letters)}"
	#		 print(strN)
	#		 self.entryOpsLetters["state"]=tk.DISABLED
	#		 btnWidget["text"] = self.txtBtnLetters[1]
			
	#		 if self.instructionsCounter == 0:
	#			 self.lblIPI_N["text"] = strN
	#			 self.lblIPI_Alphabet["text"] = self.letters #.upper()
	#		 else:
	#			 self.lblISC_N["text"] = strN
	#			 self.lblISC_Alphabet["text"] = self.letters
			
	#		 # Exceptional Case when N = 1
	#		 if self.n == 1:
	#			 self.getK(self.n)


	def toggleButtonAndEntryWidgets(self, btnWidget, btnWidgetOptions, entryWidget):
		if btnWidget['text'] == btnWidgetOptions[1]:
			btnWidget['text'] = btnWidgetOptions[0]
			#entryWidget['state'] = tk.DISABLED
			entryWidget['state'] = tk.NORMAL
		else:
			btnWidget['text'] = btnWidgetOptions[1]
			#entryWidget['state'] = tk.NORMAL
			entryWidget['state'] = tk.DISABLED


	def getLetters(self, btnWidget):
		# TODO: Deal with Non-Letters
		valueEntered = self.entryOpsLetters.get().strip()
		
		if valueEntered.isdigit(): # int only
			valueEntered = int(valueEntered)
			if self.adminMode:
				print(f"It is an 'int' : {valueEntered} - {type(valueEntered)}")
			if self.isValidN(valueEntered):
				self.n = int(valueEntered)
				self.letters = self.theAlphabet[:self.n]
			else: # The ShowInfo will already been popped-up so from here we just get out of the "getLetters()"
				return

		elif valueEntered.isalpha(): # a-zA-Z only
			if self.adminMode:
				print(f">>>It is not a potential 'int' ---> Considering it as a 'str' : ")
			self.letters = list(set(valueEntered.upper()))
			self.letters.sort()
			self.letters = "".join( self.letters )
			self.n = len(self.letters)

		else:
			strText = f"Vous avez 2 options de saisie:"
			strText+= "\n\n1. Saisir, sans espace, les lettres spécifiques de l'alphabet français que vous désirez utiliser"
			strText+= "\nExemple: AHJKM..."
			strText+= "\n\n2. Saisir une valeur numérique entière 'n'. Et celle-ci, de manière interne, va sélectionner les 'n premières lettres de l'alphabet français'"
			strText+= "\nExemple: 9 => 'ABCDEFGHI'"
			showinfo("Invalid Input", strText, icon="warning")
			return 

		self.toggleButtonAndEntryWidgets(self.btnOpsLetters, self.txtBtnLetters, self.entryOpsLetters)
		self.entryOpsK['state'] = tk.NORMAL
		self.btnOpsK['state'] = tk.NORMAL
		self.btnOpsK['text'] = self.txtBtnK[0]

		# self.ltrs = self.entryOpsLetters.get()
		if self.adminMode:
			print(f"Letters: ", self.letters, type(self.letters))
		strN = f"N = {self.n}" #{len(self.letters)}"
		if self.adminMode:
			print(strN)
		
		if self.instructionsCounter == 0:
			self.lblIPI_N["text"] = strN
			self.lblIPI_Alphabet["text"] = self.letters #.upper()
		else:
			self.lblISC_N["text"] = strN
			self.lblISC_N["foreground"] = self.fgColour
			self.lblISC_Alphabet["text"] = self.letters
			self.lblISC_Alphabet["foreground"] = self.fgColour
			# TODO : FG
		
		# Exceptional Case when N = 1
		if self.n == 1:
			self.getK(self.n)

		if self.adminMode:
			self.spbxK['to'] = self.n


	def isValidK(self, k):
		if (k <= 15) and (k >=1) and (k <= self.n):
			return True
		else:
			strText = f"Violation de la contrainte:\n1 <= k <= 15\nEt k <= {self.n} (Valeur de n)\n\nOr la valeur de 'k' fournie = {k}\n\nVeuillez entrer une valeur appropriée"
			showinfo("Invalid value of K", strText, icon="warning")
			return False

	# def getK(self, fancyParam=None):
	#	 # TODO: Deal with Non-Digits
	#	 if fancyParam == None:
	#		 return
	#	 #elif type(fancyParam) == "<class 'int'>":
	#	 elif type(fancyParam) == int:
	#		 print("Default value sent")
	#		 self.k = defaultK = fancyParam # Fun

	#		 if self.isValidK(self.k):
	#			 self.entryOpsK["state"]=tk.DISABLED
	#			 #self.entryOpsK.insert(tk.END, self.k)
	#			 self.strVarK.set(self.k)
	#			 self.btnOpsK["text"] = self.txtBtnK[1]
				
	#			 print("External")
	#			 strK = f"K = {self.k}"
	#			 print(strK, type(self.k))

	#			 if self.instructionsCounter == 0:
	#				 self.lblIPC_K["text"] = strK
	#				 #self.strVarK.set(strK)
	#			 else:
	#				 self.lblISC_K["text"] = strK

	#	 #elif type(fancyParam) == "<class 'tkinter.Button'>" and self.entryOpsK.get():
	#	 elif type(fancyParam) == tk.Button and self.entryOpsK.get():
	#		 print("Button Clicked")
	#		 btnWidget = fancyParam
	#		 #if btnWidget == None and type(defaultK) == "<class int>"
	#		 if btnWidget["text"] == self.txtBtnK[1]:
	#			 self.entryOpsK["state"]=tk.NORMAL
	#			 btnWidget["text"] = self.txtBtnK[0]
	#		 else:
	#			 #self.k = int(float(defaultK)) if defaultK != None else int(float(self.entryOpsK.get()))			
	#			 self.k = int(float(self.entryOpsK.get()))

	#			 if self.isValidK(self.k):
	#				 self.entryOpsK["state"]=tk.DISABLED
	#				 btnWidget["text"] = self.txtBtnK[1]
					
	#				 strK = f"K = {self.k}"
	#				 print(strK, type(self.k))

	#				 if self.instructionsCounter == 0:
	#					 self.lblIPI_K["text"] = strK
	#				 else:
	#					 self.lblISC_K["text"] = strK
	#	 else:
	#		 print(f"Pretty Awkward Case fancyParam = {fancyParam}")
	#		 return


	def warningEnterValidK(self):
		strText = "Veuillez saisir une valeur numérique entière pour 'k'"
		showinfo("Invalid value of K", strText)


	def getK(self, defaultValueOfK=None):
		# TODO: Deal with Non-Digits
		if self.adminMode:
			showinfo("Spinbox Value", f"Spinbox Value of K = {self.spbxK.get()} - type : {type(self.spbxK.get())} - isdigit ?: {self.spbxK.get().isdigit()}")

		if defaultValueOfK == None:
			if self.adminMode:
				showinfo("Value of k", "Btn clicked\n(No default value received)")

			valueEntered = self.entryOpsK.get()
			if valueEntered.isdigit():
				valueEntered = int(valueEntered)
			else:
				self.warningEnterValidK()
				return
			
			# if isValidK(valueEntered):
			#	 self.k = valueEntered
			# else:
			#	 return # get out of the function	 

		#elif type(fancyParam) == "<class 'int'>":
		elif (type(defaultValueOfK) == int) or (defaultValueOfK.isdigit()):
			if self.adminMode:
				print(f"Default value received: {defaultValueOfK} of type : {type(defaultValueOfK)}")
			valueEntered = int(defaultValueOfK)
			
			# if isValidK(valueEntered):
			#	 self.k = valueEntered
			# else:
			#	 return # get out of the function

		else:
			strText = "K Value : Pretty Awkward/Strange Scenario"
			strText+= f"Received k as {defaultValueOfK} of type : {type(defaultValueOfK)}"
			if self.adminMode:
				showinfo(strText, strText, icon="error")
			print(strText)
			return # get out of the function


		# valueEntered has already been turned into an 'int' and will now be checked if valid
		if self.isValidK(valueEntered):
			self.k = valueEntered
			self.strVarK.set(self.k)
		else:
			return # get out of the function
		#self.toggleButtonAndEntryWidgets(self.btnOpsLetters, self.txtBtnLetters, self.entryOpsLetters)
		self.toggleButtonAndEntryWidgets(self.btnOpsK, self.txtBtnK, self.entryOpsK)
				
		if self.adminMode:
			print("External")
		strK = f"K = {self.k}"
		if self.adminMode:
			print(f"{strK} : {type(self.k)}")

		if self.instructionsCounter == 0:
			self.lblIPI_K["text"] = strK
			#self.lblIPC_K["text"] = strK # Crazy 
		else:
			self.lblISC_K["text"] = strK
			self.lblISC_K["foreground"] = self.fgColour
			# TODO : FG
##########################


	def read_N_K_letters(self):
		# # self.theAlphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
		# # print("".join( self.theAlphabet ))
		# #ltrs = input("Enter the letters to be used without any space and without repetition: ")
		
		# # Integration
		# ltrs = self.ltrs
		
		# letters = list(set(ltrs.upper()))
		# letters.sort()
		# letters = "".join( letters )
		# n = len(letters)

		# #letters = "abcde"
		# #letters = "".join( self.theAlphabet[:n] )

		# #letters = tuple(letters.upper())
		# #letters = list(letters.upper())

		# print(f"{n} letter(s) have been selected: => {letters}")
		# k = int(input("\nEnter the Value of K: "))

		# # Integration
		# k = self.k

		# n = 26
		# k = 15

		# n = 9
		# k = 6

		# n = 5
		# k = 3
		
		#cleanedInstruction = [ self.n, self.k, self.letters ]
		self.instructionSet = self.emptyInstructionSet()
		self.instructionSet['n'] = self.n
		self.instructionSet['k'] = self.k
		self.instructionSet['letters'] = self.letters
		cleanedInstruction = self.instructionSet
		if self.adminMode:
			print(f"Cleaning: {cleanedInstruction}")
		return cleanedInstruction
###################################################################################


	def generate_combinations(self, instructionSet):
		import sys
		print ('Memory (Before): {0}Mb'.format(sys.getsizeof([])))

		start = time.time()
		#combos = list(combinations(instructionSet[2], instructionSet[1]))
		if self.adminMode:
			print("Just before 'generate_combinations':", instructionSet['letters'], instructionSet['k'])
		combos = list(combinations(instructionSet['letters'], instructionSet['k']))
		end = time.time()
		
		combos = self.normalizeTuplesToStrings(combos)
		
		#return (n, k, combos)
		# instructionSet.append(combos)
		# instructionSet.append(end-start)
		instructionSet['combos'] = combos
		instructionSet['duration'] = str(end-start)
		print ('Memory (After): {0}Mb'.format(sys.getsizeof([])))
		return instructionSet
####################################################################################


	def refreshIPC(self, nkAlphaDurationCombos):
		pass 


	def calcCombos(self):
		# global instructionsCounter
		# global currCompleted
		# global currOpsConfirmed

		if (self.n == None) or (self.k == None):
			showinfo("N & K", "Veuillez saisir des valeurs appropriées pour 'N' et 'K'")
			return

		# Integrations
		if self.instructionsCounter == 0:
			self.InstPrimary()
		else: #self.instructionsCounter > 1:
			self.InstSecondary(self.iSecMatches[self.counterISecondary-1]) if self.counterISecondary <= len(self.iSecMatches) else self.InstSecondary()

		#currOpsConfirmed = False
		if self.prevCompleted and self.currCompleted:
			# TEST ????? self.instructionsCounter += 1
			if self.adminMode:
				print(f"Step : Done with Step {self.instructionsCounter -1} Moving to {self.instructionsCounter}")
			self.currCompleted = False
		else:
			if self.adminMode:
				print(f"We are still at Step {self.instructionsCounter}")

		
		### TEST it seems like this entire block can be deleted ------ WIll get back to it
		if self.instructionsCounter > 0:
			#lblFrISC.pack(fill="both", expand="yes", padx=10)
			#self.lblFrISC.pack(fill="x", padx=10)
			
			#self.lblFrISC["text"] =f"Instruction Secondaire Courante [Etape {self.instructionsCounter}]"
			#self.lblFrISC["text"] = f"Instruction Secondaire Courante [Etape {self.instructionsCounter}] <{self.iSecMatches[self.instructionsCounter-1]}>"
			self.updateTextOfISC()
			

	def refreshOperations(self):
		pass


	def confirmerOperations(self):
		#global currOpsConfirmed
		# action = None
		# belong = None
		self.currOpsConfirmed = True
		self.instructionsCounter += 1

		self.getAction()
		self.getBelong()

		if self.adminMode:
			print(f"Action : {self.selectedAction}\nBelong : {self.selectedBelong}\nCurrent Ops Completed: {self.currOpsConfirmed}")

		# Integrations		
		#localCombinations = self.detection(self.currentCombinations, self.iSecondary[3])
		#localCombinations = self.detection(self.currentCombinations, self.iSecondary['combos'])

		########################################################################################
		# "localCombinations" renamed into "combosOfPresentsAndAbsents"
		combosOfPresentsAndAbsents = self.detection(self.currentCombinations, self.iSecondary['combos'])
		#combosOfPresentsAndAbsents = self.detectAllSubstringsMatchingCharacters(self.currentCombinations, self.iSecondary['combos'])

		# str = f"\n===> Based on your selected Operations: "
		# str += f"'{self.cliOptionsAction[self.selectedAction]}'"
		# str += f" '{self.cliOptionsBelong[self.selectedBelong]}'"
		# #str += f" {self.iSecondary[2]}"
		# str += f" {self.iSecondary['letters']}"
		
		# str += f"\n### Matrice Principale Restante ({len(combosOfPresentsAndAbsents)}): {combosOfPresentsAndAbsents}"
		
		strText = f"\n===> Based on your selected Operations: "
		strText += f"'{self.cliOptionsAction[self.selectedAction]}'"
		strText += f" '{self.cliOptionsBelong[self.selectedBelong]}'"
		#str += f" {self.iSecondary[2]}"
		strText += f" {self.iSecondary['letters']}"
		
		strText += f"\n### Matrice Principale Restante ({len(combosOfPresentsAndAbsents)}): {combosOfPresentsAndAbsents}"

		#str += f"\n\n### Instruction Secondaire ({len(self.iSecondary[3])}): {self.iSecondary[3]}"
		strText += f"\n\n### Instruction Secondaire ({len(self.iSecondary['combos'])}): {self.iSecondary['combos']}"
		if self.adminMode:
			print(strText)


		# if(selectedAction == selectedBelong):
		#	 print(f"\nKeeping PRESENT ({len(currentCombinations[0])}): {currentCombinations[0]}")
		#	 self.currentCombinations = currentCombinations[0]
		# elif(selectedAction != selectedBelong):
		#	 print(f"\nKeeping ABSENT ({len(currentCombinations[1])}): {currentCombinations[1]}")
		#	 self.currentCombinations = currentCombinations[1]

		index = None
		belongs = ["PRESENT", "ABSENT"]
		index = 0 if(self.selectedAction == self.selectedBelong) else 1
		#print(f"\n### Keeping ({belongs[index]}) : ({len(combosOfPresentsAndAbsents[index])}) Combinations : {combosOfPresentsAndAbsents[index]}")
		if self.adminMode:
			print(f"\n### Keeping ({belongs[index]}) : ({len(combosOfPresentsAndAbsents[index])}) Combinations")
		self.remainingCombos = self.currentCombinations = combosOfPresentsAndAbsents[index]

		#self.lblISC_Combos["text"] = len(self.currentCombinations)
		if self.instructionsCounter >= 1:
			self.lblIPC_Combos["text"] = len(self.currentCombinations)
		## Test
		
		#self.lblIPCDuration['text'] = f"{self.nDots} >\n{self.currentCombinations}"
		self.lblIPCDuration['text'] = f"{self.nDots} >\n"
		
		# Test
		# self.strVarIPC_OverviewRemainingCombos.set(f"{self.currentCombinations}")

		#if self.instructionsCounter > 0:
		## ZONGA
		if self.instructionsCounter == 1:
			# self.lblFrISC = tk.LabelFrame(self.master, text=strLblFrame, font=("Helvetica 11 bold"), width=self.lblFrWidth, height=50) 
			# #lblFrISC.pack(fill="both", expand="yes", padx=10)
			# self.lblFrISC.pack(fill="x", padx=10)

			# Attaching the ISC only from	instructionsCounter == 1
			#self.attachISC()
			if self.adminMode:
				print(f"-----------> OK now we are at 1: ({instructionsCounter})")

		if self.instructionsCounter > 0:
			## Test
			if self.adminMode:
				print(">>>>>>>>>>>>>>>> ICounter = ", self.instructionsCounter)
			
			# strLblFrame = f"Instruction Secondaire Courante [Etape {self.instructionsCounter}] <{self.iSecMatches[self.instructionsCounter-1]}>"
			# ###### self.lblFrISC = tk.LabelFrame(self.master, text=strLblFrame, font=("Helvetica 11 bold"), width=self.lblFrWidth, height=50) #, fg=self.colourWhite, bg=self.colourDarkBlue) #"red")
			# self.lblFrISC["text"] = strLblFrame #f"Instruction Secondaire Courante [Etape {self.instructionsCounter}]"
			#
			# Commented in favour of the method()
			self.updateTextOfISC()

			### To be verified
			# self.lblISC_isKeeping['text'] = self.cliOptionsAction[self.selectedAction]
			# self.lblISC_isContaining['text'] = self.cliOptionsBelong[self.selectedBelong]
			self.strVrISC_isKeeping.set(self.cliOptionsAction[self.selectedAction])
			self.lblISC_isKeeping.configure(foreground = self.fgColour)
			self.strVrISC_isContaining.set(self.cliOptionsBelong[self.selectedBelong])
			self.lblISC_isContaining.configure(foreground = self.fgColour)

			self.toggleDisableEnableWidget(self.btnISC_AfficherCombos)
			self.toggleDisableEnableWidget(self.btnIPC_AfficherCombos)

			self.updatelistOfInstructionSets(self.iSecondary)

			self.clearPreviousOperations()
			self.clearInternalNandK()


	def getAction(self, index=None):
	# def getAction(self):
	#	 index=None
		if self.adminMode:
			print(f"aIndex {index}")
		if index == None:
			strText = "Action Index not received\nCollecting it from the Combobox"
			#return
			print(strText)
			index = self.cliOptionsAction.index(self.cbxOpsActions.get())

		self.instructionSet["action"] = index
		self.selectedAction = index
		if self.adminMode:
			print(f"Received Action Index {self.instructionSet['action']}")


	def getBelong(self, index=None):
	# def getBelong(self):
	#	 index=None
		if self.adminMode:
			print(f"bIndex {index}")
		if index == None:
			print("Belong Index not received")
			#return
			print("Collecting it from the Combobox")
			index = self.cliOptionsBelong.index(self.cbxOpsBelongs.get())
		
		self.instructionSet["belong"] = index
		self.selectedBelong = index
		if self.adminMode:
			print(f"Received Belong Index {self.instructionSet['belong']}")


	def refreshISC(self):
		pass


	#@numba.jit(nopython=True)
	#@numba.@jit(parallel=True,fastmath=True,nogil=True,nopython=True)
	def detection(self, superSet, subSet):
		self.tic()
		# My Very Naive Approach on: Sunday 19 December 2021 12:13:54 PM IST
		print("------------Dynamic Dectection-----------")
		print(f"FS: {len(superSet)} {superSet} {type(superSet)}")
		print(f"SS: {len(subSet)} {subSet} {type(subSet)}")
		if self.testerMode:
			askyesno("Detection", "Check raw values received in logs")

		absent = []
		present = []
		superSet = list(superSet)

		for idx, fSet in enumerate(superSet):
			self.refreshScreen()
			self.updateStatusBar("Comparaison des matrices IPR & ISC", idx+1, len(superSet))
			fSet = "".join(fSet) # if Normalized
			absent.append(fSet)
			if self.adminMode:
				print(f"### {fSet}")
			for sSet in subSet:
				count = 0
				for ch in sSet:		 
					#print(ch in fSet)
					count = count + 1 if ch in fSet else count #print("x")
					# if count == len(sSet):
					#	 print(sSet, "eza na kati ya", fSet)
				if count >= len(sSet):
					if self.adminMode:
						print(sSet, "is PRESENT in", fSet)
					present.append(fSet)
				# else:
				#	 print(f"No Match of '{sSet}' Found in {fSet}!")

				#########################
				# Trying to shorten the Turn-around time
				# elif :
				#	 continue

		# Fixture Thursday 23 March 2023 04:42:34 PM IST
		present = self.getRidOfDuplicates(present)
		absent = self.getRidOfDuplicates(absent)

		# Removing the Matching items from the Absent List
		for p in present:
			if p in absent:
				absent.remove(p)
		self.updateStatusBar(f"{self.taskCompletionDelimiter}Fin de comparaison des matrices IPR & ISC")

		#print(f"\n{10*'-'}SUMMARY{10*'-'}\n### PRESENT ({len(present)}): {present}\n\n### ABSENT ({len(absent)}): {absent}")
		if self.adminMode:
			print(f"\n{10*'-'}SUMMARY{10*'-'}\n### PRESENT ({len(present)})\n\n### ABSENT ({len(absent)})")

		self.toc("---> Naive Detection")
		return [present, absent]


############ Fixture Thursday 23 March 2023 04:42:34 PM IST
	def getRidOfDuplicates(self, listOfItems): #list):
		##listFromTheSet = listOfItems # Dummy Test : Without this Fixture
		listFromTheSet = list(set(listOfItems))
		listFromTheSet.sort()
		return listFromTheSet
########################################
# Monday 10 April 2023 08:53:05 AM IST


	def detectAllSubstringsMatchingCharacters(self, SuperStrings, substrings):
		# global present, absent
		present = []
		absent = []
		for SuperString in SuperStrings:
			for substring in substrings:
				if all(ss in SuperString for ss in substring):
					print(f"{substring} is PRESENT in {SuperString}")
					if (SuperString not in present): # and (SuperString not in absent):
						present.append(SuperString)				 
					else:
						print(f"{substring} was already detected previously, hence not added again")

				else:
					#absent.append(SuperString)
					if (SuperString not in present) and (SuperString not in absent):
						absent.append(SuperString)
					#SuperStrings.remove(SuperString)
					print(f"{substring} is ABSENT from {SuperString}")

		# TODO
		# Make sure of a Pure Set : No Duplicate
		# present = list(set(present))
		# present.sort()
		# absent = list(set(absent))
		# absent.sort()

		# Remove any
		print(f"\n---> BEFORE\nPRESENTs ({len(present)}): {present}\nABSENTs ({len(absent)}): {absent}")
		# [absent.remove(x) for x in present if x in absent]
		# absent.remove(x) for x in present if x in absent

		absentCOPY = absent.copy()
		for x in present:
			if x in absent:
				absentCOPY.remove(x)
				print(f"{x} already belongs to the Present List, hence being removed from Absent List which is now : {absentCOPY}")
		absent = absentCOPY
		print(f"\n---> AFTER\nPRESENTs ({len(present)}): {present}\nABSENTs ({len(absent)}): {absent}")
		return [present, absent]
########################################################


	#@numba.jit(nopython=True)
	#@numba.@jit(parallel=True,fastmath=True,nogil=True,nopython=True)
	def normalizeTuplesToStrings(self, rawTuples=None):
		if rawTuples == None:
			strText = "No Tuple Received for normalization"
			showwarning(strText, strText)
			return None

		if self.adminMode or self.testerMode:
			rtr = "Raw Tuples Received"
			strText = f"---> {rtr}:\n{rawTuples}\n<--- {rtr}"
			print(strText)
			strText = rtr = None
			askyesno("Check Raw Tuples in Logs", f"Did you check the '{len(rawTuples)} Raw Tuple(s)' in the logs ?")

		#ipCombinations = []
		#ipCombinations = None
		ipCombinations = [np.complex64(x) for x in range(0)]

		start = time.time()
		for i, c in enumerate(rawTuples):
			#ipCombinations = [] if ipCombinations == None else ipCombinations
			combStr = ''.join(c)
			ipCombinations.append(combStr)
			#print(f"[{zfill(i+1, 3)}] {combStr}", sep="-", end="\t")
			print(f"[{i+1}] {combStr}", sep="-", end="\t")
			self.refreshScreen()
			self.updateStatusBar("Mise en forme des résultats", i+1, len(rawTuples))
		self.updateStatusBar(f"{self.taskCompletionDelimiter}[Normalisation] Mise en forme des résultats : {(time.time() - start)} second(s)")
		#print(ipCombinations)
		return ipCombinations


	def InstPrimary(self):
		# Integration
		self.iPrimary = self.read_N_K_letters()
		
		#self.iPrimary = self.read_N_K_letters()
		print(5*"-", f"Instruction Primaire : {self.iPrimary}")
		self.iPrimary = self.generate_combinations(self.iPrimary)
		#print(f"Resulting in {len(self.iPrimary[3])} combinations:\n{self.iPrimary[3]}")
		
		# print(f"Resulting in \n=> N = {self.iPrimary[0]} | K = {self.iPrimary[1]} | Letters = {self.iPrimary[2]}")
		# print(f"Total Combinations {len(self.iPrimary[3])} in {self.iPrimary[4]} second(s)")
		
		# print(f"Resulting in \n=> N = {self.iPrimary['n']} | K = {self.iPrimary['k']} | Letters = {self.iPrimary['letters']}")
		# print(f"Total Combinations {len(self.iPrimary['combos'])} in {self.iPrimary['duration']} second(s)")
		self.resultsAfterCombo(self.iPrimary)
		
		#self.currentCombinations = self.iPrimary[3]
		self.currentCombinations = self.iPrimary['combos']
		print(f"===> Current Combinations: ({len(self.currentCombinations)}) => {self.currentCombinations}")
		
		## Test
		# self.listOfInstructionSets.append(self.iPrimary)
		# showinfo(f"Instruction Count {len(self.listOfInstructionSets)}", f"{self.listOfInstructionSets}", icon="info")
		self.updatelistOfInstructionSets(self.iPrimary)		

		self.counterISecondary = self.counterISecondary + 1
		#self.cliMenu()

		# Update UI
		self.lblIPI_Combos["text"] = f"C(n, k) = {len(self.currentCombinations)}"
		# NOT YET SET : self.lblIPC_Combos["text"] = f"Combinaisons : {len(self.currentCombinations)}"
		#self.lblIPIDuration["text"] = self.iPrimary[4]
		self.lblIPIDuration["text"] = f"Durée d'éxécution : {str(self.iPrimary['duration'])}"

		self.toggleDisableEnableWidget(self.btnIPI_AfficherCombos)
		
		self.instructionsCounter += 1
		self.attachOperationsSecondaryOps()
		self.attachISC()
		self.clearInternalNandK()


	def clearInternalNandK(self):
		self.n = self.k = None


	def resultsAfterCombo(self, receivedInstructionSet):
		print(f"\n===> Resulting in : N = {receivedInstructionSet['n']} | K = {receivedInstructionSet['k']} | Letters = {receivedInstructionSet['letters']}")
		print(f"Total Combinations {len(receivedInstructionSet['combos'])} in {receivedInstructionSet['duration']} second(s)")


	def InstSecondary(self, str=""):
		print(5*"-", f"Instruction Secondaire - Step [{self.counterISecondary}]: '{str}'")
		
		self.iSecondary = self.read_N_K_letters()
		print(5*"-", f"Instruction Secondaire : {self.iSecondary}")
		
		#self.iPrimary = self.generate_combinations(self.iSecondary)
		self.iSecondary = self.generate_combinations(self.iSecondary)

		#print(f"Resulting in {len(self.iPrimary[3])} combinations:\n{self.iPrimary[3]}")
		
		# print(f"Resulting in \n=> N = {self.iSecondary[0]} | K = {self.iSecondary[1]} | Letters = {self.iSecondary[2]}")
		# print(f"Total Combinations {len(self.iSecondary[3])} in {self.iSecondary[4]} second(s)")
		
		# print(f"Resulting in \n=> N = {self.iSecondary['n']} | K = {self.iSecondary['k']} | Letters = {self.iSecondary['letters']}")
		# print(f"Total Combinations : {len(self.iSecondary['combos'])} in {self.iSecondary['duration']} second(s)")
		self.resultsAfterCombo(self.iSecondary)
		
		#self.lblISC_Combos['text'] = f"Combinaisons Totales: {len(self.iSecondary['combos'])}" # ??
		self.lblISC_Combos['text'] = f"C(n, k) = {len(self.iSecondary['combos'])}" # ??
		self.lblISC_Combos["foreground"] = self.fgColour
		self.lblISCDuration['text'] = f"Durée d'éxécution : {self.iSecondary['duration']}"
		self.lblISCDuration["foreground"] = self.fgColour
		# TODO : FG

		# TEST self.instructionsCounter += 1
		self.counterISecondary += 1
		#self.lblFrISC = tk.LabelFrame(self.master, text=f"Instruction Secondaire Courante [Etape {self.instructionsCounter}]", font=("Helvetica 11 bold"), width=lblFrWidth, height=50) #, fg=self.colourWhite, bg=self.colourDarkBlue) #"red")
		
		#self.lblFrISC["text"] = f"Instruction Secondaire Courante [Etape {self.instructionsCounter}]"
		#self.lblFrISC["text"] = f"Instruction Secondaire Courante [Etape {self.instructionsCounter}] <{self.iSecMatches[self.instructionsCounter-1]}>"
		self.updateTextOfISC()

		#self.cliMenu()
		#self.cliMenuOperations()


	def updateTextOfISC(self):
			# TODO : Extra Inner Indentation but it still wors as all have 
			strText = f"Instruction Secondaire Courante "
			if self.adminMode:
				strText = f"[Etape {self.instructionsCounter}]"
			if self.adminMode and (self.instructionsCounter <= len(self.iSecMatches)):
				strText += f"<{self.iSecMatches[self.instructionsCounter-1]}>"
			self.lblFrISC["text"] = strText
			# SRC & Reference : self.lblFrISC["text"] =f"Instruction Secondaire Courante [Etape {self.instructionsCounter}]"

		
	def attachISC(self):
		#self.lblFrISC = tk.LabelFrame(self.master, text=f"Instruction Secondaire Courante [Etape {0}]", font=("Helvetica 11 bold"), width=lblFrWidth, height=50) #, fg=self.colourWhite, bg=self.colourDarkBlue) #"red")
		
		#strLblFrame = f"Instruction Secondaire Courante [Etape {self.instructionsCounter}] <{self.iSecMatches[self.instructionsCounter]}>"
		#self.updateTextOfISC()
		strLblFrame = f"Instruction Secondaire Courante [Etape {self.instructionsCounter}] <{self.iSecMatches[self.instructionsCounter-1]}>"

		self.lblFrISC = tk.LabelFrame(self.master, text=strLblFrame, font=("Helvetica 11 bold"), width=self.lblFrWidth, height=50, fg=self.fgColour, bg=self.bgColour)# "red")
		# if instructionsCounter > 0:
		#lblFrISC.pack(fill="both", expand="yes", padx=10)
		self.lblFrISC.pack(fill=tk.X, padx=10, pady=5)

		self.lblFrISC.columnconfigure((0, 2), weight=1)
		self.lblFrISC.columnconfigure(1, weight=2)

		self.ISCvalueNotYetSet: str = "[?]"

		self.someVariable = None
		self.someVariable = f"Dummy Value"
		self.someVariable = f"Dummy Value{self.ISCvalueNotYetSet}"
		self.setLblISC_N(self.someVariable)
##########################################################


	def setLblISC_N(self, value=None):
		if self.adminMode:
			strText = "No Value Passed !" if value == None else f"Raw : {value}\ncount : {value.count(self.ISCvalueNotYetSet)}"
			showinfo("Value Received", strText)

		#ISCvalueNotYetSet = self.ISCvalueNotYetSet

		strText = f"Valeur de N = {self.ISCvalueNotYetSet}"
		#self.lblISC_N = tk.Label(self.lblFrISC, text=strText, bg=self.bgColour, fg=self.colourGreen if strText.split("=")[1] != ISCvalueNotYetSet else self.colourRed)#{self.N}")
		self.lblISC_N = tk.Label(self.lblFrISC, text=strText, bg=self.bgColour, fg=self.colourGreen if not strText.count(self.ISCvalueNotYetSet) else self.colourRed)#{self.N}")
		#lblIPC_N.pack(side=tk.LEFT, padx=10)
		self.lblISC_N.grid(row=0, column=0)

		self.lblISC_Alphabet = tk.Label(self.lblFrISC, text= self.nDots, bg=self.bgColour, fg=self.colourGreen if not strText.count(self.ISCvalueNotYetSet) else self.colourRed)#) #self.receivedLetters) #, width="26")
		#lblIPC_Alphabet.pack(side=tk.LEFT, padx=10)
		self.lblISC_Alphabet.grid(row=0, column=1)

		strText = f"Valeur de K = {self.ISCvalueNotYetSet}"
		self.lblISC_K = tk.Label(self.lblFrISC, text=strText, bg=self.bgColour, fg=self.colourGreen if not strText.count(self.ISCvalueNotYetSet) else self.colourRed)#) #{self.K}")
		#lblIPC_K.pack(side=tk.RIGHT, padx=10)
		self.lblISC_K.grid(row=0, column=2)

		# Dummy Test with Fake Space
		# lblIPC_space = tk.Label(lblFrIPC) #, text=f"Combinaisons Totales = {totalCombos}")
		# #lblIPC_space.pack(fill="both", expand=True)
		# lblIPC_space.grid(row=1, column=0, rowspan=3)

		strText = f"C(n, k) = {self.ISCvalueNotYetSet}"
		self.lblISC_Combos = tk.Label(self.lblFrISC, text=strText, bg=self.bgColour, fg=self.colourGreen if not strText.count(self.ISCvalueNotYetSet) else self.colourRed)#) #{self.totalCombos}")
		#lblIPC_K.pack(padx=10)
		#
		#lblIPC_K.grid(row=2, column=0)
		self.lblISC_Combos.grid(row=2, column=0)

		strText = f"Durée d'éxécution = {self.ISCvalueNotYetSet}"
		self.lblISCDuration = tk.Label(self.lblFrISC, text=strText, bg=self.bgColour, fg=self.colourGreen if not strText.count(self.ISCvalueNotYetSet) else self.colourRed)#) #{self.totalCombos}")
		self.lblISCDuration.grid(row=2, column=1)

		#self.btnIPI_AfficherCombos = tk.Button(self.lblFrIPI, text="Afficher", command=self.afficherCombos, width=11, state=tk.DISABLED) #, bg=self.colourPrimary)
		
		#self.btnISC_AfficherCombos = tk.Button(self.lblFrISC, text="Afficher", command=self.afficherCombos, width=11) #, bg=self.colourPrimary)
		self.btnISC_AfficherCombos = tk.Button(self.lblFrISC, state=tk.DISABLED, text="Afficher", command=lambda: self.afficherCombos(len(self.listOfInstructionSets)), cursor="hand1", width=11) #, bg=self.colourPrimary)
		### Reference
		# len(self.listOfInstructionSets[index]['combos'])
		# len(self.listOfInstructionSets)
		self.btnISC_AfficherCombos.grid(row=2, column=2, padx=5, pady=5)

###
		self.lblISC_OpsChoices = tk.Label(self.lblFrISC, text=f"Opérations Choisies", bg=self.bgColour, fg=self.fgColour)#) #{self.totalCombos}")
		self.lblISC_OpsChoices.grid(row=3, column=0)

		self.strVrISC_isKeeping = tk.StringVar()
		
		self.strVrISC_isKeeping.set(self.ISCvalueNotYetSet)
		# if self.adminMode:
		#	 self.strVrISC_isKeeping.set("isKeeping")

		self.lblISC_isKeeping = tk.Label(self.lblFrISC, textvariable=self.strVrISC_isKeeping, font=("Helvetica", 12, "bold"), bg=self.bgColour, fg=self.colourGreen if not self.strVrISC_isKeeping.get().count(self.ISCvalueNotYetSet) else self.colourRed)#) #{self.totalCombos}")
		self.lblISC_isKeeping.grid(row=3, column=1)

		self.strVrISC_isContaining = tk.StringVar()
		self.strVrISC_isContaining.set(self.ISCvalueNotYetSet)
		# if self.adminMode:
		#	 self.strVrISC_isContaining.set("isContaining")

		#self.btnIPI_AfficherCombos = tk.Button(self.lblFrIPI, text="Afficher", command=self.afficherCombos, width=11, state=tk.DISABLED) #, bg=self.colourPrimary)
		# self.btnISC_AfficherCombos = tk.Button(self.lblFrISC, text="Afficher", command=self.afficherCombos, width=11) #, bg=self.colourPrimary)
		# self.btnISC_AfficherCombos.grid(row=3, column=2, padx=5, pady=5)
		
		# self.lblISC_isContaining = tk.Label(self.lblFrISC, textvariable=self.strVrISC_isContaining, font=("Helvetica", 12, "bold"), bg=self.bgColour, fg=self.colourGreen if self.strVrISC_isContaining.get() != ISCvalueNotYetSet else self.colourRed)#) #{self.totalCombos}")
		self.lblISC_isContaining = tk.Label(self.lblFrISC, textvariable=self.strVrISC_isContaining, font=("Helvetica", 12, "bold"), bg=self.bgColour, fg=self.colourGreen if not self.strVrISC_isContaining.get().count(self.ISCvalueNotYetSet) else self.colourRed)#) #{self.totalCombos}")
		self.lblISC_isContaining.grid(row=3, column=2, padx=5, pady=5)


	def createStatusBar(self, container=None):
		if container == None:
			container = self.master

		self.taskCompletionDelimiter = "#DONE#"		
		#builtBy = f"built by '{self.devTeam.name}'" # Not Valid Call
		#builtBy = f"developed by '{self.devTeam['name']}'"
		builtBy = f"'{self.devTeam['name']}'" #AWAAA
		self.strVarStatusBar = tk.StringVar() #(self.get_name_of_the_app())
		#self.strVarStatusBar.set(f"Welcome to '{self.getAppName()}' {builtBy}")
		self.strVarStatusBar.set(f"'{self.getAppName()}' v{self.getAppVersion()} - {self.combine.getDevTeam('name')}")
		self.lblStatusBar = tk.Label(container, textvariable=self.strVarStatusBar, bd=1, relief=tk.SUNKEN, anchor=tk.W)
		self.lblStatusBar.pack(side=tk.BOTTOM, fill=tk.X)


	def updateStatusBarBackgroundColourToNormal(self):
		self.lblStatusBar.configure(background=self.colourSystemGrey)


	def updateStatusBar(self, stringToBeDisplayed: str =" . . .", currentValue: int = None, totalValues: int = None, isAdminInfo: bool=False):
	#def updateStatusBar(self, stringToBeDisplayed=" . . .", currentValue=None, totalValues=None, isAdminInfo=False):
		# e = datetime.now()
		# print (e.strftime("%Y-%m-%d %H:%M:%S"))
		#e = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
		bgColour: str = self.colourGreen if stringToBeDisplayed.startswith(self.taskCompletionDelimiter) else self.colourSystemGrey
		stringToBeDisplayed: str = stringToBeDisplayed.lstrip(self.taskCompletionDelimiter)

		if currentValue != None and totalValues !=None:
			# TODO : 
			# currentValue = int(currentValue)
			# totalValues = int(totalValues)
			
			#stringToBeDisplayed = f"[{ (currentValue * 100) / totalValues } %] : {currentValue} / {totalValues} - {stringToBeDisplayed}"
			stringToBeDisplayed += f" : [{ round((currentValue * 100) / totalValues, 3) } %] : {currentValue} / {totalValues}"
			
			#bgColour = self.colourGreen if (currentValue +1) == totalValues else self.colourYellow
			bgColour = self.colourGreen if (currentValue) == totalValues else self.colourYellow
			if bgColour == self.colourGreen:
				self.lblStatusBar.after(30_000, self.updateStatusBarBackgroundColourToNormal)
		
		self.strVarStatusBar.set(stringToBeDisplayed) #"Button Clicked at " + e)
		self.lblStatusBar.configure(background=bgColour)
		print(stringToBeDisplayed)


	def addCloseButton(self):
		#self.btnClose = tk.Button(self.heading, text="Close Window", command=self.closeWindow)
		self.btnClose = tk.Button(self.heading, text="Se Déconnecter", command=self.closeWindow)# , fg=self.fgColour, bg=self.bgColour)#)
		self.btnClose.pack(side=tk.LEFT, padx=80)


	def addWelcomeLabel(self):
		# self.lblWelcome = tk.Label(self.heading, text="Bienvenue, Mr David", font=("Helvetica", 13, "bold"), foreground=self.headingTextColour, bg=self.bgColour)
		self.lblWelcome = tk.Label(self.heading, text=f"Bienvenue, {self.combine.currentUser}", font=("Helvetica", 13, "bold"), foreground=self.headingTextColour, bg=self.bgColour)
		self.lblWelcome.pack(side=tk.LEFT, padx=80, pady=3)
		

	def closeWindow(self, option="q"):
		self.db.quickBackup()
		if self.adminMode:
			print(f"option: {option}")
		print("\nGUI: Exiting the Programme")
		self.master.destroy() # GUI
		#self.master.quit()	 #root.quit()

		if(option == "n") or (option == "r"):
			os.system("echo '------> Restarting'")
			#os.system("python3 initSVGD_COMBINE_GUI.py")
			os.system("python3 SVGD_COMBINE.py")
			restartCommand = ".\\SVGD_COMBINE"if platform.system() == 'Windows' else "./SVGD_COMBINE"
			os.system(restartCommand)

		elif(option == "q"):
			print("CLI: Bye & See you soon !")
			exit()	# CLI
			print("I am supposed to be 'INVISIBLE'")
		else:
			print("Wrong Argument entered")

#####################################
### Last Break
#####################################
### Next Stage 

############## MENU BAR############################################
	def createWindowMenuBar(self):
	#def createMenuBar(self):
		self.WIP("createWindowMenuBar")

		app_menuBar = tk.Menu(self.master)
		self.master.config(menu=app_menuBar)
		
		# Menu Items
		## OPERATIONS
		opts_menu = tk.Menu(app_menuBar, tearoff=0)
		app_menuBar.add_cascade(label="Opérations", menu=opts_menu)
		### ABOVE LINE CAN BE PLACED AT THE BOTTOM AFTER ADDING INDIVIDUAL OPTIONS
		# if self.adminMode: # TODO patch
		#	 opts_menu.add_command(label="Nouvelle Session", command=lambda: self.closeWindow('n')) #newSession)
		
		#opts_menu.add_command(label="Nouvelle Session", command=lambda: self.closeWindow('n')) #newSession)
		opts_menu.add_command(label="Réinitialiser la Session", command=lambda: self.resetAllOperations())
		opts_menu.add_separator()
		opts_menu.add_command(label="Enregistrer les résultats", command=lambda: self.WIP("Enregistrer la Session", True, True))
		# Friday 24 March 2023 02:36:21 AM IST
		# TODO : Generate a PDF Report and Print it
		# python 3 print pdf
		# https://www.youtube.com/watch?v=ijTUx5OceDY
		# Excel, HTML and PDF
		# https://www.youtube.com/watch?v=H7baMf481lU
		opts_menu.add_command(label="Imprimer les résultats", command=lambda: self.WIP("Imprimer la Session", True, True))
		opts_menu.add_command(label="Et encore plus", command=lambda: self.WIP("Imprimer la Session", True, True))
		opts_menu.add_separator()
		opts_menu.add_command(label="Se Déconnecter", command=self.closeWindow)
		#ALTERNATIVE: file_menu.add_command(label="Exit", command=self.master.quit)

		## SETTINGS
		settings_menu = tk.Menu(app_menuBar, tearoff=0)
		settings_menu.add_command(label="Changer Mot de passe", command=lambda: self.uiResetpassword())
		settings_menu.add_command(label="Changer les couleurs", command=lambda: self.WIP("Changer les couleurs", True, True))
		settings_menu.add_command(label="Et encore plus", command=lambda: self.WIP("Et encore plus", True, True))
		app_menuBar.add_cascade(label="Paramètres", menu=settings_menu)

		## INFO
		info_menu = tk.Menu(app_menuBar, tearoff=0)
		info_menu.add_command(label="À propos", command=self.about_SVGD_COMBINE)
		info_menu.add_command(label="App. Version", command=self.about_appVersion)
		info_menu.add_command(label="The Dev. Team", command=self.about_CSMARTLAB)
		if self.adminMode: # TODO
			info_menu.add_command(label="Mettre à jour", command=self.about_updateSVGD)
			info_menu.add_command(label="Nous Contacter", command=self.about_contactCSMARTLAB)
		#if self.adminMode:
			info_menu.add_command(label="Bug Fix & Future Enhancements", command=self.about_bugFixes)
		app_menuBar.add_cascade(label="Infos", menu=info_menu)

		## Test
		# self.dateStrVar = tk.StringVar()
		# self.dateStrVar.set("Initial")
		# test_menu = tk.Menu(app_menuBar, tearoff=0)
		# info_menu.add_command(label="À propos de l'Application", command=self.about_SVGD_COMBINE)
		# info_menu.add_command(label="The Dev. Team", command=self.about_CSMARTLAB)
		# app_menuBar.add_cascade(label=self.dateStrVar.get(), menu=test_menu)


	## Menu Items' Commands
	def about_bugFixes(self):
		strText = ""
		listOfFeaturesToBeImplemented = ["Tab Control", "Optimize Normalisation"]
		for index, feature in enumerate(listOfFeaturesToBeImplemented):
			strText += f"\n{index + 1}. {feature}"
		showinfo("Enhancements", strText)


	def about_SVGD_COMBINE(self):
		aboutSVGD = "" #f"[Work In Progress]\n
		aboutSVGD += "Ce programme utilise la formule des Combinaisons\nsans repétitions, qui est\ndonnée sous la forme: C(n, k)"
		#aboutWin = tk.Toplevel(self.master)
		#button = tk.Button(filewin, text="Work In Progress", font=("Arial 16"), bg="green")
		# lblCSL = tk.Label(aboutWin, bg=self.colourPrimary, fg="white", font=("Arial 16"), text=aboutSVGD).pack()
		showinfo("About the App", aboutSVGD, icon="info")


	def about_CSMARTLAB(self):		
		aboutCSL = f"{self.devTeam['name']}\nProviding Pragmatic Solutions for your daily needs\n\n{self.devTeam['email']}\n{self.devTeam['mobile']}"
		aboutCSL = f"{self.devTeam['name']}\nProviding Pragmatic Solutions\n\n{self.devTeam['email']}\n{self.devTeam['mobile']}"

		aboutCSL = f"{self.devTeam['name']}\nPragmatic Solutions\n{self.devTeam['website']}\n\n{self.devTeam['email']}\n{self.devTeam['mobile']}"
		#tlCSLpopup = tk.Toplevel(self.master)
		#lblCSL = tk.Label(tlCSLpopup, bg=self.colourPrimary, fg="white", font=("Arial 16"), text=aboutCSL).pack()
		#btnOk = tk.Button(cslPopUp, text="OK").pack()
		showinfo("About The Dev. Team", aboutCSL, icon="info")
		return


	def about_contactCSMARTLAB(self):
		self.WIP("about_contactCSMARTLAB")
		return


	def about_updateSVGD(self):
		self.WIP("about_updateSVGD")
		return


	def about_appVersion(self):		
		aboutVersion = f"Version : {self.getAppVersion()}" #\n\nMode d'usage :\nEssaie limité pour le client"
		# TODO: aboutVersion += f"\n\n{deadline}"
		showinfo("App version", aboutVersion, icon="info")


	def newSession(self):
		self.closeWindow('r')
		return


############## END MENU BAR ############################################
	def drawRightLabelFrame(self):
		# Right LabelFrame
		#pass
		self.WIP("drawRightLabelFrame")
		# ## IPI
		# if not testMode:
		#	 lblIPI_N = tk.Label(lblFrIPI, text="N initial").pack(side=tk.LEFT)
		#	 lblIPI_K = tk.Label(lblFrIPI, text="K initial").pack(side=tk.RIGHT)
		#return

# print("# FrontEnd : Loaded Successfully !")
# ############## END Class SVGD ############################################

# def main():
#	 bEnd = COMBINE()
#	 print("# BackEnd : Ready for further instructions.")
# #####################################################################################################


def main() -> None:
	SVGD()	#fEnd = SVGD()

if __name__ == "__main__":
	main()
print("# SVGD COMBINE : End of file")
#from infix_folder.special import Infix